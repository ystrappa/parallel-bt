#!/bin/bash

# Fixed parameters:
# Dataset size D = 10000
# model: adjnoun - 112 binary variables
# Test: I(0,1|Z), Z={2,3,4,...}

echo "Usage: ./experiment.sh [thr] [rep] [max] [data_path] [mem_max]"
echo "E.g.: ./experiment.sh 8 1 20 experiments/data/adjnoun 30000"

thr=$1
rep=$2
max=$3
data_path=$4
mem_max=$5

#dataset="experiments/data/adjnoun"

echo "conditioning_set_size,total_time" > results_bt.csv
echo "conditioning_set_size,total_time" > results_pbt.csv


for z in $(seq 1 ${max}); do

 for r in $(seq 1 ${rep}); do

  java -native -Xmx${mem_max}M -jar -Djomp.threads=${thr} pbt.jar $z parallel ${data_path} >> results_pbt_r${r}.csv
  echo -e "R=${r} - finished parallel,\t conditioning set size ${z}"

  java -native -Xmx${mem_max}M -jar pbt.jar $z sequential ${data_path} >> results_bt_r${r}.csv
  echo -e "R=${r} - finished sequential,\t conditioning set size ${z}"

 done
done

echo "DONE."

