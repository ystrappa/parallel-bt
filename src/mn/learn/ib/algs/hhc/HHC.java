package mn.learn.ib.algs.hhc;




import java.util.List;

import mn.learn.ib.it.StatisticalIndependenceTest;
import mn.rep.UndirectedGraph;


import data.Dataset;



public class HHC {
	/**
	 * The number of variables of the domain
	 */
	private int n;
	
	/**
	 * The output network learned by IBMAP-HC 
	 */
	private UndirectedGraph outputNetwork;
	
	/**
	 * The statistical independence test instance
	 */
	private StatisticalIndependenceTest test;
	
	/**
	 * The training dataset used in the algorithm to learn the independence structure 
	 */
	private Dataset dataset;
	
	public HHC(int n, StatisticalIndependenceTest test, Dataset dataset) {
		this.n = n;
		outputNetwork = new UndirectedGraph(n);
		this.test = test;
		this.dataset = dataset;
	}

	/**
	 * HHC (Global learning algorithm)
	 * <p>
	 * 1. Find PC(X) for every variable X in data using semi-interleaved
	 * HITON-PC (without symmetry correction) and lexicographic prioritization
	 * <p>
	 * 2. Piece together the undirected skeleton using an 'OR rule' (an edge
	 * exists between A and B iff A is in PC(B) or B is in PC(A)
	 * <p>
	 * 3. Use greedy steepest-ascent TABU search and BDeu score to orient edges
	 * (not implemented for undirected graphs here)
	 * <p>
	 */
	public void run() {
		HITONPC hitonpc = null; 
		for (int x = 0; x < n; x++) {
			hitonpc = new HITONPC(x, n, test, dataset);
			List<Integer> blanket = hitonpc.run();
			for(Integer y: blanket){
				outputNetwork.addEdge(x, y);
			}
		}
	}

	public UndirectedGraph getOutputNetwork() {
		return outputNetwork;
	}

}
