package mn.learn.ib.algs.ibmap;

import mn.rep.UndirectedGraph;

/**
 * @author fschluter This is a simple data structure for storing the IB-scores
 *         of undirected graphs
 */
public class UndirectedGraphScore {

	private UndirectedGraph undirectedGraph;
	private double[][] markovBlanketClosureScores;
	private double[] variableScores;
	private double totalScore;
	private long testCounts;
	private long weightedTestCounts;

	public UndirectedGraphScore(UndirectedGraph undirectedGraph) {
		this.undirectedGraph = undirectedGraph;
		int n = undirectedGraph.getNumberOfNodes();
		markovBlanketClosureScores = new double[n][n];
		variableScores = new double[n];
	}

	public UndirectedGraph getUndirectedGraph() {
		return undirectedGraph;
	}

	public void setUndirectedGraph(UndirectedGraph undirectedGraph) {
		this.undirectedGraph = undirectedGraph;
	}

	public double[][] getMarkovBlanketClosureScores() {
		return markovBlanketClosureScores;
	}

	public void setMarkovBlanketClosureScores(
			double[][] markovBlanketClosureScores) {
		this.markovBlanketClosureScores = markovBlanketClosureScores;
	}

	public double getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(double totalScore) {
		this.totalScore = totalScore;
	}

	public double getVariableScore(int i) {
		return variableScores[i];
	}

	public void setVariableScore(int i, double variableScore) {
		variableScores[i] = variableScore;
	}

	public long getTestCounts() {
		return testCounts;
	}

	public long getWeightedTestCounts() {
		return weightedTestCounts;
	}

	public void setTestCounts(long testCounts) {
		this.testCounts = testCounts;
	}

	public void setWeightedTestCounts(long weightedTestCounts) {
		this.weightedTestCounts = weightedTestCounts;
	}

}
