package mn.learn.ib.algs.ibmap;

import java.util.List;

/**
 * @author fschluter This is a simple data structure for storing the IB-scores
 *         of undirected graphs
 */
public class MarkovBlanketScore {

	/**
	 * Variable of the domain whose Markov blanket score is computed 
	 */
	private int targetVariable;
	
	/**
	 * Variables in the Markov blanket of the target variable
	 */
	private List<Integer> blanket;

	/**
	 * Number of variables in the domain
	 * */
	private int n; 
	private double[] markovBlanketClosureScores;
	private double totalScore;

	public MarkovBlanketScore(int targetVariable, List<Integer> blanket, int n) {
		this.targetVariable = targetVariable;
		this.blanket = blanket;
		this.n = n;
		markovBlanketClosureScores = new double[n];
	}

	/**
	 * @return the targetVariable
	 */
	public int getTargetVariable() {
		return targetVariable;
	}

	/**
	 * @param targetVariable the targetVariable to set
	 */
	public void setTargetVariable(int targetVariable) {
		this.targetVariable = targetVariable;
	}

	/**
	 * @return the blanket
	 */
	public List<Integer> getBlanket() {
		return blanket;
	}

	/**
	 * @param blanket the blanket to set
	 */
	public void setBlanket(List<Integer> blanket) {
		this.blanket = blanket;
	}

	/**
	 * @return the n
	 */
	public int getN() {
		return n;
	}

	/**
	 * @param n the n to set
	 */
	public void setN(int n) {
		this.n = n;
	}

	/**
	 * @return the markovBlanketClosureScores
	 */
	public double[] getMarkovBlanketClosureScores() {
		return markovBlanketClosureScores;
	}

	/**
	 * @param markovBlanketClosureScores the markovBlanketClosureScores to set
	 */
	public void setMarkovBlanketClosureScores(double[] markovBlanketClosureScores) {
		this.markovBlanketClosureScores = markovBlanketClosureScores;
	}

	/**
	 * @return the totalScore
	 */
	public double getTotalScore() {
		return totalScore;
	}

	/**
	 * @param totalScore the totalScore to set
	 */
	public void setTotalScore(double totalScore) {
		this.totalScore = totalScore;
	}

	

}
