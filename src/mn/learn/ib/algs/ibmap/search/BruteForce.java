package mn.learn.ib.algs.ibmap.search;

import java.util.List;

import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.rep.UndirectedGraph;
import data.Dataset;

/**
 * @author fschluter
 *
 */
public class BruteForce {
	/**
	 * The number of variables of the domain
	 */
	private int n;
	
	/**
	 * The output network learned by IBMAP-HC
	 */
	private UndirectedGraphScore outputNetwork;

	/**
	 * The training dataset used in the algorithm to learn the independence
	 * structure
	 */
	private Dataset dataset;

	/**
	 * An object for computing the score of structures. Possible instances are:
	 * ib.ibmap.score.IBScore, ib.ibmap.score.RIBScore,
	 * 
	 */
	private GraphScorable scoringFunction;

	/**
	 * @param n
	 *            Number of variables of the domain
	 * @param test
	 *            Statististical independence test instance
	 * @param dataset
	 *            Dataset instance
	 * @param greedy2 
	 */
	public BruteForce(int n, Dataset dataset, GraphScorable scoringFunction) {
		this.n = n;
		this.dataset = dataset;
		this.scoringFunction = scoringFunction;
	}

	/**
	 * Brute force search over the space of all the undirected structures with n nodes
	 */
	public void run() {
		// Computing the score of the empty structure
		double bestScore = Integer.MIN_VALUE;

		List<UndirectedGraph> allPossibleGraphs = UndirectedGraph.getAllPossibleGraphs(n);
		for (UndirectedGraph g : allPossibleGraphs) {
			UndirectedGraphScore gScore = scoringFunction.computeScore(g, dataset);
			double score = gScore.getTotalScore();
			if(score>bestScore){
				bestScore = score;
				outputNetwork = gScore;
			}
		}
		
	}

	/**
	 * @return
	 */
	public UndirectedGraph getOutputNetwork() {
		return outputNetwork.getUndirectedGraph();
	}

	public double getBestScore() {
		return outputNetwork.getTotalScore();
	}
}
