package mn.learn.ib.algs.ibmap.search;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mn.learn.ib.algs.ibmap.MarkovBlanketScore;
import mn.learn.ib.algs.ibmap.score.BlanketScorable;
import mn.rep.UndirectedGraph;
import data.Dataset;

/**
 * @author fschluter
 *
 */
public class LocalHillClimbing {
	/**
	 * The number of variables of the domain
	 */
	private int n;

	/**
	 * The output network learned by IBMAP-HC
	 */
	private List<Integer> outputBlanket;

	/**
	 * The training dataset used in the algorithm to learn the independence
	 * structure
	 */
	private Dataset dataset;

	/**
	 * An object for computing the score of the possible blankets.
	 * 
	 */
	private BlanketScorable scoringFunction;

	/**
	 * Hill climbing search can use the greedy operator for selecting
	 * heuristically the neighbor (true) or selecting naively the best neighbor
	 * one edge away
	 */
	private boolean greedy = true;

	/**
	 * Represents the score of the best structure found by the optimization
	 */
	private double bestScore;

	/**
	 * Writes debug info about the optimization
	 */
	private BufferedWriter writer = null;

	/**
	 * The true graph is known when it is used with synthetic data
	 */
	private UndirectedGraph trueGraph = null;

	private UndirectedGraph outputNetwork;

	/**
	 * @param n
	 *            Number of variables of the domain
	 * @param test
	 *            Statististical independence test instance
	 * @param dataset
	 *            Dataset instance
	 * @param greedy2
	 */
	public LocalHillClimbing(int n, Dataset dataset,
			BlanketScorable scoringFunction, boolean greedy) {
		this.n = n;
		this.dataset = dataset;
		this.scoringFunction = scoringFunction;
		this.greedy = greedy;
	}

	/**
	 * Simple implementation of pseudocode shown in (Schluter, et al., 2013)
	 * 
	 * @throws IOException
	 */
	public void run() throws IOException {
		outputNetwork = new UndirectedGraph(n);
		for (int x = 0; x < n; x++) {
			List<Integer> blanketOfX = learnBlanketOfX(x);
			for (Integer y : blanketOfX) {
				outputNetwork.addEdge(x, y); // OR assumption
			}
		}
	}

	private List<Integer> learnBlanketOfX(int x) throws IOException {
		// Computing the score of the empty blanket
		List<Integer> b = new ArrayList<Integer>();
		MarkovBlanketScore bScore = scoringFunction.computeScore(x, b, n,
				dataset);
		double currentScore = bScore.getTotalScore();
		do {
			// Selecting the best neighbor blanket
			MarkovBlanketScore neighbor = selectBestNeighbor(x, b, bScore);
			if (neighbor.getTotalScore() <= currentScore) {
				/* Local maxima reached. Termination condition. */
				if (writer != null) {
					debug(x, b, currentScore);
					writer.close();
				}
				outputBlanket = b;
				bestScore = currentScore;
				return b;
			}
			b = neighbor.getBlanket();
			currentScore = neighbor.getTotalScore();
			bScore = neighbor;
			if (writer != null) {
				debug(x, b, currentScore);
			}
		} while (true);
	}

	/**
	 * Writes debug info about the optimization process
	 * 
	 * @param b
	 * @param currentScore
	 * @throws IOException
	 */
	private void debug(int x, List<Integer> b, double currentScore)
			throws IOException {
		int hd = -1;
		int fp = -1;
		int fn = -1;

		if (trueGraph != null) {
			hd = hammingDistance(x, b);
			fp = falsePositives(x, b);
			fn = falseNegatives(x, b);
		}
		writer.write(dataset.getDatasetName() + " " + dataset.getDatasetName()
				+ " " + dataset.getExamples().size() + " "
				+ scoringFunction.getScoringFunctionName() + " "
				+ getBitsetString(b) + " " + currentScore + " " + hd + " " + fp
				+ " " + fn + "\n");
	}

	private String getBitsetString(List<Integer> b) {
		String bitset = "";
		for (int y = 0; y < n; ++y) {
			bitset += b.contains(y) ? "1" : "0";
		}

		return bitset;
	}

	private int falseNegatives(int x, List<Integer> b) {
		int falseNegatives = 0;
		for (int y = 0; y < n; ++y) {
			if (!b.contains(y) && trueGraph.existEdge(x, y))
				++falseNegatives;
		}
		return falseNegatives;
	}

	private int falsePositives(int x, List<Integer> b) {
		int falsePositives = 0;
		for (int y = 0; y < n; ++y) {
			if (b.contains(y) && !trueGraph.existEdge(x, y))
				++falsePositives;
		}
		return falsePositives;
	}

	private int hammingDistance(int x, List<Integer> b) {
		return falsePositives(x, b) + falseNegatives(x, b);
	}

	/**
	 * Returns a the neighbour structure flippng the edge that maximizes the
	 * IB-Score
	 * 
	 * @param bScore
	 * 
	 * @param g
	 * @param gScore
	 * @return
	 */
	protected MarkovBlanketScore selectBestNeighbor(int x, List<Integer> b,
			MarkovBlanketScore bScore) {
		if (greedy)
			return selectGreedyNeighbor(x, b, bScore);
		else
			return selectLocalNeighbor(x, b);
	}

	/**
	 * @param g
	 * @return
	 */
	private MarkovBlanketScore selectGreedyNeighbor(int x, List<Integer> b,
			MarkovBlanketScore bScore) {

		int bestY = 0;
		double minClosureScore = Integer.MAX_VALUE;
		// Obtaining scores of g, computed in previous iteration
		double[] markovBlanketClosureScores = bScore
				.getMarkovBlanketClosureScores();
		for (int y = 0; y < n; ++y) {
			if (x != y) {
				// Searching the test with the lowest
				// contribution to the current score
				if (markovBlanketClosureScores[y] < minClosureScore) {
					minClosureScore = markovBlanketClosureScores[y];
					bestY = y;
				}
			}
		}

		List<Integer> neighbor = new ArrayList<Integer>(b);
		if (b.contains(bestY))
			neighbor.remove(bestY);
		else
			neighbor.add(bestY);

		MarkovBlanketScore neighborScore = scoringFunction.computeScore(x,
				neighbor, n, dataset);

		return neighborScore;
	}

	/**
	 * @param g
	 * @param bScore
	 * @return
	 */
	private MarkovBlanketScore selectLocalNeighbor(int x, List<Integer> b) {
		double maxScore = Integer.MIN_VALUE;
		MarkovBlanketScore neighborWithBestScore = null;
		for (Integer i = 0; i < n - 1; ++i) {
			if (i != x) {
				List<Integer> neighbor = new ArrayList<Integer>(b);
				if (b.contains(i))
					neighbor.remove(i);
				else
					neighbor.add(i);
				MarkovBlanketScore neighborScore = scoringFunction
						.computeScore(x, neighbor, n, dataset);
				if (neighborScore.getTotalScore() > maxScore) {
					maxScore = neighborScore.getTotalScore();
					neighborWithBestScore = neighborScore;
				}
			}
		}
		return neighborWithBestScore;
	}

	/**
	 * @return
	 */
	public UndirectedGraph getOutputNetwork() {
		return outputNetwork;
	}

	/**
	 * @return
	 */
	public double getBestScore() {
		return bestScore;
	}

	public void setDebugFile(BufferedWriter writer) {
		this.writer = writer;
	}

	public void setTrueGraph(UndirectedGraph trueGraph) {
		this.trueGraph = trueGraph;
	}

}
