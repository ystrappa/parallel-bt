package mn.learn.ib.algs.ibmap.algs;

import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.rep.UndirectedGraph;
import data.Dataset;

/**
 * Simple implementation of pseudocode shown in (Schluter, et al., 2013)
 * 
 * @author fschluter
 */
public class IBMAPHC {
	/**
	 * The number of variables of the domain
	 */
	private int n;

	/**
	 * The output network learned by IBMAP-HC
	 */
	private UndirectedGraph outputNetwork;

	/**
	 * The training dataset used in the algorithm to learn the independence
	 * structure
	 */
	private Dataset dataset;

	/**
	 * @param n
	 *            Number of variables of the domain
	 * @param test
	 *            Statististical independence test instance
	 * @param dataset
	 *            Dataset instance
	 */
	public IBMAPHC(int n, Dataset dataset) {
		this.n = n;
		this.dataset = dataset;
	}

	/**
	 * Simple implementation of pseudocode shown in (Schluter, et al., 2013) It
	 * uses a heuristic Hill - climbing search starting from the empty
	 * structure, to maximize the IB-score function.
	 */
	public void run() {
		// Computing the score of the empty structure
		UndirectedGraph g = new UndirectedGraph(n);
		IBScore ibScore = new IBScore();
		UndirectedGraphScore gScore = ibScore.computeScore(g, dataset);
		double currentScore = gScore.getTotalScore();
		double neighborScore = 0;
		do {
			// Selecting the best neighbor structure
			UndirectedGraph neighbor = selectNeighbor(g, gScore);
			// Computing the score of the best neighbor structure
			UndirectedGraphScore nScore = ibScore.computeScore(neighbor,
					dataset);
			neighborScore = nScore.getTotalScore();
			if (neighborScore <= currentScore) {
				// Local maxima reached. Termination condition.
				System.out.println("Local maxima reached in score "
						+ currentScore);
				System.out.println("------------");

				outputNetwork = g;
				return;
			}
			g = neighbor;
			gScore = nScore;
			currentScore = neighborScore;
			System.out.println("Ascending ! neighbor score " + currentScore);
		} while (true);
	}

	/**
	 * Returns a neighbour structure flippng the edge that minimizes σX,Y (G) +
	 * σY,X (G)
	 * 
	 * @param g
	 * @param gScore
	 * @return
	 */
	protected UndirectedGraph selectNeighbor(UndirectedGraph g,
			UndirectedGraphScore gScore) {
		int bestX = 0, bestY = 0;
		double minProduct = Integer.MAX_VALUE;
		// Obtaining scores of g, computed in previous iteration
		double[][] markovBlanketClosureScores = gScore
				.getMarkovBlanketClosureScores();
		UndirectedGraph u = new UndirectedGraph(g);
		for (int i = 0; i < n - 1; ++i) {
			for (int j = i + 1; j < n; ++j) {
				double sum = markovBlanketClosureScores[i][j]
						+ markovBlanketClosureScores[j][i];
				// Obtaining the pair of tests whose product adds the lowest
				// value to the current score
				if (sum < minProduct) {
					minProduct = sum;
					bestX = i;
					bestY = j;
				}
			}
		}
		// Flipping the worst edge
		u.flipEdge(bestX, bestY);
		return u;
	}

	/**
	 * @return
	 */
	public UndirectedGraph getOutputNetwork() {
		return outputNetwork;
	}

}
