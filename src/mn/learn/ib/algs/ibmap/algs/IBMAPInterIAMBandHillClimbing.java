package mn.learn.ib.algs.ibmap.algs;

import java.util.ArrayList;
import java.util.List;

import mn.learn.ib.algs.ibmap.MarkovBlanketScore;
import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.algs.ibmap.score.BlanketScorable;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.learn.ib.algs.ibmap.score.LocalIBScore;
import mn.learn.ib.algs.ibmap.score.RIBScore;
import mn.rep.UndirectedGraph;
import data.Dataset;

/**
 * This method optimizes the Marginal Pseudo Likelihood scoring function for
 * Markov networks structure discover, as proposed in (Pensar et al, 2014),
 * Section 5.
 * 
 * @author fschluter
 *
 */
public class IBMAPInterIAMBandHillClimbing {

	/**
	 * The number of variables of the domain
	 */
	private int n;

	/**
	 * The output network learned by IBMAP-HC
	 */
	private UndirectedGraph outputNetwork;

	/**
	 * The training dataset used in the algorithm to learn the independence
	 * structure
	 */
	private Dataset dataset;

	/**
	 * Equivalent sample size that adjust the strength of the prior
	 * */
	public double equivalentSampleSize = 1;

	/**
	 * Determines if the score for structures can be computed incrementally from
	 * other structures
	 */
	public boolean incremental = true;

	/**
	 * Specifies if the second step for solving inconsistencies is made with IB-Score 
	 * or with MB-RIB scoring function.
	 * values available are {"ibscore","ribscore"}
	 */
	private String scoringFunctionString = "ibscore";

	public IBMAPInterIAMBandHillClimbing(int n, Dataset dataset,
			boolean incremental, String scoringFunctionString) {
		this.n = dataset.getNumberOfVariables();
		outputNetwork = new UndirectedGraph(n);
		this.dataset = dataset;
	}

	/**
	 * Run the algorithm
	 */
	public void run() {
		/*
		 * First phase: learning each Markov blanket with interleaved IAMB
		 * strategy and local MPL score
		 */
		List<List<Integer>> blankets = new ArrayList<List<Integer>>();
		for (int x = 0; x < n; x++) {
			blankets.add(interIAMB(x));
		}

		/*
		 * Second phase: learning a structure with hill climbing and global MPL
		 * score, only in the space of inconsistent edges
		 */
		outputNetwork = graphHillClimbing(blankets);
	}

	/**
	 * Learns the Markov blanket of a variable with interleaved IAMB strategy
	 * and local MPL score. Follows the pseudocode of Algorithm 1 of (Pensar, et
	 * al; 2014). Pensar, Johan, et al.
	 * "Marginal Pseudo-Likelihood Learning of Markov Network structures." arXiv
	 * preprint arXiv:1401.4988 (2014).
	 * 
	 * @param x
	 *            The target variable
	 * @return The Markov blanket of x
	 */
	private List<Integer> interIAMB(int x) {
		List<Integer> mb = new ArrayList<Integer>();
		List<Integer> potential_mb = new ArrayList<Integer>();

		boolean potentialMBHasChanged = true;
		while (potentialMBHasChanged) {

			List<Integer> complement = dataset.getDomainVariables();
			complement.remove((Integer) x);
			complement.removeAll(mb);

			mb = new ArrayList<Integer>(potential_mb);

			potentialMBHasChanged = false;
			for (Integer i : complement) {
				List<Integer> mbUnionI = new ArrayList<Integer>(mb);
				mbUnionI.add(i);

				if (localIBScore(x, new ArrayList<Integer>(mbUnionI)) > localIBScore(x, new ArrayList<Integer>(potential_mb))) {
					potential_mb = new ArrayList<Integer>(mbUnionI);
					potentialMBHasChanged = true;
				}
			}

			boolean removalIncreasesScore = true;
			while (removalIncreasesScore && potential_mb.size() > 2) {
				removalIncreasesScore = false;
				mb = new ArrayList<Integer>(potential_mb);
				for (Integer i : mb) {
					List<Integer> mbWithoutI = new ArrayList<Integer>(mb);
					mbWithoutI.remove(i);

					if (localIBScore(x, mbWithoutI) > localIBScore(x, potential_mb)) {
						potential_mb = new ArrayList<Integer>(mbWithoutI);
						removalIncreasesScore = true;
					}
				}
			}

		}

		return potential_mb;
	}

	
	/**
	 * @param i
	 * @param blanketOfX_i
	 * @return
	 */
	private double localIBScore(int i, List<Integer> blanketOfX_i) {
		BlanketScorable scoringFunction = new LocalIBScore();
		MarkovBlanketScore bScore = scoringFunction.computeScore(i, blanketOfX_i, n,
				dataset);
		return bScore.getTotalScore();
	}

	/**
	 * Learns the independence structure with a hill climbing search, using as
	 * objective function the global MPL score, only in the space of
	 * inconsistent edges. Follows the pseudocode of Algorithm 2 of (Pensar, et
	 * al; 2014). Pensar, Johan, et al.
	 * "Marginal Pseudo-Likelihood Learning of Markov Network structures." arXiv
	 * preprint arXiv:1401.4988 (2014).
	 * 
	 * @param blankets The Markov blankets learned previously
	 * @return A structure learned with a hill climbing search and MPL scoring function
	 */
	private UndirectedGraph graphHillClimbing(List<List<Integer>> blankets) {
		UndirectedGraph g = new UndirectedGraph(n);
		UndirectedGraph potentialG = new UndirectedGraph(n);
		
		GraphScorable scoringFunction = null; 
		if(scoringFunctionString.equals("ibscore")){
			scoringFunction = new IBScore();
		}else if(scoringFunctionString.equals("ribscore")){
			scoringFunction = new RIBScore(); // default
		}

		boolean poteintialGHasChanged = true;
		while (poteintialGHasChanged) {

			g = new UndirectedGraph(potentialG);
			poteintialGHasChanged = false;
			UndirectedGraphScore gScore = scoringFunction.computeScore(g,
					dataset);
			UndirectedGraphScore potentialGScore = scoringFunction
					.computeScore(potentialG, gScore, dataset);

			for (int i = 0; i < n - 1; ++i) {
				for (int j = i + 1; j < n; ++j) {
					if ( blankets.get(i).contains(j)
							|| blankets.get(j).contains(i)  ) {
						UndirectedGraph neighbor = new UndirectedGraph(g);
						neighbor.flipEdge(i, j);
						UndirectedGraphScore neighborScore = incremental ? scoringFunction
								.computeScore(neighbor, gScore, dataset)
								: scoringFunction.computeScore(neighbor,
										dataset);

						if (neighborScore.getTotalScore() > potentialGScore
								.getTotalScore()) {
							potentialG = new UndirectedGraph(neighbor);
							poteintialGHasChanged = true;
						}
					}
				}
			}

		}
		return potentialG;
	}

	public UndirectedGraph getOutputNetwork() {
		return outputNetwork;
	}

}
