package mn.learn.ib.algs.ibmap.score;

import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.rep.UndirectedGraph;
import data.Dataset;

/**
 * @author fschluter
 * 
 */
public interface Scorable {

	/**
	 * 
	 * 
	 * @param g
	 * @param test
	 * @param dataset
	 * @return
	 */
	public UndirectedGraphScore computeScore(UndirectedGraph g, Dataset dataset);
}
