package mn.learn.ib.algs.ibmap.score;


import java.util.ArrayList;
import java.util.List;

import data.Dataset;
import math.Permutations;
import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.it.BayesianTest;
import mn.learn.ib.it.Gamma;
import mn.learn.ib.it.SimulatedBayesianTest;
import mn.rep.UndirectedGraph;


/**
 * @author fschluter
 * 
 */
public class MPL implements GraphScorable {
	/* Equivalent sample size that adjust the strength of the prior */
	public double equivalentSampleSize = 1;

	
	/**
	 * Marginal Pseudo Likelihood scoring function proposed at (Pensar et. al., 2014) 
	 * 
	 * @param g
	 *            The structure we want to score
	 * @return The score of the structure
	 */
	public UndirectedGraphScore computeScore(UndirectedGraph g, Dataset dataset) {
		double score = 0;
		UndirectedGraphScore gScore = new UndirectedGraphScore(g);
		
		/* for each variable of the domain */ 
		for (int i = 0; i < g.getNumberOfNodes(); ++i) {
			double variableScore = computeVariableScore(g, dataset, i);
			gScore.setVariableScore(i, variableScore);
			score += variableScore;
		}

		gScore.setTotalScore(score);
		return gScore;
	}


	@Override
	public UndirectedGraphScore computeScore(UndirectedGraph g,
			UndirectedGraphScore incremental, Dataset dataset) {
		UndirectedGraphScore gScore = new UndirectedGraphScore(g);

		boolean[] differences = g.getVariablesWithDifferentBlanket(incremental
				.getUndirectedGraph());
		double score = 0;
		for (int i = 0; i < differences.length; i++) {
			double variableScore = 0;
			if (differences[i]) {
				variableScore = computeVariableScore(g, dataset, i);
			} else {
				variableScore = incremental.getVariableScore(i);
			}
			gScore.setVariableScore(i, variableScore);
			score += variableScore; 
		}
		gScore.setTotalScore(score);
		return gScore;
	}

	private double computeVariableScore(UndirectedGraph g, Dataset dataset, int i) {
		double variableScore = 0;
		// permutations of current variables X_i
		List<Integer> x_i = new ArrayList<Integer>();
		x_i.add(i);
		List<int[]> permutationsOfX_i = Permutations.getPermutations(x_i, dataset.getCardinalities());

		// permutations of the Markov blanket of the current variables X_i
		List<Integer> blanketOfX_i = g.getBlanketOf(i);
		
		List<int[]> permutationsOfBlanketOfX_i = Permutations.getPermutations(blanketOfX_i, dataset.getCardinalities());

		for (int[] permutationOfBlanketOfX_i : permutationsOfBlanketOfX_i) {
			int countsOfPermutationOfBlanketOfX_i = 0;
			double alpha_i = 0;
			for (int[] permutationOfX_i : permutationsOfX_i) {
				List<Integer> variables = new ArrayList<Integer>();
				variables.addAll(blanketOfX_i);
				variables.addAll(x_i);
				int[] values = new int[permutationOfBlanketOfX_i.length + 1];
				System.arraycopy(permutationOfBlanketOfX_i, 0, values, 0, permutationOfBlanketOfX_i.length);
				values[permutationOfBlanketOfX_i.length] = permutationOfX_i[0];
				int counts = dataset.getCounts(variables, values);
				double alpha = equivalentSampleSize / (permutationsOfBlanketOfX_i.size() * permutationsOfX_i.size()) ;

				countsOfPermutationOfBlanketOfX_i += counts;
				alpha_i += alpha;

				variableScore += Gamma.logGamma(alpha + counts) - Gamma.logGamma(alpha);
			}
			variableScore += Gamma.logGamma(alpha_i) - Gamma.logGamma(alpha_i + countsOfPermutationOfBlanketOfX_i);
		}
		return variableScore;
	}

	@Override
	public String getScoringFunctionName() {
		return "mpl";
	}

}
