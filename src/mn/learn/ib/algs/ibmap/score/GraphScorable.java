package mn.learn.ib.algs.ibmap.score;

import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.rep.UndirectedGraph;
import data.Dataset;

/**
 * @author fschluter
 * 
 */
public interface GraphScorable {

	/**
	 * 
	 * Computes the score of an undirected graph from a dataset
	 * 
	 * @param g
	 * @param test
	 * @param dataset
	 * @return
	 */
	public UndirectedGraphScore computeScore(UndirectedGraph g, Dataset dataset);
	
	/**
	 * Computes the score of an undirected graph from a dataset, 
	 * using the information of a structure whose score has benn computed before, 
	 * in order to save computational costs.
	 * @param g
	 * @param incremental
	 * @param dataset
	 * @return
	 */
	public UndirectedGraphScore computeScore(UndirectedGraph g, UndirectedGraphScore incremental, Dataset dataset);
	
	/**
	 * Returns a name or key string that identifies the scoring function
	 * @return
	 */
	public String getScoringFunctionName();
	
}
