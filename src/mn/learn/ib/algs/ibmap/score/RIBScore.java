package mn.learn.ib.algs.ibmap.score;


import java.util.ArrayList;
import java.util.List;

import data.Dataset;
import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.it.BayesianTest;
import mn.learn.ib.it.IndependenceTriplet;
import mn.rep.UndirectedGraph;
import util.JGraphtUtils;


/**
 * @author fschluter
 * 
 */
public class RIBScore implements GraphScorable {

	private double defaultPrior = .5d;
	private boolean onlyIndependences = false;
	private BayesianTest test; 

	public RIBScore() {
		this(false);
		test = new BayesianTest();

	}
	
	public RIBScore(boolean onlyIndependences) {
		this.onlyIndependences = onlyIndependences;
		test = new BayesianTest();
	}  

	/* (non-Javadoc)
	 * @see mn.learn.ib.algs.ibmap.score.Scorable#computeScore(mn.rep.UndirectedGraph, data.Dataset)
	 */
	public UndirectedGraphScore computeScore(UndirectedGraph g,  Dataset dataset) {
		UndirectedGraphScore gScore = new UndirectedGraphScore(g);
		ArrayList<IndependenceTriplet> closure = getClosureOf(g);
		int n = g.getNumberOfNodes();
		double[][] markovBlanketClosureScores = new double[n][n];
		double score = 0;
		long testCounts = 0;
		long weightedTestCounts = 0;

		for (IndependenceTriplet triplet : closure) {
			int x = triplet.getX().get(0);
			int y = triplet.getY().get(0);
			test.independent(dataset, triplet);
			++testCounts;
			weightedTestCounts+=triplet.getX().size()+triplet.getY().size()+triplet.getZ().size();

			double[] logProbs = test.getLogProbs();
			if (g.existEdge(x, y)) {
				//log probability of dependence model
				score+=logProbs[1];
				markovBlanketClosureScores[x][y] = logProbs[1];
			} else {
				//log probability of independence model
				score+=logProbs[0];
				markovBlanketClosureScores[x][y] = logProbs[0];
			}
		}
		gScore.setMarkovBlanketClosureScores(markovBlanketClosureScores);
		gScore.setTotalScore(score);
		gScore.setTestCounts(testCounts);
		gScore.setWeightedTestCounts(weightedTestCounts);

		return gScore;
	}

	/* (non-Javadoc)
	 * @see mn.learn.ib.algs.ibmap.score.Scorable#computeScore(mn.rep.UndirectedGraph, mn.learn.ib.algs.ibmap.UndirectedGraphScore, data.Dataset)
	 */
	public UndirectedGraphScore computeScore(UndirectedGraph g, UndirectedGraphScore incremental, Dataset dataset) {
		return computeScore(g, dataset); //TODO: WRITE PSEUDO CODE
	}
	
	/**
	 * This method return the Markov blanket closure of graph g
	 * 
	 * @param g
	 * @return
	 */
	private ArrayList<IndependenceTriplet> getClosureOf(UndirectedGraph g) {
		
		String[] sortedNodes = JGraphtUtils.sortNodesByDegreeInAscendingOrder(g);
		
		ArrayList<IndependenceTriplet> closure = new ArrayList<IndependenceTriplet>();
		for (int x = 0; x < g.getNumberOfNodes() -1 ; ++x) {
			for (int y = x+1; y < g.getNumberOfNodes(); ++y) {
				if (x != y) {
					List<Integer> blanketOfX = g.getBlanketOf(new Integer(sortedNodes[x]));
					blanketOfX.remove(new Integer(sortedNodes[y]));
					IndependenceTriplet triplet = new IndependenceTriplet(new Integer(sortedNodes[x]), new Integer(sortedNodes[y]), blanketOfX);

					/*
					 * when onlyIndependences is true, the closure only contains
					 * independence assertions when onlyIndependences is false, the
					 * closure contains independence and dependence assertions
					 */
					if (!onlyIndependences
							|| (onlyIndependences && !g.existEdge(new Integer(sortedNodes[x]), new Integer(sortedNodes[y])))) {
						closure.add(triplet);
					}
				}
			}
		}
		return closure;
	}
	
	public void setTest(BayesianTest test) {
		this.test = test;
	}

	@Override
	public String getScoringFunctionName() {
		return "ribscore";
	}
}
