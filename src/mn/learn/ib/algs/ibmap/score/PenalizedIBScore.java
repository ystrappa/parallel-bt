package mn.learn.ib.algs.ibmap.score;

import java.util.ArrayList;
import java.util.List;

import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.it.BayesianTest;
import mn.learn.ib.it.IndependenceTriplet;
import mn.rep.UndirectedGraph;
import data.Dataset;

/**
 * @author fschluter
 * 
 */
public class PenalizedIBScore implements GraphScorable {

	private boolean onlyIndependences = false;

	public PenalizedIBScore() {
		this(false);
	}

	public PenalizedIBScore(boolean onlyIndependences) {
		this.onlyIndependences = onlyIndependences;
	}

	/**
	 * Computing the IB-score of a graph g. This is the product of the
	 * posteriors of all the assertions in the Markov blanket closure
	 * 
	 * @param g
	 *            The structure we want to score
	 * @return The score of the structure
	 */
	public UndirectedGraphScore computeScore(UndirectedGraph g, Dataset dataset) {
		BayesianTest test = new BayesianTest();
		UndirectedGraphScore gScore = new UndirectedGraphScore(g);
		// ArrayList<IndependenceTriplet> closure = getClosureOfStructure(g);
		int n = g.getNumberOfNodes();
		double[][] markovBlanketClosureScores = new double[n][n];
		double score = 0;

		for (int i = 0; i < g.getNumberOfNodes(); ++i) {
			ArrayList<IndependenceTriplet> closureOfX = getClosureOfVariable(g,
					i);
			for (IndependenceTriplet triplet : closureOfX) {
				test.independent(dataset, triplet);
				int x = triplet.getX().get(0);
				int y = triplet.getY().get(0);
				double[] logProbs = test.getLogProbs();
				if (g.existEdge(x, y)) {
					int penalizedScore = 0;
					if (logProbs[1] > Math.log(0.6))
						penalizedScore = 1;
					else if (logProbs[1] < Math.log(0.4))
						penalizedScore = -1;
					else
						penalizedScore = 0;

					score += penalizedScore;
					markovBlanketClosureScores[x][y] = penalizedScore;
				} else {
					// log probability of independence model
					int penalizedScore = 0;
					if (logProbs[0] > Math.log(0.6))
						penalizedScore = 1;
					else if (logProbs[0] < Math.log(0.4))
						penalizedScore = -1;
					else
						penalizedScore = 0;

					score += penalizedScore;
					markovBlanketClosureScores[x][y] = penalizedScore;
				}
			}

		}

		gScore.setMarkovBlanketClosureScores(markovBlanketClosureScores);
		gScore.setTotalScore(score); //normalizing
		return gScore;
	}

	private ArrayList<IndependenceTriplet> getClosureOfVariable(
			UndirectedGraph g, int x) {
		ArrayList<IndependenceTriplet> closureOfX = new ArrayList<IndependenceTriplet>();
		for (int y = 0; y < g.getNumberOfNodes(); ++y) {
			if (x != y) {
				List<Integer> blanketOfX = g.getBlanketOf(x);
				blanketOfX.remove((Integer) y);
				IndependenceTriplet triplet = new IndependenceTriplet(x, y,
						blanketOfX);

				/*
				 * when onlyIndependences is true, the closure only contains
				 * independence assertions when onlyIndependences is false, the
				 * closure contains independence and dependence assertions
				 */
				if (!onlyIndependences
						|| (onlyIndependences && !g.existEdge(x, y))) {
					closureOfX.add(triplet);
				}
			}
		}
		return closureOfX;
	}

	@Override
	public UndirectedGraphScore computeScore(UndirectedGraph g,
			UndirectedGraphScore incremental, Dataset dataset) {
		int n = g.getNumberOfNodes();
		BayesianTest test = new BayesianTest();
		UndirectedGraphScore gScore = new UndirectedGraphScore(g);
		boolean[] differences = g.getVariablesWithDifferentBlanket(incremental
				.getUndirectedGraph());
		double[][] markovBlanketClosureScores = new double[n][n];
		double score = 0;
		for (int i = 0; i < differences.length; i++) {
			if (differences[i]) {
				ArrayList<IndependenceTriplet> closureOfVariable = getClosureOfVariable(
						g, i);
				for (IndependenceTriplet triplet : closureOfVariable) {
					int x = triplet.getX().get(0);
					int y = triplet.getY().get(0);
					test.independent(dataset, triplet);
					double[] logProbs = test.getLogProbs();
					if (g.existEdge(x, y)) {
						int penalizedScore = 0;
						if (logProbs[1] > Math.log(0.6))
							penalizedScore = 1;
						else if (logProbs[1] < Math.log(0.4))
							penalizedScore = -1;
						else
							penalizedScore = 0;

						score += penalizedScore;
						markovBlanketClosureScores[x][y] = penalizedScore;
					} else {
						// log probability of independence model
						int penalizedScore = 0;
						if (logProbs[0] > Math.log(0.6))
							penalizedScore = 1;
						else if (logProbs[0] < Math.log(0.4))
							penalizedScore = -1;
						else
							penalizedScore = 0;

						score += penalizedScore;
						markovBlanketClosureScores[x][y] = penalizedScore;
					}
				}
			} else {
				for (int j = 0; j < n; j++) {
					score += incremental.getMarkovBlanketClosureScores()[i][j];
					markovBlanketClosureScores[i][j] = incremental
							.getMarkovBlanketClosureScores()[i][j];
				}
			}
		}
		gScore.setMarkovBlanketClosureScores(markovBlanketClosureScores);
		gScore.setTotalScore(score);
		return gScore;
	}
	
	@Override
	public String getScoringFunctionName() {
		return "pibscore";
	}
}
