package mn.learn.ib.algs.ibmap.score;

import java.util.ArrayList;
import java.util.List;

import mn.learn.ib.algs.ibmap.MarkovBlanketScore;
import mn.learn.ib.it.BayesianTest;
import mn.learn.ib.it.IndependenceTriplet;
import data.Dataset;

/**
 * Local IB-score computes the score only 
 * for the Markov blanket of a specific variable
 * @author fschluter
 * 
 */
public class LocalIBScore implements BlanketScorable {

	private BayesianTest test; 

	public LocalIBScore() {
		this(false);
		test = new BayesianTest();
	}
	
	public LocalIBScore(boolean onlyIndependences) {
		test = new BayesianTest();
	}  


	@Override
	public MarkovBlanketScore computeScore(int x, List<Integer> blanket, int n,
			Dataset dataset) {

		MarkovBlanketScore bScore = new MarkovBlanketScore(x,blanket,n);
		double[] markovBlanketClosureScores = new double[n];
		double score = 0;

			ArrayList<IndependenceTriplet> closureOfX = getClosureOfVariable(
					x,blanket,n);
			for (IndependenceTriplet triplet : closureOfX) {
				test.independent(dataset, triplet);
				int y = triplet.getY().get(0);
				double[] logProbs = test.getLogProbs();
				if (blanket.contains(y)) {
//					// log probability of dependence model
					score += logProbs[1];
					markovBlanketClosureScores[y] = logProbs[1];
				} else {
					// log probability of independence model
					score += logProbs[0];
					markovBlanketClosureScores[y] = logProbs[0];
				}
			}

		bScore.setMarkovBlanketClosureScores(markovBlanketClosureScores);
		bScore.setTotalScore(score);
		return bScore;
	}

	private ArrayList<IndependenceTriplet> getClosureOfVariable(
			int x, List<Integer> blanket, int n) {
		List<Integer> z = new ArrayList<Integer>(blanket);
		ArrayList<IndependenceTriplet> closureOfX = new ArrayList<IndependenceTriplet>();
		for (int y = 0; y < n; ++y) {
			if (x != y) {
				z.remove((Integer) y);
				IndependenceTriplet triplet = new IndependenceTriplet(x, y,
						z);
				closureOfX.add(triplet);
			}
		}
		return closureOfX;
	}

	public void setTest(BayesianTest test) {
		this.test = test;
	}

	@Override
	public String getScoringFunctionName() {
		return "ibscore";
	}
}
