package mn.learn.ib.algs.ibmap.score;

import java.util.List;

import mn.learn.ib.algs.ibmap.MarkovBlanketScore;
import data.Dataset;

/**
 * @author fschluter
 * 
 */
public interface BlanketScorable {

	/**
	 * 
	 * Computes the score of the Markov blanket of a variable from a dataset
	 * 
	 * @param g
	 * @param test
	 * @param dataset
	 * @return
	 */
	public MarkovBlanketScore computeScore(int x, List<Integer>blanket, int n, Dataset dataset);
	
	/**
	 * Returns a name or key string that identifies the scoring function
	 * @return
	 */
	public String getScoringFunctionName();
	
}
