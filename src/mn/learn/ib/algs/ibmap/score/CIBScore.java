package mn.learn.ib.algs.ibmap.score;

import java.util.ArrayList;
import java.util.List;

import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.it.BayesianTest;
import mn.learn.ib.it.IndependenceTriplet;
import mn.rep.UndirectedGraph;
import data.Dataset;

/**
 * Composed closure using sets: one independence assertion 
 * between each variable with all the non neighbors conditioning 
 * in the neighbors and one dependence assertion 
 * between each variable and all its neighbors. 
 * In general the performance of this scoring funcion is not good. 
 * It requieres a lot of data.
 * @author fschluter
 * 
 */
public class CIBScore implements GraphScorable {

	/**
	 * Computing the IB-score of a graph g. This is the product of the
	 * posteriors of all the assertions in the Markov blanket closure
	 * 
	 * @param g
	 *            The structure we want to score
	 * @return The score of the structure
	 */
	public UndirectedGraphScore computeScore(UndirectedGraph g, Dataset dataset) {
		BayesianTest test = new BayesianTest();
		UndirectedGraphScore gScore = new UndirectedGraphScore(g);
		// ArrayList<IndependenceTriplet> closure = getClosureOfStructure(g);
		int n = g.getNumberOfNodes();
		double[][] markovBlanketClosureScores = new double[n][n];
		double score = 0;

		for (int i = 0; i < g.getNumberOfNodes(); ++i) {
			List<IndependenceTriplet> closureOfX = getClosureOfVariable(g,
					i);
			for (IndependenceTriplet triplet : closureOfX) {
				test.independent(dataset, triplet);
				int x = triplet.getX().get(0);
				int y = triplet.getY().get(0);
				double[] logProbs = test.getLogProbs();
				if (g.existEdge(x, y)) {
					// log probability of dependence model
					score += logProbs[1];
					markovBlanketClosureScores[x][y] = logProbs[1];
				} else {
					// log probability of independence model
					score += logProbs[0];
					markovBlanketClosureScores[x][y] = logProbs[0];
				}
			}

		}

		gScore.setMarkovBlanketClosureScores(markovBlanketClosureScores);
		gScore.setTotalScore(score);
		return gScore;
	}

	private List<IndependenceTriplet> getClosureOfVariable(UndirectedGraph g,
			int x) {
		List<IndependenceTriplet> closureOfX = new ArrayList<IndependenceTriplet>();

		List<Integer> xList = new ArrayList<Integer>();
		xList.add(x);
		List<Integer> independentYList = new ArrayList<Integer>();
		List<Integer> blanketOfX = g.getBlanketOf(x);

		for (int y = 0; y < g.getNumberOfNodes(); ++y) {
			if (x != y) {
				if (!g.existEdge(x, y)) {
					independentYList.add(y);
				}
			}
		}

		IndependenceTriplet tripletIndependence = new IndependenceTriplet(
				xList, independentYList, blanketOfX);
		if(independentYList.size()>0) closureOfX.add(tripletIndependence);
		
		IndependenceTriplet tripletDependence = new IndependenceTriplet(xList,
				blanketOfX, new ArrayList<Integer>());
		if(blanketOfX.size()>0) closureOfX.add(tripletDependence);

		return closureOfX;
	}

	@Override
	public UndirectedGraphScore computeScore(UndirectedGraph g,
			UndirectedGraphScore incremental, Dataset dataset) {
		int n = g.getNumberOfNodes();
		BayesianTest test = new BayesianTest();
		UndirectedGraphScore gScore = new UndirectedGraphScore(g);
		boolean[] differences = g.getVariablesWithDifferentBlanket(incremental
				.getUndirectedGraph());
		double[][] markovBlanketClosureScores = new double[n][n];
		double score = 0;
		for (int i = 0; i < differences.length; i++) {
			if (differences[i]) {
				List<IndependenceTriplet> closureOfVariable = getClosureOfVariable(
						g, i);
				for (IndependenceTriplet triplet : closureOfVariable) {
					int x = triplet.getX().get(0);
					int y = triplet.getY().get(0);
					test.independent(dataset, triplet);
					double[] logProbs = test.getLogProbs();
					if (g.existEdge(x, y)) {
						// log probability of dependence model
						score += logProbs[1];
						markovBlanketClosureScores[x][y] = logProbs[1];
					} else {
						// log probability of independence model
						score += logProbs[0];
						markovBlanketClosureScores[x][y] = logProbs[0];
					}
				}
			} else {
				for (int j = 0; j < n; j++) {
					score += incremental.getMarkovBlanketClosureScores()[i][j];
					markovBlanketClosureScores[i][j] = incremental
							.getMarkovBlanketClosureScores()[i][j];
				}
			}
		}
		gScore.setMarkovBlanketClosureScores(markovBlanketClosureScores);
		gScore.setTotalScore(score);
		return gScore;
	}

	@Override
	public String getScoringFunctionName() {
		return "cibscore";
	}
}
