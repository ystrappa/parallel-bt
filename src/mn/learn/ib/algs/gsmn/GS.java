package mn.learn.ib.algs.gsmn;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mn.learn.ib.it.IndependenceTriplet;
import mn.learn.ib.it.StatisticalIndependenceTest;
import mn.learn.ib.it.TestOutcome;


import data.Dataset;


/**
 * @author fschluter
 * 
 */
public class GS {

	/**
	 * The statistical independence test instance
	 */
	private StatisticalIndependenceTest test;

	/**
	 * Target variable to learn its Markov blanket
	 */
	int targetVariable;
	/**
	 * The number of variables of the domain
	 */
	private int n;

	/**
	 * The training dataset used in the algorithm to learn the independence
	 * structure
	 */
	private Dataset dataset;

	/**
	 * 
	 */
	private List<Integer> tpc;

	/**
	 * 
	 */
	private List<Integer> open;

	/**
	 * @param T
	 * @param n
	 * @param test
	 * @param dataset
	 */
	public GS(int T, int n, StatisticalIndependenceTest test, Dataset dataset) {
		this.targetVariable = T;
		this.n = n;
		this.test = test;
		this.dataset = dataset;
	}

	/**
	 * Return parents and children of T
	 * 
	 * 
	 */
	public List<Integer> run() {
		// inizialization
		tpc = new ArrayList<Integer>();
		open = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			if (i != targetVariable)
				open.add(i);
		}

		// inclusion heuristic function
		inclusionHeuristic();

		// elimination strategy
		eliminationStrategy();

		// interleaving strategy
		do {
			inclusionHeuristic();
			eliminationStrategy();
		} while (!open.isEmpty());

		return tpc;
	}

	/**
	 * a. sort in descending order the variables X in open according to the
	 * pairwise association with T
	 * <p>
	 * b. remove from open variables with zero association with T (when
	 * I(X,T|empty) )
	 * <p>
	 * c. insert at end of tpc(T) the first variable in open and remove it from
	 * open
	 * <p>
	 * 
	 * @param open
	 * @param s
	 */
	private void inclusionHeuristic() {
		// a
		TestOutcome[] testsOutcomes = new TestOutcome[open.size()];
		for (int i = 0; i < open.size(); ++i) {
			int x = open.get(i);
			IndependenceTriplet triplet = new IndependenceTriplet(x, targetVariable, new ArrayList<Integer>());
			boolean truthValue = test.independent(dataset, triplet);
			double logLikelihood = test.getLoglikelihoods()[1];
			double logProbability = test.getLogProbs()[1];
			testsOutcomes[i] = new TestOutcome(triplet, truthValue, logLikelihood, logProbability);
		}
		Arrays.sort(testsOutcomes, Collections.reverseOrder());

		// b
		open = new ArrayList<Integer>();
		for (int i = 0; i < testsOutcomes.length; ++i) {
			if (!testsOutcomes[i].isTruthValue()) {
				open.add(testsOutcomes[i].getTriplet().getX().get(0));
			}
		}

		// c
		if (!open.isEmpty()) {
			tpc.add(open.get(0));
			open.remove(0);
		}

	}


	/**
	 * <b>if</b>
	 * <p>
	 * open is empty
	 * <p>
	 * for each X in tpc, if exists any subset Z of tpc s.t. I(X,T|Z), remove X
	 * from tpc.
	 * <p>
	 * <b>else</b>
	 * <p>
	 * X <- last variable added to tpc
	 * <p>
	 * if exists any subset Z of tpc s.t. I(X,T|Z), remove X from tpc.
	 * 
	 * @param open
	 * @param s
	 */
	private void eliminationStrategy() {
		if (open.isEmpty()) {
			List<Integer> tpcCopy = new ArrayList<Integer>(tpc);
			for (Integer x : tpcCopy) {
				shrink(x);
			}
		} else {
			int x = tpc.get(tpc.size() - 1);
			shrink(x);

		}
	}

	/**
	 * @param x
	 */
	private void shrink(Integer x) {
		List<Integer> tpcCopyWithoutX = new ArrayList<Integer>(tpc);
		tpcCopyWithoutX.remove(x);
		Set<Set<Integer>> tpcPowerSet = powerSet(new HashSet<Integer>(tpcCopyWithoutX));
		for (Set<Integer> set : tpcPowerSet) {
			IndependenceTriplet triplet = new IndependenceTriplet(x, targetVariable, new ArrayList<Integer>(set));
			boolean truthValue = test.independent(dataset, triplet);
			double logLikelihood = test.getLoglikelihoods()[1];
			double logProbability = test.getLogProbs()[1];
			TestOutcome testOutcome = new TestOutcome(triplet, truthValue, logLikelihood, logProbability);
			if (testOutcome.isTruthValue()) {
				tpc.remove((Object) x);
				break;
			}
		}
	}

	/**
	 * @param <T>
	 * @param originalSet
	 * @return
	 */
	public <T> Set<Set<T>> powerSet(Set<T> originalSet) {
		Set<Set<T>> sets = new HashSet<Set<T>>();
		if (originalSet.isEmpty()) {
			sets.add(new HashSet<T>());
			return sets;
		}
		List<T> list = new ArrayList<T>(originalSet);
		T head = list.get(0);
		Set<T> rest = new HashSet<T>(list.subList(1, list.size()));
		for (Set<T> set : powerSet(rest)) {
			Set<T> newSet = new HashSet<T>();
			newSet.add(head);
			newSet.addAll(set);
			sets.add(newSet);
			sets.add(set);
		}
		return sets;
	}

}
