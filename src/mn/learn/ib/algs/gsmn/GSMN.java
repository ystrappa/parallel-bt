package mn.learn.ib.algs.gsmn;




import java.util.List;

import mn.learn.ib.it.StatisticalIndependenceTest;
import mn.rep.UndirectedGraph;


import data.Dataset;



public class GSMN {
	/**
	 * The number of variables of the domain
	 */
	private int n;
	
	/**
	 * The output network learned by IBMAP-HC 
	 */
	private UndirectedGraph outputNetwork;
	
	/**
	 * The statistical independence test instance
	 */
	private StatisticalIndependenceTest test;
	
	/**
	 * The training dataset used in the algorithm to learn the independence structure 
	 */
	private Dataset dataset;
	
	public GSMN(int n, StatisticalIndependenceTest test, Dataset dataset) {
		this.n = n;
		outputNetwork = new UndirectedGraph(n);
		this.test = test;
		this.dataset = dataset;
	}

	/**
	 */
	public void run() {
		GS gs = null; 
		for (int x = 0; x < n; x++) {
			gs = new GS(x, n, test, dataset);
			List<Integer> blanket = gs.run();
			for(Integer y: blanket){
				outputNetwork.addEdge(x, y);
			}
		}
	}

	public UndirectedGraph getOutputNetwork() {
		return outputNetwork;
	}

}
