package mn.learn.ib.it;

import java.util.List;

import data.Dataset;
import data.SimulatedDataset;

/**
 * Version of Bayesian test for using simulated data
 * 
 * @author fschluter
 * 
 */
public class SimulatedBayesianTest extends BayesianTest {

	/* Vectors of hyperparameters */
	public double[][] _gammaIJ;

	/* Uniform (homogeneous) values for hyperparameters */
	public double _uniformGamma = 1;
	public double _uniformAlpha = 1; // marginal over gamma
	public double _uniformBeta = 1; // marginal over gamma

	/* Prior Probability of conditional independence model */
	public double priorOfConditionalIndependenceModel = 0.5;

	/* Log of the posterior of the conditional dependence model */
	private double logPosteriorDependenceModel;
	/* Log of the posterior of the conditional independence model */
	private double logPosteriorIndependenceModel;
	/* Log of the likelihood of the conditional dependence model */
	private double loglikelihoodDependentModel;
	/* Log of the likelihood of the conditional independence model */
	private double loglikelihoodIndependentModel;

	private double uncertainty = 0.0;

	public SimulatedBayesianTest(double uncertainty) {
		this.uncertainty = uncertainty;
	}

	/**
	 * This method is used to test the independence of a set of variables x
	 * respect to a set of variables y, conditioning in a set of variables z.
	 * 
	 * @param dataset
	 *            An instance of the dataset class
	 * @param x
	 *            A set of random variables
	 * @param y
	 *            A set of random variables
	 * @param z
	 *            A set of random variables
	 * @param i
	 *            The cardinality of all the variables (i.e. 2 for binary
	 *            variables).
	 * @return A decision of Independences of the triplet, given data
	 */
	public boolean independent(SimulatedDataset dataset, List<Integer> x, List<Integer> y, List<Integer> z) {
		IndependenceTriplet triplet = new IndependenceTriplet(x, y, z);
		return independent(dataset, triplet);
	}

	/**
	 * This method is used to test the independence truth value of a triplet
	 * conformed by a set of variables x, a set of variables y, and a
	 * conditioning set of variables z.
	 * 
	 * @param dataset
	 *            An instance of the dataset class
	 * @param x
	 *            A random variable
	 * @param y
	 *            A random variable
	 * @param z
	 *            A set of random variables
	 * @param i
	 *            The cardinality of all the variables (i.e. 2 for binary
	 *            variables).
	 * @return A decision of independence of the triplet, given data
	 */
	public boolean independent(Dataset dataset, IndependenceTriplet triplet) {

		int s = triplet.getZ().size();
		int ds = dataset.getCardinalities()[0]; // here we assume that in the
												// simmulation, all the
												// variables have the same
												// cardinality
		int numberOfSlices = (int) Math.pow(ds, s);

		int D = dataset.getMaxNumberOfExamples();

		double alpha = ((SimulatedDataset)dataset).getTrueGraph().existEdge(triplet.getX().get(0), triplet.getY().get(0)) ? 0 + uncertainty
				: 1 - uncertainty;

		double delta = D / (1 + alpha * (ds - 1));
		double delta_ii = delta / Math.pow(ds, s + 1);
		double notDelta = D - delta;
		double notDelta_ij = notDelta / (Math.pow(ds, s + 2) - Math.pow(ds, s + 1));

		/*
		 * two different tables for the same counts... they only use different
		 * indexes
		 * 
		 */
		double[][] completeContingencyTable = new double[ds][ds];
		double[] contingencyTableForDependence = new double[ds * ds];
		int k = 0;
		for (int i = 0; i < ds; i++) {
			for (int j = 0; j < ds; j++) {
				double counts = i == j ? delta_ii : notDelta_ij;
				completeContingencyTable[i][j] = counts;
				contingencyTableForDependence[k] = counts;
				++k;
			}
		}

		double[] marginalsContingencyTable = new double[ds];
		for (int i = 0; i < ds; i++) {
			for (int j = 0; j < ds; j++) {
				marginalsContingencyTable[i] += completeContingencyTable[i][j];
			}
		}

		double unconditionalDependenceLogLikelihood = computeUnconditionalDependenceLogLikelihood(
				contingencyTableForDependence);
		double unconditionalIndependenceLoglikelihood = computeUnconditionalIndependenceLogLikelihood(
				marginalsContingencyTable, ds);

		/* in this simulation we have the same counts for all the slices, so we
		 have the same likelihoods
		 for all the slices*/
		loglikelihoodIndependentModel = unconditionalIndependenceLoglikelihood * numberOfSlices;
		loglikelihoodDependentModel = computeLoglikelihoodOfDependentModel(
				unconditionalIndependenceLoglikelihood, unconditionalDependenceLogLikelihood, numberOfSlices);

		// a is a name for log P(D|M_dep) - log P(D|M_ind) + log
		// P(M_dep) - log P(M_ind)
		double a = loglikelihoodDependentModel - loglikelihoodIndependentModel
				+ Math.log(1 - priorOfConditionalIndependenceModel) - Math.log(priorOfConditionalIndependenceModel);

		// applying a trick to compute log(exp()) without
		// underflow
		// see
		// https://hips.seas.harvard.edu/blog/2013/01/09/computing-log-sum-exp/
		// -log (1+exp(a)) is now -a -log(1+exp(-a))
		logPosteriorIndependenceModel = -a - Math.log1p(Math.exp(-a));

		logPosteriorDependenceModel = -Math
				.log1p(Math.exp(loglikelihoodIndependentModel - loglikelihoodDependentModel));

		
		if (logPosteriorIndependenceModel > logPosteriorDependenceModel) {
			return true;
		} else {
			return false;
		}
	}

	private double computeUnconditionalDependenceLogLikelihood(double[] contingencyTableForDependence) {
		return dirichletFunction(contingencyTableForDependence, _uniformGamma);
	}

	/**
	 * @param contingencyTableForIndependence
	 * @param dataset
	 * @param x
	 * @param y
	 * @param datasetForSlice
	 * @return
	 */
	private double computeUnconditionalIndependenceLogLikelihood(double[] contingencyTableForIndependence, int ds) {
		double alpha = ds * _uniformAlpha;
		double beta = ds * _uniformBeta;
		return dirichletFunction(contingencyTableForIndependence, alpha)
				+ dirichletFunction(contingencyTableForIndependence, beta);
	}

	/**
	 * Compute the data likelihood of the dependent model. Formula (20) in
	 * (Margaritis and Bromberg, 2009)
	 * 
	 * @param unconditionalDependenceLikelihoods2
	 * @param unconditionalIndependenceLoglikelihood
	 * @return
	 */
	private double computeLoglikelihoodOfDependentModel(double unconditionalIndependenceLoglikelihood,
			double unconditionalDependenceLoglikelihood, int numberOfSlices) {

		/* probability of the dependent model */
		double not_pMCI = 1 - priorOfConditionalIndependenceModel;

		/* prior of the independence model for each slice k */
		double pk = Math.log(priorOfConditionalIndependenceModel) / numberOfSlices;

		/* prior of the dependence model for each slice k */
		double qk = Math.log1p(-Math.pow(priorOfConditionalIndependenceModel, 1.0 / numberOfSlices));

		if (qk == Double.NaN || qk == Double.NEGATIVE_INFINITY || qk == Double.POSITIVE_INFINITY) {
			throw new RuntimeException("Math error with prior values, qk is  " + qk + ". Prior used: "
					+ priorOfConditionalIndependenceModel);
		}
		if (pk == Double.NaN || pk == Double.NEGATIVE_INFINITY || pk == Double.POSITIVE_INFINITY) {
			throw new RuntimeException("Math error with prior values, pk is  " + pk + ". Prior used: "
					+ priorOfConditionalIndependenceModel);
		}

		double aux1, aux2 = 0, aux3 = 0;

		/* in this simulation we have the same counts for all the slices, so we
		 have the same likelihoods
		 for all the slices*/
		double gk = unconditionalIndependenceLoglikelihood;
		double hk = unconditionalDependenceLoglikelihood;
		if (gk >= hk) { // preventing zero division
			aux1 = Math.log1p(Math.exp((hk + qk) - (gk + pk)));
			aux2 += gk + pk + aux1;
			aux3 += -aux1;
		} else {
			aux1 = Math.log1p(Math.exp((gk + pk) - (hk + qk)));
			aux2 += (hk + qk) + aux1;
			aux3 += (gk + pk) - (hk + qk) - aux1;
		}
		aux2 = aux2 * numberOfSlices; //the same likelihoods for all the slices
		aux3 = aux3 * numberOfSlices;

		double aux4 = Math.log(-Math.expm1(aux3));
		return aux2 + aux4 - Math.log(not_pMCI);
	}

	/**
	 * Computes the Dirichlet function for a given set of counts and the
	 * corresponding hyper-parameters.
	 * 
	 * @param counts
	 *            An array of counts of the variables of interest
	 * @param hyperparameter
	 *            The hyper-parameters of the variables of interest
	 * @return The Dirichlet value (factorial function).
	 */
	final private double dirichletFunction(double counts[], double hyperparameter) {
		double totalNumberOfCounts = 0;
		double marginalHyperparameter = 0;
		for (int i = 0; i < counts.length; i++) {
			totalNumberOfCounts += counts[i];
			marginalHyperparameter += hyperparameter;
		}

		double dirichletValue = 0;
		for (int i = 0; i < counts.length; i++) {
			dirichletValue += Gamma.logGamma(hyperparameter + counts[i]) - Gamma.logGamma(hyperparameter);
		}
		dirichletValue += Gamma.logGamma(marginalHyperparameter)
				- Gamma.logGamma(marginalHyperparameter + totalNumberOfCounts);

		return dirichletValue;
	}

	/**
	 * @return A vector of double values, wit logprobs[0] the log probability of
	 *         the independent model, and logprobs[1] the log probability of the
	 *         dependent model, and
	 */
	public double[] getLogProbs() {
		double[] logProbs = new double[2];
		logProbs[0] = logPosteriorIndependenceModel;
		logProbs[1] = logPosteriorDependenceModel;
		return logProbs;
	}
}
