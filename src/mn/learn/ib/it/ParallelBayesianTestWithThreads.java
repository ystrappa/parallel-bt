package mn.learn.ib.it;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.magicwerk.brownies.collections.BigList;

import math.Permutations;
import data.Dataset;

/**
 * This is an open source implementation of the Bayesian statistical test of
 * conditional independence (Margaritis, 2005). The Bayesian statistical test of
 * conditional independence is a nonparametric statistical analyzing method that
 * can be used to assess the probability of association or independence of
 * facts. In contrast with the well-known Chi-square ($\chi^2$) test, the
 * Bayesian statistical test works by comparing two competing statistical
 * models: an independence model and a dependence model, which are obtained by
 * computing the posterior probability of each model on data. This source code
 * follows the pseudocode of the technical report "An Open Source Implementation
 * Of The Bayesian Statistical Test Of Independence" , (Schl\"uter and Edera,
 * 2013).
 * 
 * @author fschluter
 * 
 */
public class ParallelBayesianTestWithThreads implements StatisticalIndependenceTest {

	public ParallelBayesianTestWithThreads(int nThreads) {
		// this.nThreads = nThreads;
		this.pool = Executors.newFixedThreadPool(nThreads);
	}

	/*
	 * Begin parallel auxiliary attributes and methods
	 */
	private ExecutorService pool;

	private BigList<Callable<Double>> callableListD = new BigList<Callable<Double>>();
	private BigList<Callable<Double>> callableListI = new BigList<Callable<Double>>();

	public void addCallable(Callable<Double> callable, String which) {
		switch (which) {
		case "d":
			this.callableListD.add(callable);
			break;

		case "i":
			this.callableListI.add(callable);
			break;

		default:
			break;
		}

	}

	public void clearCallables(String which) {
		switch (which) {
		case "d":
			this.callableListD.clear();
			break;

		case "i":
			this.callableListI.clear();
			break;

		default:
			break;
		}

	}

	public void executeThreads(String which) {
		try {
			switch (which) {
			case "d":
				pool.invokeAll(this.callableListD);
				break;

			case "i":
				pool.invokeAll(this.callableListI);
				break;

			default:
				break;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public BigList<Double> getResult(String which) {

		BigList<Future<Double>> resultList = null;
		BigList<Double> resultArray = null;
		try {
			switch (which) {
			case "d":
				pool.invokeAll(this.callableListD);
				break;
			case "i":
				pool.invokeAll(this.callableListI);
				break;
			default:
				break;
			}

			resultArray = new BigList<Double>(resultList.size());

			for (int i = 0; i < resultList.size(); i++) {
				resultArray.add(resultList.get(i).get());
				if (resultArray.get(i) == Double.NaN || resultArray.get(i) == Double.NEGATIVE_INFINITY
						|| resultArray.get(i) == Double.POSITIVE_INFINITY) {
					throw new RuntimeException("Loglikelihoods is " + resultArray.get(i));
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return resultArray;
	}

	/*
	 * Calcula el log-likelihood de independencia condicional para un slice. 
	 */
	public class CallableIndependenceLoglikelihood implements Callable<Double> {
		List<Integer> x, y;
		Dataset datasetForSlice;

		CallableIndependenceLoglikelihood(List<Integer> x, List<Integer> y, Dataset datasetForSlice) {
			this.x = x;
			this.y = y;
			this.datasetForSlice = datasetForSlice;
		}

		public Double call() throws Exception {
			List<int[]> permutationsOfX = Permutations.getPermutations(x, datasetForSlice.getCardinalities());
			double[] countsOfX = new double[permutationsOfX.size()];
			for (int j = 0; j < permutationsOfX.size(); ++j) {
				int[] permutationOfX = permutationsOfX.get(j);
				countsOfX[j] = datasetForSlice.getCounts(x, permutationOfX);
				// System.out.println("counts of x" + x + " " +countsOfX[j]);
			}
			List<int[]> permutationsOfY = Permutations.getPermutations(y, datasetForSlice.getCardinalities());
			double[] countsOfY = new double[permutationsOfY.size()];

			for (int j = 0; j < permutationsOfY.size(); ++j) {
				int[] permutationOfY = permutationsOfY.get(j);
				countsOfY[j] = datasetForSlice.getCounts(y, permutationOfY);
			}
			double alpha = permutationsOfX.size() * _uniformAlpha;
			double beta = permutationsOfY.size() * _uniformBeta;

			return dirichletFunction(countsOfX, alpha) + dirichletFunction(countsOfY, beta);

		}
	}

	/*
	 * Calcula el log-likelihood de dependencia condicional para un slice.
	 */
	public class CallableDependenceLoglikelihood implements Callable<Double> {
		List<Integer> x, y;
		Dataset datasetForSlice;

		CallableDependenceLoglikelihood(List<Integer> x, List<Integer> y, Dataset datasetForSlice) {
			this.x = x;
			this.y = y;
			this.datasetForSlice = datasetForSlice;
		}

		public Double call() throws Exception {
			List<Integer> xyList = new ArrayList<Integer>();
			xyList.addAll(x);
			xyList.addAll(y);
			List<int[]> permutationsOfXY = Permutations.getPermutations(xyList, datasetForSlice.getCardinalities());
			double[] countsOfXY = new double[permutationsOfXY.size()];

			for (int j = 0; j < permutationsOfXY.size(); ++j) {
				int[] permutationOfXY = permutationsOfXY.get(j);
				countsOfXY[j] = datasetForSlice.getCounts(xyList, permutationOfXY);
			}

			return dirichletFunction(countsOfXY, _uniformGamma);
		}
	}

	/*
	 * End parallel auxiliary attributes and methods
	 */

	/* Vectors of hyperparameters */
	public double[][] _gammaIJ;

	/* Uniform (homogeneous) values for hyperparameters */
	public double _uniformGamma = 1;
	public double _uniformAlpha = 1; // marginal over gamma
	public double _uniformBeta = 1; // marginal over gamma

	/* Prior Probability of conditional independence model */
	public double priorOfConditionalIndependenceModel = 0.5;

	/* Log of the posterior of the conditional dependence model */
	private double logPosteriorDependenceModel;
	/* Log of the posterior of the conditional independence model */
	private double logPosteriorIndependenceModel;
	/* Log of the likelihood of the conditional dependence model */
	private double loglikelihoodDependentModel;
	/* Log of the likelihood of the conditional independence model */
	private double loglikelihoodIndependentModel;

	/**
	 * This method is used to test the independence of a set of variables x
	 * respect to a set of variables y, conditioning in a set of variables z.
	 * 
	 * @param dataset
	 *            An instance of the dataset class
	 * @param x
	 *            A set of random variables
	 * @param y
	 *            A set of random variables
	 * @param z
	 *            A set of random variables
	 * @param i
	 *            The cardinality of all the variables (i.e. 2 for binary
	 *            variables).
	 * @return A decision of Independences of the triplet, given data
	 */
	public boolean independent(Dataset dataset, List<Integer> x, List<Integer> y, List<Integer> z) {
		// System.out.println("Running parallel Bayesian test.");
		IndependenceTriplet triplet = new IndependenceTriplet(x, y, z);
		return independent(dataset, triplet);
	}

	/**
	 * This method is used to test the independence truth value of a triplet
	 * conformed by a set of variables x, a set of variables y, and a
	 * conditioning set of variables z.
	 * 
	 * @param dataset
	 *            An instance of the dataset class
	 * @param x
	 *            A random variable
	 * @param y
	 *            A random variable
	 * @param z
	 *            A set of random variables
	 * @param i
	 *            The cardinality of all the variables (i.e. 2 for binary
	 *            variables).
	 * @return A decision of independence of the triplet, given data
	 */
	public boolean independent(Dataset dataset, IndependenceTriplet triplet) {
		List<Integer> x = triplet.getX();
		List<Integer> y = triplet.getY();
		List<Integer> z = triplet.getZ();
		initializeHyperparameters(x, y, dataset.getCardinalities());
		
		int slicesSize = (int) Math.pow(2, z.size());

		/* Probability of unconditional dependence for each slice */		
		BigList<Double> unconditionalDependenceLoglikelihoods = new BigList<Double>(slicesSize);
		/* probability of unconditional independence for each slice */
		BigList<Double> unconditionalIndependenceLoglikelihoods = new BigList<Double>(slicesSize);

		for (int i = 0; i < slicesSize; ++i) {
			// Get the next permutation
			int[] slice = Permutations.get(i, z.size());
			Dataset datasetForSlice = dataset.getSubdataset(z, slice);
			// System.out.println(" test x " + x+ " y "+y + " slice "+slice);

			// Create workers for each slice
			Callable<Double> callableD = new CallableDependenceLoglikelihood(x, y, datasetForSlice);
			addCallable(callableD, "d");

			// Create workers for each slice
			Callable<Double> callableI = new CallableIndependenceLoglikelihood(x, y, datasetForSlice);
			addCallable(callableI, "i");

		}

		// Ejecutar y obtener resultados
		unconditionalDependenceLoglikelihoods = getResult("d");
		unconditionalIndependenceLoglikelihoods = getResult("i");

		loglikelihoodIndependentModel = computeLoglikelihoodOfIndependentModel(unconditionalIndependenceLoglikelihoods);
		if (loglikelihoodIndependentModel == Double.NaN || loglikelihoodIndependentModel == Double.NEGATIVE_INFINITY
				|| loglikelihoodIndependentModel == Double.POSITIVE_INFINITY) {
			throw new RuntimeException("conditional Independence Loglikelihoods is " + loglikelihoodIndependentModel);
		}
		loglikelihoodDependentModel = computeLoglikelihoodOfDependentModel(unconditionalIndependenceLoglikelihoods,
				unconditionalDependenceLoglikelihoods);
		if (loglikelihoodDependentModel == Double.NaN || loglikelihoodDependentModel == Double.NEGATIVE_INFINITY
				|| loglikelihoodDependentModel == Double.POSITIVE_INFINITY) {
			throw new RuntimeException("conditional Dependence Loglikelihoods is " + loglikelihoodDependentModel);
		}

		// a is a name for log P(D|M_dep) - log P(D|M_ind) + log P(M_dep) - log
		// P(M_ind)
		double a = loglikelihoodDependentModel - loglikelihoodIndependentModel
				+ Math.log(1 - priorOfConditionalIndependenceModel) - Math.log(priorOfConditionalIndependenceModel);

		// applying a trick to compute log(exp()) without underflow
		// see
		// https://hips.seas.harvard.edu/blog/2013/01/09/computing-log-sum-exp/
		// -log (1+exp(a)) is now -a -log(1+exp(-a))
		logPosteriorIndependenceModel = -a - Math.log1p(Math.exp(-a));

		logPosteriorDependenceModel = -Math
				.log1p(Math.exp(loglikelihoodIndependentModel - loglikelihoodDependentModel));

		if (logPosteriorIndependenceModel > logPosteriorDependenceModel) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param dataset
	 * @param x
	 * @param y
	 * @param datasetForSlice
	 * @return
	 */
	private double computeUnconditionalIndependenceLogLikelihood(List<Integer> x, List<Integer> y,
			Dataset datasetForSlice) {

		List<int[]> permutationsOfX = Permutations.getPermutations(x, datasetForSlice.getCardinalities());
		double[] countsOfX = new double[permutationsOfX.size()];
		for (int j = 0; j < permutationsOfX.size(); ++j) {
			int[] permutationOfX = permutationsOfX.get(j);
			countsOfX[j] = datasetForSlice.getCounts(x, permutationOfX);
			// System.out.println("counts of x" + x + " " +countsOfX[j]);
		}
		List<int[]> permutationsOfY = Permutations.getPermutations(y, datasetForSlice.getCardinalities());
		double[] countsOfY = new double[permutationsOfY.size()];
		// omp for
		for (int j = 0; j < permutationsOfY.size(); ++j) {
			int[] permutationOfY = permutationsOfY.get(j);
			countsOfY[j] = datasetForSlice.getCounts(y, permutationOfY);
			// System.out.println("counts of y " +y + " " +countsOfY[j]);
		}
		double alpha = permutationsOfX.size() * _uniformAlpha;
		double beta = permutationsOfY.size() * _uniformBeta;
		return dirichletFunction(countsOfX, alpha) + dirichletFunction(countsOfY, beta);
	}

	private double computeUnconditionalDependenceLogLikelihood(List<Integer> x, List<Integer> y,
			Dataset datasetForSlice) {

		List<Integer> xyList = new ArrayList<Integer>();
		xyList.addAll(x);
		xyList.addAll(y);
		List<int[]> permutationsOfXY = Permutations.getPermutations(xyList, datasetForSlice.getCardinalities());
		double[] countsOfXY = new double[permutationsOfXY.size()];
		//
		for (int j = 0; j < permutationsOfXY.size(); ++j) {
			int[] permutationOfXY = permutationsOfXY.get(j);
			countsOfXY[j] = datasetForSlice.getCounts(xyList, permutationOfXY);
			// System.out.println("counts of x" + x+ "y " +y + " "
			// +countsOfXY[j]);
		}
		return dirichletFunction(countsOfXY, _uniformGamma);
	}

	/**
	 * Initializing hyperparameters uniformly. This is a common decision to
	 * initialize uniformly all the hyperparameters with a value 1, which means
	 * that there is no prior information about the hyperparameters.
	 * 
	 * @param x
	 *            First variable of the test
	 * @param y
	 *            Second variable of the test
	 * @param cardinalities
	 *            Cardinalities of the variables of the test
	 */
	private void initializeHyperparameters(List<Integer> x, List<Integer> y, int[] cardinalities) {
		/* computing cardinality of sets of variables */
		int totalCardinalityOfX = 0, totalCardinalityOfY = 0;
		for (int i : x)
			totalCardinalityOfX += cardinalities[i];
		for (int i : y)
			totalCardinalityOfY += cardinalities[i];

		/* initializing gamma hyperparameters */
		_gammaIJ = new double[totalCardinalityOfX][totalCardinalityOfY];
		for (int i = 0; i < totalCardinalityOfX; ++i)
			for (int j = 0; j < totalCardinalityOfY; ++j)
				_gammaIJ[i][j] = _uniformGamma;

	}

	/**
	 * Compute the data likelihood of the independent model. Formula (19) in
	 * (Margaritis and Bromberg, 2009).
	 * 
	 * @param unconditionalDependenceLikelihoods
	 */
	private double computeLoglikelihoodOfIndependentModel(BigList<Double> unconditionalIndependenceLogikelihoods) {
		double logLikelihoodsDependenceModel = 0;
		for (double d : unconditionalIndependenceLogikelihoods) {
			logLikelihoodsDependenceModel += d;
		}
		return logLikelihoodsDependenceModel;
	}

	/**
	 * Compute the data likelihood of the dependent model. Formula (20) in
	 * (Margaritis and Bromberg, 2009)
	 * 
	 * @param unconditionalDependenceLikelihoods2
	 * @param unconditionalIndependenceLoglikelihoods
	 * @return
	 */
	private double computeLoglikelihoodOfDependentModel(BigList<Double> unconditionalIndependenceLoglikelihoods,
			BigList<Double> unconditionalDependenceLoglikelihoods) {
		/* number of slices */
		int K = unconditionalDependenceLoglikelihoods.size();

		/* probability of the dependent model */
		double not_pMCI = 1 - priorOfConditionalIndependenceModel;

		/* prior of the independence model for each slice k */
		double pk = Math.log(priorOfConditionalIndependenceModel) / K;

		/* prior of the dependence model for each slice k */
		double qk = Math.log1p(-Math.pow(priorOfConditionalIndependenceModel, 1.0 / K));

		if (qk == Double.NaN || qk == Double.NEGATIVE_INFINITY || qk == Double.POSITIVE_INFINITY) {
			throw new RuntimeException("Math error with prior values, qk is  " + qk + ". Prior used: "
					+ priorOfConditionalIndependenceModel);
		}
		if (pk == Double.NaN || pk == Double.NEGATIVE_INFINITY || pk == Double.POSITIVE_INFINITY) {
			throw new RuntimeException("Math error with prior values, pk is  " + pk + ". Prior used: "
					+ priorOfConditionalIndependenceModel);
		}

		double aux1, aux2 = 0, aux3 = 0;

		for (int k = 0; k < K; ++k) {
			double gk = unconditionalIndependenceLoglikelihoods.get(k);
			double hk = unconditionalDependenceLoglikelihoods.get(k);
			if (gk >= hk) { // preventing zero division
				aux1 = Math.log1p(Math.exp((hk + qk) - (gk + pk)));
				aux2 += gk + pk + aux1;
				aux3 += -aux1;
			} else {
				aux1 = Math.log1p(Math.exp((gk + pk) - (hk + qk)));
				aux2 += (hk + qk) + aux1;
				aux3 += (gk + pk) - (hk + qk) - aux1;
			}
		}
		double aux4 = Math.log(-Math.expm1(aux3));
		return aux2 + aux4 - Math.log(not_pMCI);
	}

	/**
	 * Computes the Dirichlet function for a given set of counts and the
	 * corresponding hyper-parameters.
	 * 
	 * @param counts
	 *            An array of counts of the variables of interest
	 * @param hyperparameter
	 *            The hyper-parameters of the variables of interest
	 * @return The Dirichlet value (factorial function).
	 */
	final private double dirichletFunction(double counts[], double hyperparameter) {
		double totalNumberOfCounts = 0;
		double marginalHyperparameter = 0;
		for (int i = 0; i < counts.length; i++) {
			totalNumberOfCounts += counts[i];
			marginalHyperparameter += hyperparameter;
		}

		double dirichletValue = 0;
		for (int i = 0; i < counts.length; i++) {
			dirichletValue += Gamma.logGamma(hyperparameter + counts[i]) - Gamma.logGamma(hyperparameter);
		}
		dirichletValue += Gamma.logGamma(marginalHyperparameter)
				- Gamma.logGamma(marginalHyperparameter + totalNumberOfCounts);

		return dirichletValue;
	}

	/**
	 * @return A vector of double values, wit logprobs[0] the log probability of
	 *         the independent model, and logprobs[1] the log probability of the
	 *         dependent model, and
	 */
	public double[] getLogProbs() {
		double[] logProbs = new double[2];
		logProbs[0] = logPosteriorIndependenceModel;
		logProbs[1] = logPosteriorDependenceModel;
		return logProbs;
	}

	/**
	 * @return A vector of double values, wit loglikelihoods[0] the log
	 *         likelihood of the independent model, and logprobs[1] the log
	 *         likelihood of the dependent model, and
	 */
	public double[] getLoglikelihoods() {
		double[] logLikelihoods = new double[2];
		logLikelihoods[0] = loglikelihoodIndependentModel;
		logLikelihoods[1] = loglikelihoodDependentModel;
		return logLikelihoods;
	}

	public void setUniformParameters(int alpha) {
		_uniformGamma = alpha;
		_uniformAlpha = alpha;
		_uniformBeta = alpha;
	}

}
