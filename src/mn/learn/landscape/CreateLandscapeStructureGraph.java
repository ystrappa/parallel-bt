package mn.learn.landscape;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;

import data.Dataset;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.learn.ib.algs.ibmap.score.MPL;
import mn.learn.ib.algs.ibmap.score.RIBScore;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import util.BitsetUtils;

public class CreateLandscapeStructureGraph {
	private static String dir = "/home/fschluter/experiments/datasets/sampled/undirected/mpl";
	private static String trueModelFile = "/home/fschluter/experiments/datasets/sampled/undirected/mpl/synthetic.n4.7.graph";
	private static String generatingModel = "synthetic.n4.7.ds.2";
	private static int rep = 1;
	private static int seed = 1;
	private static int domainSize = 2;
	private static List<Integer> Ds = Arrays.asList(1000);
	private static Dataset dataset;
	private static String datasetName;

	public static String scoringFunctionString = "ibscore" ;
	private static double column = 1;
	private static int row = 1;
	private static int[] nodesForEachLevel;
	private static int horizontalDistance = 100;
	private static int verticalDistance = 100;

	private static int DEFAULT_WIDTH = 798;
	private static int DEFAULT_HEIGHT = 1571;
	private static int n;
	private static GraphScorable scoringFunction = null;

	public static void generateLandscapeStructure(int domainSize, int D) throws IOException, InterruptedException {
		UndirectedGraph trueGraph = UndirectedGraphParser
				.readTrueGraph(trueModelFile);
		n = trueGraph.getNumberOfNodes();
		datasetName = generatingModel + ".r." + rep + ".seed." + seed + ".csv";
		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName), D, 1);

		
		if (scoringFunctionString.equals("ibscore")) {
			scoringFunction = new IBScore();
		} else if (scoringFunctionString.equals("mpl")) {
			scoringFunction = new MPL();
		} else if (scoringFunctionString.equals("ribscore")) {
			scoringFunction = new RIBScore();
		}

		n = domainSize;

		Graph graph = new SingleGraph("Hasse diagram");

		int numberOfEdges = n * (n - 1) / 2;
		int numberOfLevels = numberOfEdges + 1;
		nodesForEachLevel = new int[numberOfLevels];
		verticalDistance = DEFAULT_HEIGHT / numberOfLevels;
		horizontalDistance = DEFAULT_WIDTH / numberOfLevels;
		String[] set = new String[numberOfEdges];
		

		int k = 0;
		for (int i = 0; i < n - 1; ++i) {
			for (int j = i + 1; j < n; ++j) {
				String nodeName = "" + i + "^" + j;
				set[k++] = nodeName;
			}
		}

		// We will enumerate all the subset
		for (long i = 0, max = 1 << set.length; i < max; i++) {
			// we create a new subset
			List newSet = new ArrayList();
			for (int j = 0; j < set.length; j++) {
				// check if the j bit is set to 1
				int isSet = (int) i & (1 << j);
				if (isSet > 0) {
					// if yes, add it to the set
					newSet.add(set[j]);
				}
			}
			// For the new subset, print links to all supersets
			if (newSet.size() != set.length) {
				printLinksToImmediateSupersets(newSet, set, graph);
			}
		}

		for (Node node : graph) {
			UndirectedGraph structure = asUndirectedGraph(node.getId());
			long structureCode = BitsetUtils.convert(structure.getBitset());
			node.addAttribute("ui.label", structureCode);
			node.addAttribute("ui.hover", structure.getBitsetString());
			// node.addAttribute("ui.label", node.getId());
		}

		Viewer viewer = graph.display();

		viewer.disableAutoLayout();

	}

	/**
	 * This method print links from a subset to all its immediate supersets (not
	 * optimized).
	 * 
	 * @param subset
	 *            the subset
	 * @param set
	 *            the set of all integers
	 * @param writer
	 *            object to write to the output file
	 * @param graph
	 * @throws IOException
	 */
	private static void printLinksToImmediateSupersets(List subset, String[] set, Graph graph) throws IOException {
		// For each integer in the set of all integers
		for (int i = 0; i < set.length; i++) {
			String value = set[i];
			// if it is not contained in the subset
			if (subset.contains(value) == false) {
				// we add it to the set to make an immediate superset
				// and write the link
				List newSet = new ArrayList();
				newSet.addAll(subset);
				newSet.add(value);

				if (graph.getNode(asString(subset)) == null) {
					graph.addNode(asString(subset));
					Node node = graph.getNode(asString(subset));
					node.setAttribute("x", nodesForEachLevel.length * horizontalDistance * column);
				}

				if (graph.getNode(asString(newSet)) == null) {
					graph.addNode(asString(newSet));

					Node node = graph.getNode(asString(newSet));
					UndirectedGraph structure = asUndirectedGraph(newSet);
					long structureCode = BitsetUtils.convert(structure.getBitset());
					double score = scoringFunction.computeScore(structure, dataset).getTotalScore();
//					double max=-Double.MAX_VALUE;
//			    	double min=Double.MAX_VALUE;
//					score=((score-min)/(max-min));
					
					int thisRow = newSet.size();
					if (row != thisRow) {
						row = thisRow;
					}
					++nodesForEachLevel[row];
					column = nodesForEachLevel[row];

					System.out.println(asString(newSet) + "row " + row + " column " + column);
					node.setAttribute("y", nodesForEachLevel.length * -verticalDistance * row);
					node.setAttribute("x", nodesForEachLevel.length * horizontalDistance * column);
					
					node.setAttribute("ui.color", Math.exp(score));
					System.out.println("struct " + structureCode  + " score " + Math.exp(score ));

					graph.addAttribute("ui.stylesheet", "node { " + "fill-mode: dyn-plain; "
							+ "fill-color:red, yellow; " + "shape: box; " + "size: 20px, 20px; " + "}");

				}
				graph.addEdge(asString(subset) + asString(newSet), asString(subset), asString(newSet));
			}
		}
	}

	/**
	 * Convert a set to a string representation
	 * 
	 * @param set
	 *            the set as a list of integers
	 * @return a string
	 */
	private static String asString(List set) {
		Collections.sort(set);
		// if the empty set, we will write "{}"
		if (set.size() == 0) {
			return "\"{}\"";
		}
		// otherwise we will write the set of integers
		StringBuffer buffer = new StringBuffer();
		buffer.append("\"{");
		// for each integer
		for (int i = 0; i < set.size(); i++) {
			String value = (String) set.get(i);
			buffer.append(value);
			if (i != set.size() - 1) {
				buffer.append(",");
			}
		}
		buffer.append("}\"");
		return buffer.toString();
	}

	private static UndirectedGraph asUndirectedGraph(List set) {
		UndirectedGraph undirectedGraph = new UndirectedGraph(n);

		Collections.sort(set);
		// if the empty set, we will write "{}"
		if (set.size() == 0) {
			return undirectedGraph;
		}

		// for each integer
		for (int i = 0; i < set.size(); i++) {
			String value = (String) set.get(i);
			String[] nodes = value.split("\\^");
			undirectedGraph.addEdge(new Integer(nodes[0]), new Integer(nodes[1]));
		}

		return undirectedGraph;
	}

	// Collections.sort(set);
	// // if the empty set, we will write "{}"
	// if (set.size() == 0) {
	// return "\"{}\"";
	// }
	// // otherwise we will write the set of integers
	// StringBuffer buffer = new StringBuffer();
	// buffer.append("\"{");
	// // for each integer
	// for (int i = 0; i < set.size(); i++) {
	// String value = (String) set.get(i);
	// buffer.append(value);
	// if (i != set.size() - 1) {
	// buffer.append(",");
	// }
	// }
	// buffer.append("}\"");
	// return buffer.toString();

	private static UndirectedGraph asUndirectedGraph(String id) {
		id = id.replace("\"{", "");
		id = id.replace("}\"", "");
		UndirectedGraph undirectedGraph = new UndirectedGraph(n);
		if (id.equals(""))
			return undirectedGraph;
		String[] edges = id.split(",");
		for (String edge : edges) {
			edge = edge.replace("\"{", "");
			edge = edge.replace("}\"", "");
			String[] nodes = edge.split("\\^");
			undirectedGraph.addEdge(new Integer(nodes[0]), new Integer(nodes[1]));
		}
		return undirectedGraph;
	}

	public static void main(String args[]) throws IOException, InterruptedException {
		for (int D = 0; D < Ds.size(); D++) {
			generateLandscapeStructure(4, Ds.get(D));
		}
	}
}
