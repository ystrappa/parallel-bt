package mn.learn.bs;


import java.util.ArrayList;
import java.util.List;

import math.Permutations;
import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.it.Gamma;
import mn.rep.UndirectedGraph;
import data.Dataset;


/**
 * @author fschluter
 * 
 */
public class BDEu implements GraphScorable {

	/* Uniform (homogeneous) values for hyperparameters */
	public double _uniformAlpha = 1;

	public BDEu(int uniformAlpha) {
		_uniformAlpha = uniformAlpha;
	}

	/**
	 * @param dataset
	 *            An instance of the dataset class
	 * @param x
	 *            A random variable
	 * @param y
	 *            A random variable
	 * @param z
	 *            A set of random variables
	 * @param i
	 *            The cardinality of all the variables (i.e. 2 for binary
	 *            variables).
	 * @return The BDEu score of a graph, using the dataset given as parameter input
	 */
	public UndirectedGraphScore computeScore(UndirectedGraph graph, Dataset dataset) {
		double score = 0;
		for (int i = 0; i < graph.getNumberOfNodes(); ++i) {
			// permutations of current variables X_i
			List<Integer> x_i = new ArrayList<Integer>();
			x_i.add(i);
			List<int[]> permutationsOfX_i = Permutations.getPermutations(x_i, dataset.getCardinalities());

			// permutations of the Markov blanket of the current variables X_i
			List<Integer> blanketOfX_i = graph.getBlanketOf(i);
			List<int[]> permutationsOfBlanketOfX_i = Permutations.getPermutations(blanketOfX_i, dataset.getCardinalities());

			for (int[] permutationOfBlanketOfX_i : permutationsOfBlanketOfX_i) {
				int countsOfPermutationOfBlanketOfX_i = 0;
				double alpha_i = 0;
				for (int[] permutationOfX_i : permutationsOfX_i) {
					List<Integer> variables = new ArrayList<Integer>();
					variables.addAll(blanketOfX_i);
					variables.addAll(x_i);
					int[] values = new int[permutationOfBlanketOfX_i.length + 1];
					System.arraycopy(permutationOfBlanketOfX_i, 0, values, 0, permutationOfBlanketOfX_i.length);
					values[permutationOfBlanketOfX_i.length] = permutationOfX_i[0];
					int counts = dataset.getCounts(variables, values);
					double alpha = _uniformAlpha;

					countsOfPermutationOfBlanketOfX_i += counts;
					alpha_i += alpha;

					score += Gamma.logGamma(alpha + counts) - Gamma.logGamma(alpha);
				}
				score += Gamma.logGamma(alpha_i) - Gamma.logGamma(alpha_i + countsOfPermutationOfBlanketOfX_i);
			}
		}

		UndirectedGraphScore gScore = new UndirectedGraphScore(graph);
		gScore.setTotalScore(score);
		return gScore;
	}

	@Override
	public UndirectedGraphScore computeScore(UndirectedGraph g, UndirectedGraphScore incremental, Dataset dataset) {
		return computeScore(g,dataset);
	}

	@Override
	public String getScoringFunctionName() {
		return "bdeu";
	}

}
