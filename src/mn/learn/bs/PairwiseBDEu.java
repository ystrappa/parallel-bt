package mn.learn.bs;



import java.util.ArrayList;
import java.util.List;


import math.Permutations;
import mn.learn.ib.it.Gamma;
import mn.rep.UndirectedGraph;

import data.Dataset;


/**
 * @author fschluter
 * 
 */
public class PairwiseBDEu {

	/* Uniform (homogeneous) values for hyperparameters */
	public double _uniformAlpha = 1;

	public PairwiseBDEu(int uniformAlpha) {
		_uniformAlpha = uniformAlpha;
	}

	/**
	 * @param dataset
	 *            An instance of the dataset class
	 * @param x
	 *            A random variable
	 * @param y
	 *            A random variable
	 * @param z
	 *            A set of random variables
	 * @param i
	 *            The cardinality of all the variables (i.e. 2 for binary
	 *            variables).
	 * @return A decision of Independences of the triplet, given data
	 */
	public double computeScore(Dataset dataset, UndirectedGraph graph) {
		double score = 0;
		for (int i = 0; i < graph.getNumberOfNodes() - 1; ++i) {
			for (int j = i + 1; j < graph.getNumberOfNodes(); ++j) {
				if (graph.existEdge(i, j)) {
					// permutations of current variables X_i
					List<Integer> edge = new ArrayList<Integer>();
					edge.add(i);
					edge.add(j);
					List<int[]> permutationsOfIJ = Permutations.getPermutations(edge, dataset.getCardinalities());
					int totalAlpha = 0;
					for (int[] permutation : permutationsOfIJ) {
						int counts = dataset.getCounts(edge, permutation);
						double alpha = _uniformAlpha;
						totalAlpha += alpha;
						score += Gamma.logGamma(alpha + counts) - Gamma.logGamma(alpha);
					}
					score += Gamma.logGamma(totalAlpha) - Gamma.logGamma(totalAlpha + dataset.getExamples().size());
				} else {
					List<Integer> iList = new ArrayList<Integer>();
					iList.add(i);
					List<int[]> permutationsOfI = Permutations.getPermutations(iList, dataset.getCardinalities());
					int totalAlpha = 0;
					for (int[] permutation : permutationsOfI) {
						int counts = dataset.getCounts(iList, permutation);
						double alpha = _uniformAlpha;
						totalAlpha += alpha;
						score += Gamma.logGamma(alpha + counts) - Gamma.logGamma(alpha);
					}
					score += Gamma.logGamma(totalAlpha) - Gamma.logGamma(totalAlpha + dataset.getExamples().size());

					List<Integer> jList = new ArrayList<Integer>();
					jList.add(j);
					List<int[]> permutationsOfJ = Permutations.getPermutations(jList, dataset.getCardinalities());
					totalAlpha = 0;
					for (int[] permutation : permutationsOfJ) {
						int counts = dataset.getCounts(jList, permutation);
						double alpha = _uniformAlpha;
						totalAlpha += alpha;
						score += Gamma.logGamma(alpha + counts) - Gamma.logGamma(alpha);
					}
					score += Gamma.logGamma(totalAlpha) - Gamma.logGamma(totalAlpha + dataset.getExamples().size());
				}
			}
		}

		return score;
	}

}
