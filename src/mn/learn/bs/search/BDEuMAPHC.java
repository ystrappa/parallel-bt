package mn.learn.bs.search;

import mn.learn.bs.BDEu;
import mn.rep.UndirectedGraph;
import data.Dataset;

public class BDEuMAPHC {
	/**
	 * The number of variables of the domain
	 */
	private int n;

	/**
	 * The output network learned by IBMAP-HC
	 */
	private UndirectedGraph outputNetwork;

	/**
	 * The training dataset used in the algorithm to learn the independence
	 * structure
	 */
	private Dataset dataset;

	private int uniformAlpha = 1;

	/**
	 * @param n
	 *            Number of variables of the domain
	 * @param test
	 *            Statististical independence test instance
	 * @param dataset
	 *            Dataset instance
	 */
	public BDEuMAPHC(int n, Dataset dataset) {
		this.n = n;
		this.dataset = dataset;
	}

	/**
	 * Simple implementation of pseudocode shown in (Schluter, et al., 2013)
	 */
	public void run() {
		// Computing the score of the empty structure
		BDEu bDEu = new BDEu(uniformAlpha);
		UndirectedGraph g = new UndirectedGraph(n);
		double currentScore = bDEu.computeScore(g, dataset).getTotalScore();
		double neighborScore = 0;
		do {
			// Selecting the best neighbor structure
			UndirectedGraph neighbor = selectNeighbor(g);
			// Computing the score of the best neighbor structure
			neighborScore = bDEu.computeScore(neighbor, dataset).getTotalScore();
			if (neighborScore <= currentScore) {
				// Local maxima reached. Termination condition.
				outputNetwork = g;
				return;
			}
			g = neighbor;
			currentScore = neighborScore;
		} while (true);
	}

	/**
	 * Returns a neighbour structure flippng the edge that minimizes σX,Y (G) +
	 * σY,X (G)
	 * 
	 * @param g
	 * @param gScore
	 * @return
	 */
	protected UndirectedGraph selectNeighbor(UndirectedGraph g) {
		int bestX = 0, bestY = 0;
		double maxScore = Integer.MIN_VALUE;
		// Obtaining scores of g, computed in previous iteration

		BDEu bDEu = new BDEu(uniformAlpha);
		UndirectedGraph u = new UndirectedGraph(g);
		for (int i = 0; i < n - 1; ++i) {
			for (int j = i + 1; j < n; ++j) {
				UndirectedGraph neighbor = new UndirectedGraph(g);
				neighbor.flipEdge(i, j);
				double neighborScore = bDEu.computeScore(neighbor, dataset).getTotalScore();
				// Obtaining the pair of tests whose product adds the lowest
				// value to the current score

				if (neighborScore > maxScore) {
					maxScore = neighborScore;
					bestX = i;
					bestY = j;
				}
			}
		}
		// Flipping the worst edge
		u.flipEdge(bestX, bestY);
		return u;
	}

	/**
	 * @return
	 */
	public UndirectedGraph getOutputNetwork() {
		return outputNetwork;
	}

	public void setUniformAlpha(int alpha) {
		this.uniformAlpha = alpha;
	}

}
