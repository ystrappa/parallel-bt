package math;

import java.util.BitSet;

import mn.rep.UndirectedGraph;

public class Combinations {

	static BitSet bs = new BitSet(3);
	static UndirectedGraph map;

	static void fill(int k, int n) {
		if (k == n) {
			System.out.println(BitSetUtil.convert(bs) + ": " + bs);
			return;
		}

		bs.set(k, false);
		fill(k + 1, n);
		bs.set(k, true);
		fill(k + 1, n);
	}

	public static void main(String[] args) {
		int n = 3;
		map = new UndirectedGraph(n * (n - 1) / 2);
		fill(0, n);
	}

}