package math;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import mn.rep.UndirectedGraph;

public class BitSetUtil {

	public static List<BitSet> getAllPossibleBitsets(int k) {
		int size = 1 << k;
		ArrayList<BitSet> results = new ArrayList<BitSet>();
		results.add(new BitSet(k)); // adding empty
		for (int val = 1; val < size; val++) {
			BitSet bs = new BitSet(k);
			results.add(bs);
			for (int b = 0; b < k; b++) {
				if (((val >>> b) & 1) == 1) {
					bs.set(b);
				}
			}
		}
		return results;
	}

	public static BitSet convert(long value) {
		BitSet bits = new BitSet();
		int index = 0;
		while (value != 0L) {
			if (value % 2L != 0) {
				bits.set(index);
			}
			++index;
			value = value >>> 1;
		}
		return bits;
	}

	public static long convert(BitSet bits) {
		long value = 0L;
		for (int i = 0; i < bits.length(); ++i) {
			value += bits.get(i) ? (1L << i) : 0L;
		}
		return value;
	}

	public static UndirectedGraph getMapOfAllPossibleBitsets(int k) {
		UndirectedGraph map = new UndirectedGraph(k*(k-1)/2);
		int size = 1 << k;
		ArrayList<BitSet> results = new ArrayList<BitSet>();
		
		BitSet emptyBitset = new BitSet(k);
		results.add(emptyBitset); // adding empty
		
		for (int val = 1; val < size; val++) {
			BitSet bs = new BitSet(k);
			results.add(bs);
			for (int b = 0; b < k; b++) {
				if (((val >>> b) & 1) == 1) {
					bs.set(b);
				}
			}
			
			
		}
		return map;
	}
	
	
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.println(""+getAllPossibleBitsets(i*(i-1)/2).size());
		}
	}

}
