package math;

import java.util.ArrayList;
import java.util.Random;


public class Mathutil {

  public static void svd(double[][] a,double[] w,double[][] v)
    {
      int i,its,j,jj,k,l=0,nm=0;
      boolean flag;
      int m = a.length;
      int n = a[0].length;
      double c,f,h,s,x,y,z;
      double anorm = 0., g = 0., scale=0. ;
      //zliberror._assert(m>=n) ;
      double[] rv1 = new double[n];

      //System.out.println("SVD beware results may not be sorted!");

      for (i = 0; i<n; i++) {
        l = i+1;
        rv1[i] = scale*g;
        g = s = scale  = 0. ;
        if  (i<m) {
          for  (k = i; k<m; k++)  scale += abs(a[k][i]) ;
          if (scale!=0.0) {
            for (k = i; k<m; k++) {
              a[k][i] /= scale;
              s += a[k][i]*a[k][i] ;
            }
            f = a[i][i];
            g = -SIGN(Math.sqrt(s),f) ;
            h=f*g-s;
            a[i][i]=f-g;
            //if (i!=(n-1)) {		// CHECK
              for (j = l; j<n; j++) {
                for (s = 0,k = i; k<m; k++)
                  s += a[k][i]*a[k][j];
                f = s/h;
                for (k = i; k<m; k++)
                  a[k][j] += f*a[k][i];
              }
              //}
            for (k = i; k<m; k++) a[k][i] *= scale;
          }
        }
        w[i] = scale*g;
        g = s = scale = 0.0 ;
        if (i<m && i!=n-1) {		//
          for (k = l; k<n; k++)
            scale += abs(a[i][k]) ;
          if  (scale != 0.) {
            for  (k = l; k<n; k++) {	//
              a[i][k]  /= scale;
              s += a[i][k]*a[i][k] ;
            }
            f = a[i][l];
            g = -SIGN(Math.sqrt(s),f);
            h = f*g-s;
            a[i][l] = f-g;
            for  (k = l; k<n; k++)
              rv1[k] = a[i][k]/h;
            if (i!=m-1) {		//
              for (j = l; j<m; j++) {	//
                for (s = 0, k = l; k<n; k++)
                  s += a[j][k]*a[i][k] ;
                for (k = l; k<n; k++)
                  a[j][k] += s*rv1[k] ;
              }
            }
            for (k = l; k<n; k++)
              a[i][k] *= scale;
          }
        } //i<m && i!=n-1
        anorm = Math.max(anorm,(abs(w[i])+abs(rv1[i])));
      } //i
      for (i = n-1; i>=0; --i) {
        if (i<n-1) {			//
          if (g != 0.) {
            for (j = l; j<n; j++)
            v[j][i] = (a[i][j]/a[i][l])/g;
            for (j = l; j<n; j++) {
              for (s = 0,k = l; k<n; k++)
                s += a[i][k]*v[k][j];
              for (k = l; k<n; k++)
                v[k][j] += s*v[k][i];
            }
          }
          for (j = l; j<n; j++)		//
          v[i][j] = v[j][i] = 0.0;
        }
        v[i][i] = 1.0;
        g = rv1[i];
        l = i;
      }
      //for (i=IMIN(m,n);i>=1;i--) {	// !
      //for (i = n-1; i>=0; --i)  {
      for (i = Math.min(m-1,n-1); i>=0; --i) {
        l = i+1;
        g = w[i];
        if (i<n-1)			//
        for (j = l; j<n; j++)		//
        a[i][j] = 0.0;
        if (g != 0.) {
          g = 1./g;
          if (i!= n-1) {
            for(j = l; j<n; j++) {
              for (s = 0, k = l; k<m; k++)
                s += a[k][i]*a[k][j];
              f = (s/a[i][i])*g;
              for (k = i; k<m; k++)
                a[k][j] += f*a[k][i];
            }
          }
          for (j = i; j < m; j++)
            a[j][i] *= g;
        }
        else {
          for (j = i; j<m; j++)
          a[j][i] = 0.0;
        }
        a[i][i] += 1.0;
      }
      for (k = n-1; k>=0; --k)  {
        for (its = 1; its<=30; ++its) {
          flag = true;
          for (l = k; l>=0;  --l) {
            nm = l-1;
            if ((abs(rv1[l])+anorm) == anorm) {
              flag = false;
              break ;
            }
            if ((abs(w[nm])+anorm)  == anorm) break;
          }
          if (flag) {
            c = 0.0;
            s = 1.0;
            for (i = l; i<=k; i++)  {	//
              f = s*rv1[i];
              rv1[i] = c*rv1[i];
              if ((abs(f)+anorm)==anorm)
                break;
              g = w[i];
              h = pythag(f,g) ;
              w[i] = h;
              h = 1.0/h;
              c = g*h;
              s = -f*h;
              for (j = 0; j<m; j++) {
                y = a[j][nm] ;
                z = a[j][i];
                a[j][nm] = y*c+z*s;
                a[j][i] = z*c-y*s;
              }
            }
          } //flag
          z = w[k];
          if (l==k) {
            if (z<0.) {
              w[k] = -z;
              for (j = 0; j<n; j++)
                v[j][k] = -v[j][k];
            }
            break;
          } //l==k
          //zliberror._assert(its<50, "no svd convergence in 50 iterations");

          x = w[l];
          nm = k-1;
          y = w[nm];
          g = rv1[nm] ;
          h = rv1[k] ;
          f = ((y-z)*(y+z)+(g-h)*(g+h))/(2*h*y);
          g = pythag(f,1.0) ;
          f = ((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
          c = s = 1.0;
          for (j = l; j<=nm; j++) {
            i = j+1;
            g = rv1[i];
            y = w[i];
            h = s*g;
            g = c*g;
            z = pythag(f,h) ;
            rv1[j] = z;
            c = f/z;
            s = h/z;
            f = x*c+g*s;
            g = g*c-x*s;
            h = y*s;
            y *= c;
            for (jj = 0; jj<n; jj++) {
              x = v[jj][j];
              z = v[jj][i];
              v[jj][j] = x*c+z*s;
              v[jj][i] = z*c-x*s;
            }
            z = pythag(f,h) ;
            w[j] = z;
            if (z != 0.0) {
              z = 1.0/z;
              c = f*z;
              s = h*z;
            }
            f = c*g+s*y;
            x = c*y-s*g;
            for (jj = 0; jj<m; ++jj) {
              y = a[jj][j];
              z = a[jj][i];
              a[jj][j] = y*c+z*s;
              a[jj][i] = z*c-y*s;
            }
          } //j<nm
          rv1[l] = 0.0;
          rv1[k] = f;
          w[k] = x;
        } //its
      } //k
      // free rv1
    } //svd

    public static void svdbksb(double [][]u, double []w, double [][]v, int m, int n, double []b, double []x)
      {
          int jj, j, i;
          double s;
          double [] tmp;
          tmp = new double [n];
          for (j = 0; j < n; j++) {
                  /* Calculate UTB. */
              s = 0.0;
              if (w[j]!=0.0) {
                 /* Nonzero result only if wj is nonzero. */
                 for (i = 0; i < m; i++) s += u[i][j] * b[i];
                 s /= w[j]; /* This is the divide by wj . */
              }
              tmp[j] = s;
          }
          for (j = 0; j < n; j++) {
                  /* Matrix multiply by V to get answer. */
              s = 0.0;
              for (jj = 0; jj < n; jj++) s += v[j][jj] * tmp[jj];
              x[j] = s;
          }
      }

    public static void solveWithSVD(double [][]a, int rows, int cols, double [] f, double [] alpha){

     // if (rows <= cols - 1) {
        //solve the A.x = B using SVD
        double[][] u = new double[rows][cols];
        double[][] v = new double[cols][cols];
        double[] w = new double[cols];

        for (int i = 0; i < rows; i++)for (int j = 0; j < cols; j++) u[i][j] = a[i][j]; //make a copy of A  coz we may needed to calculate other statistics like r2
        Mathutil.svd(u, w, v); //svd of uu to produce [u,w,v]
        double wmax = 0.0;
        for (int j = 0; j < cols; j++)if (w[j] > wmax) wmax = w[j];
        String remCoeff = "";
        double wmin = wmax * 1.0e-6; //set the threshold for singular values allowed to be non zero
        for (int j = 0; j < cols; j++)
          if (w[j] < wmin) {
            w[j] = 0.0;
            remCoeff = remCoeff + " " + j; //for printing purpose
          }
        Mathutil.svdbksb(u, w, v, rows, cols, f, alpha); //back substitute [u,w,v] to b to get x ([u,w,v].xs = ys)
     /* }
      else{
        // solve the ATA.x = ATb using SVD

        double[][] u = new double[cols][cols];
        double[][] v = new double[cols][cols];
        double[] w = new double[cols];

        double [][] aT = new double[cols][rows];     // transpose of a
        double [] aTf = new double[rows];            // aTb

        transpose(a, rows, cols, aT);         //transpose A = coeffT
        multMM(aT, cols, rows, a, u);         //multiply coeffT by coeff to get square matrix u=ATA which is input to svd
        multMV(aT, cols, rows, f, aTf);       //multiply coeffT by f (RHS) to get ATf
        Mathutil.svd(u, w, v);                 //decompose ATA
        double wmax = 0.0;
        for(int j = 1; j <= cols; j++) if (w[j] > wmax) wmax = w[j];
        String remCoeff="";
        double wmin = wmax * 1.0e-6;                               //set the threshold for singular values allowed to be non zero
        for(int j = 1; j <= cols; j++)
           if (w[j] < wmin) {
               w[j] = 0.0;
               remCoeff = remCoeff+" "+j;         //for printing purpose
           }
        Mathutil.svdbksb(u, w, v, cols, cols, aTf, alpha);                 //backsubstitute to solve ATA.x  = ATb  ([u,w,v].xs = b)
      }*/

    }


    public static void solveWithSVDMultipleB(double [][]A, int rows, int cols, double [][]B, double [][] params){
        double[][] u = new double[rows][cols];
        double[][] v = new double[cols][cols];
        double[] w = new double[cols];

        for (int i = 0; i < rows; i++)for (int j = 0; j < cols; j++) u[i][j] = A[i][j]; //make a copy of A  coz we may needed to calculate other statistics like r2
        Mathutil.svd(u, w, v); //svd of uu to produce [u,w,v]
        double wmax = 0.0;
        for (int j = 0; j < cols; j++)if (w[j] > wmax) wmax = w[j];
        String remCoeff = "";
        double wmin = wmax * 1.0e-6; //set the threshold for singular values allowed to be non zero
        for (int j = 0; j < cols; j++)
          if (w[j] < wmin) {
            w[j] = 0.0;
            remCoeff = remCoeff + " " + j; //for printing purpose
          }

        int len = cols-1; //len should be equal to cols-1

        for (int i=0; i<len; i++){
          double [] rhs = new double [rows];
          for (int j=0; j<rows; j++)
            rhs[j] = B[j][i];
          Mathutil.svdbksb(u, w, v, rows, cols, rhs, params[i]); //back substitute [u,w,v] to b to get x ([u,w,v].xs = ys)
        }
    }
    
    //used for multiproduct and multi period scienario - rows = datalength,  cols = prod*period+1
    public static void solveWithSVDMultipleBMultiProd(double [][]A, int rows, int cols, double [][][]B, double [][][] params){
        double[][] u = new double[rows][cols];
        double[][] v = new double[cols][cols];
        double[] w = new double[cols];
        
        //decompose matrix with SVD
        for (int i = 0; i < rows; i++)for (int j = 0; j < cols; j++) u[i][j] = A[i][j]; //make a copy of A  coz we may needed to calculate other statistics like r2
        Mathutil.svd(u, w, v); //svd of uu to produce [u,w,v]
        double wmax = 0.0;
        for (int j = 0; j < cols; j++)if (w[j] > wmax) wmax = w[j];
        String remCoeff = "";
        double wmin = wmax * 1.0e-6; //set the threshold for singular values allowed to be non zero
        for (int j = 0; j < cols; j++)
          if (w[j] < wmin) {
            w[j] = 0.0;
            remCoeff = remCoeff + " " + j; //for printing purpose
          }
        
        //now back substitute for each right-hand-side
        double [] rhs = new double [rows];
        for (int i=0; i<B.length; i++)					//for each product
    		for (int j=0; j<B[0][0].length; j++){		//for each period
    			for (int k=0; k<B[0].length; k++){ 		//make RHS and backsubstitute to get parameters of the demand model for product in that period  - a equation
        			rhs[k] = B[i][k][j];				
        		}
    			Mathutil.svdbksb(u, w, v, rows, cols, rhs, params[i][j]); //back substitute [u,w,v] to b to get x ([u,w,v].xs = ys)
        	}        
    }

    //helping methods
    static final double abs(double a)
    {
      return (a < 0.) ? -a  : a;
    }

    static final double pythag(double a, double b)
    {
      return Math.sqrt(a*a + b*b);
    }

    static final double SIGN(double a,double b)
    {
      return ((b) >= 0. ? abs(a) : -abs(a));
    }

    public static void transpose (double [][]a, int rh, int ch, double [][]b)
    {
        int i, j;
        for (i = 0; i < rh; i++)
            for (j = 0; j < ch; j++)
                b[j][i] = a[i][j];
    }

    //multiply matrix by matrix
    public static void multMM (double [][]a, int rh, int ch, double [][]b, double [][]c)
    /*
     * Multiply matrix a[rl..rh][cl..ch] by matrix b[cl..ch][rl..rh] to give matrix
     * c[rl..rh][rl..rh].
     */
    {
        int i, j, k;
        double sum = 0.0;
        for (i = 0; i < rh; i++)
        {
            for (j = 0; j < rh; j++)
            {
                sum = 0.0;
                for (k = 0; k < ch; k++)
                    sum += a[i][k] * b[k][j];
                c[i][j] = sum;
            }
        }
    }

    //multiply matrix by vector
    public static void multMV (double [][]a, int rh, int ch, double [] b, double []c)
    /*
     * Multiply matrix a[rl..rh][cl..ch] by vector b[cl..ch] to give vector
     * c[rl..rh].
     */
    {
        int i, k;
        for (i = 0; i < rh; i++)
        {
            double sum = 0.0;
            for (k = 0; k < ch; k++)
                sum += a[i][k] * b[k];
            c[i] = sum;
        }
    }

    //given a matrix, returns a copy of it
    public static double[][] copyFromMatrix(double matrix[][]){
      int row = matrix.length;
      double newMatrix[][] =  new double [row][];
      for (int i=0; i<row; i++){
        int col = matrix[i].length;
        newMatrix[i] = new double[col];
        for (int j=0; j<col; j++)
          newMatrix[i][j] = matrix[i][j];
      }
      return newMatrix;
    }

    //given a mean and stdev and a random seed, samples a number with guassian distribution
    public static double getGaussian(double mean, double std, Random rand) {
	return mean + std*rand.nextGaussian();
    }

    public static double map(double lb, double ub, double x){
        return x/(ub-lb);
    }

    public static double unmap(double lb, double ub, double x){
        return x*(ub-lb);
    }

    public static double findSum(double var[]){
        double sum = 0.0;
        for (int i=1; i<var.length; i++)
        	sum+=var[i];
        return sum;
      }
    
    public static int findSum(int var[]){
        int sum = 0;
        for (int i=1; i<var.length; i++)
        	sum+=var[i];
        return sum;
      }
    
    public static int findMinPos(int numVar, double var[]){
      double minVar = var[0];
      int minPos = 0;
      for (int i=1; i<numVar; i++){
        if (var[i] < minVar) minVar = var[i];
        minPos = i;
      }
      return minPos;
    }

    public static int findMaxPos(int numVar, double var[]){
      double maxVar = var[0];
      int maxPos = 0;
      for (int i=1; i<numVar; i++){
        if (var[i] < maxVar) maxVar = var[i];
        maxPos = i;
      }
      return maxPos;
    }

    public static double findMax(double var[]){
      int numVar = var.length;
      double maxVar = var[0];
      for (int i=1; i<numVar; i++)
        if (var[i] > maxVar) maxVar = var[i];
      return maxVar;
    }

    public static double findMax(int numVar, double var[]){
      double maxVar = var[0];
      for (int i=1; i<numVar; i++)
        if (var[i] > maxVar) maxVar = var[i];
      return maxVar;
    }

    public static int findMax(int numVar, int var[]){
      int maxVar = var[0];
      for (int i=1; i<numVar; i++)
        if (var[i] > maxVar) maxVar = var[i];
      return maxVar;
    }

    public static double findMax(double var[][]){
        double maxVar = var[0][0];
        for (int i=0; i<var.length; i++)
        	for (int j=0; j<var[i].length; j++)
        		if (var[i][j] > maxVar) maxVar = var[i][j];
        return maxVar;
      }
    
    public static double findMin(double var[]){
      int numVar = var.length;
      double minVar = var[0];
      for (int i=1; i<numVar; i++)
        if (var[i] < minVar) minVar = var[i];
      return minVar;
    }

    public static double findMin(int numVar, double var[]){
      double minVar = var[0];
      for (int i=1; i<numVar; i++)
        if (var[i] < minVar) minVar = var[i];
      return minVar;
    }

    public static int findMin(int numVar, int var[]){
      int minVar = var[0];
      for (int i=1; i<numVar; i++)
        if (var[i] < minVar) minVar = var[i];
      return minVar;
    }

    public static double findMin(double var[][]){
        double minVar = var[0][0];
        for (int i=0; i<var.length; i++)
        	for (int j=0; j<var[i].length; j++)
        		if (var[i][j] < minVar) minVar = var[i][j];
        return minVar;
      }

    public static double findMean(double var[]){
      int numVar = var.length;
      double avg = 0;
      for (int i=0; i<numVar; i++)
        avg += var[i];
      return (avg/ (double) numVar);
    }

    //given mean and the array of variable, estimates standard vediation
    public static double findStDev(double mean, double [] arr){
      int len = arr.length;
      double sumSqrdif = 0;
      for (int i=0; i<len; i++)
        sumSqrdif += (arr[i] - mean) * (arr[i] - mean);
      return Math.sqrt(sumSqrdif/(double)(len-1));
    }

    //given only array of variable, estimates standard vediation
    public static double findStDev(double [] arr){
      int len = arr.length;
      double mean = findMean(arr);
      double sumSqrdif = 0;
      for (int i=0; i<len; i++)
        sumSqrdif += (arr[i] - mean) * (arr[i] - mean);
      return Math.sqrt(sumSqrdif/(double)(len-1));
    }

    public static void findAvgArray(int numRow, int numVar,  double matrixVar[][], double avgArray[]){
      for (int i=0; i<numVar; i++){
        avgArray[i] = 0;
        for (int j = 0; j < numRow; j++)
          avgArray[i] += matrixVar[j][i];
        avgArray[i] = avgArray[i]/numRow;
      }
    }

    //round vector to closest integer
    public static void roundArray(int numVar, double arrayVar[]){
      for (int t=0; t<numVar; t++){
        arrayVar[t] = Math.floor(arrayVar[t]+0.5f);
      }
    }

    public static double getRMSE(double[][] observed, double[][] expected){
	    double mse = 0.0;
	    for (int i = 0; i < observed.length; i++) {
	      for (int j=0; j< observed.length; j++)
	        mse += (observed[i][j] - expected[i][j]) * (observed[i][j] - expected[i][j]);
	    }
	    mse = mse / (observed.length*observed[0].length);
	    return(Math.sqrt(mse)); //root mean squared error    	
    }

    //can use generic for all data types
    public static void sortRefGivenValueMinToMax(ArrayList <Integer> ref, ArrayList <Double> val){
      int len = ref.size();
      double tmpval;  //temporary value
      int 	tmpref; //temporary reference
      for (int i=0; i<len-1; i++){
        //find the position for maximum number in the array
        int minPos = i;
        for (int j=i+1; j<len; j++) if (val.get(minPos) > val.get(j)) minPos = j;
        //backup the curent val
        tmpval = val.get(i);
        tmpref = ref.get(i);
        //copy min value to current place
        val.set(i, val.get(minPos));
        ref.set(i, ref.get(minPos));
        //replace the max chrom with curent
        val.set(minPos,tmpval);
        ref.set(minPos,tmpref);
      }
    }

    //can use generic for all data types
    public static void sortRefGivenValueMaxToMin(ArrayList <Integer> ref, ArrayList <Double> val){
      int len = ref.size();
      double tmpval;  //temporary value
      int 	tmpref; //temporary reference
      for (int i=0; i<len-1; i++){
        //find the position for maximum number in the array
        int maxPos = i;
        for (int j=i+1; j<len; j++) if (val.get(maxPos)< val.get(j)) maxPos = j;
        //backup the curent val
        tmpval = val.get(i);
        tmpref = ref.get(i);
        //copy min value to current place
        val.set(i, val.get(maxPos));
        ref.set(i, ref.get(maxPos));
        //replace the max chrom with curent
        val.set(maxPos,tmpval);
        ref.set(maxPos,tmpref);
      }
    }

    //can use generic for all data types
    public static void sortMinToMax(double [] arr){
      int len = arr.length;
      double tmpval;  //temporary value
      for (int i=0; i<len-1; i++){
        //find the position for maximum number in the array
        int minPos = i;
        for (int j=i+1; j<len; j++) if (arr[minPos] > arr[j]) minPos = j;
        //backup the curent val
        tmpval = arr[i];
        //copy min value to current place
        arr[i] = arr[minPos];
        //replace the max chrom with curent
        arr[minPos] = tmpval;
      }
    }

    //can use generic for all data types
    public static void sortMaxToMin(double [] arr){
      int len = arr.length;
      double tmpval;  //temporary value
      for (int i=0; i<len-1; i++){
        //find the position for maximum number in the array
        int maxPos = i;
        for (int j=i+1; j<len; j++) if (arr[maxPos] < arr[j]) maxPos = j;
        //backup the curent val
        tmpval = arr[i];
        //copy max value to current place
        arr[i] = arr[maxPos];
        //replace the min chrom with curent
        arr[maxPos] = tmpval;
      }
    }

    //given a vector of numbers, estimates the cumulative probabilty distribution for it
    public static double[][] getCumDist(double [] arr){
      int len = arr.length;
      double[][] cumDistTmp = new double[len][2];  //len = maximum possible length

      //First sort the array
      double[] sortedArr = new double[len];
      for (int k = 0; k < len; k++) sortedArr[k] = arr[k];
      Mathutil.sortMinToMax(sortedArr);

      //Now estimate the cumalative distribution by identifying number of uniques
      double dist; //cumulative probability storage
      double unique = sortedArr[0];
      int totalUniques = 0;
      for (int k = 1; k < len; k++) {
        if (unique < sortedArr[k]) {
          dist = (double) k / len;   //since k starts with 0, this is ok. otherwise we would have to use k-1
          cumDistTmp[totalUniques][0] = unique;
          cumDistTmp[totalUniques][1] = dist;
          totalUniques++;
          unique = sortedArr[k];
        }
      }
      //for the final one
      cumDistTmp[totalUniques][0] = unique;
      cumDistTmp[totalUniques][1] = 1.0;  //cumulative probability for the final one is 1
      totalUniques++;

      double[][] cumDist = new double[totalUniques][2];
      for (int k = 0; k < totalUniques; k++) {
        cumDist[k][0] = cumDistTmp[k][0];
        cumDist[k][1] = cumDistTmp[k][1];
      }
      return cumDist;
    }

    //given a vector of numbers, estimates the univariate probability distribution for it
    public static double[][] getUnivariateProbDist(double [] arr){
      int len = arr.length;
      double[][] probDistTmp = new double[len][2];  //len = maximum possible length

      //First sort the array
      double[] sortedArr = new double[len];
      for (int k = 0; k < len; k++) sortedArr[k] = arr[k];
      Mathutil.sortMinToMax(sortedArr);

      //Now estimate the cumalative distribution by identifying number of uniques
      double unique = sortedArr[0];
      int totalUniques = 0;
      int counter = 1;
      for (int k = 1; k < len; k++) {
        if (unique < sortedArr[k]) {
          probDistTmp[totalUniques][0] = unique;
          probDistTmp[totalUniques][1] = (double) counter / len;
          totalUniques++;
          unique = sortedArr[k];
          counter = 0;
        }
        counter++;
      }
      //for the final one
      probDistTmp[totalUniques][0] = unique;
      probDistTmp[totalUniques][1] = (double) counter / len;
      totalUniques++;

      //now copy the distribution of unique variables to an array and return it
      double[][] univariateProbDist = new double[totalUniques][2];
      for (int k = 0; k < totalUniques; k++) {
        univariateProbDist[k][0] = probDistTmp[k][0];
        univariateProbDist[k][1] = probDistTmp[k][1];
      }
      return univariateProbDist;
    }

    // given a set and the 
	public static void getCombinationRec(int itr, int []comb, int order,  int []set, ArrayList <int[]> listSet){
		  if (order==0) {
			  listSet.add(comb.clone());
			  return;
		  }
		  for (int i=itr; i<set.length-order+1; i++){
				  comb[comb.length - order] = set[i];
			  	  getCombinationRec(i+1, comb, order-1,  set, listSet);				  	  
		  }
	  }
	  
	//calculate factorial of n, recursive method
	static int fact(int n) {	    	
	 	if (n <= 1) {
	   	    return 1;
	   	}
	   	else {
	   	    return n * fact(n-1);
	   	}
	}
	
	public static int decode(Boolean alleles[], int startPos, int endPos){
	    int accum, powerof2;
	    accum =0; powerof2=1;
	    for (int i = endPos-1; i>=startPos; i--){
	      if (alleles[i]) accum = accum + powerof2;
	      powerof2 = powerof2 * 2;
	    }
	    return(accum);
	}//{decode}
	  
	//decode boolean string to double string
	public static double [] decodedVariables(Boolean alleles[], int numVar, double [] minVar, double [] maxVar){
		double [] dv = new double[numVar];
		int blockSize = alleles.length/numVar;
		double maxDecode = Math.pow(2, blockSize);
		for (int i=0; i<numVar; i++)
			dv[i] = minVar[i] + (((maxVar[i]-minVar[i])/maxDecode)*decode(alleles,i*blockSize, (i+1)*blockSize));
		return dv;
	}
	  
	public static int toInt(boolean b){
		    if (b) return 1;
		      else return -1;
		  }
	  
	
	public static int [] randPermutation(int numb[], Random rand){
		int [] perm = new int[numb.length];
		int pos = 0;
		int tmp = 0;
		//copy
		for (int i=0; i<numb.length; i++)
			perm[i] = numb[i];
		//now do random permutation
		for (int i=0; i<perm.length; i++){
			pos = (int) (rand.nextDouble() * (perm.length-i));
			pos = i+pos;
			tmp = perm[pos];
			perm[pos] = perm[i];
			perm[i] = tmp;
		}				
		return perm;		
	}
		  
}
