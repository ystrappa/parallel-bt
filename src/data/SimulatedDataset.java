package data;

import mn.rep.UndirectedGraph;

/**
 * Simple class representing a tabular dataset (train set)
 * 
 * @author fschluter
 */
public class SimulatedDataset extends Dataset {

	/**
	 * Simulated dataset responds to one underlying dataset
	 */
	private UndirectedGraph trueGraph;
	
	
	/**
	 *   
	 */
	private double alpha;

	/**
	 * @param maxNumberOfExamples
	 *            Number specifying how many rows must be read from the file
	 *            that contains the training examples.
	 * @param cardinalities
	 *            Array specifying the cardinalities of the variables of the
	 *            domain.
	 * @param trueGraph 
	 */
	public SimulatedDataset(int maxNumberOfExamples, int[] cardinalities, UndirectedGraph trueGraph) {
		super(maxNumberOfExamples,cardinalities);
		this.numberOfVariables = cardinalities.length;
		this.trueGraph = trueGraph;
	}

	public UndirectedGraph getTrueGraph() {
		return trueGraph;
	}

}