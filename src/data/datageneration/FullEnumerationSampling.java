package data.datageneration;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

import util.BitsetUtils;

public class FullEnumerationSampling {

	private static int n = 6;
	private static double D = 8000;
	private static int repetitions = 10;
	private static String outputDir = "/home/fschluter/experiments/datasets/fullEnumeration";
	private static String generatingModel= "generatingModel3.n6";

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		String file = "data"+ File.separator + generatingModel+".fullEnum.csv";
		
		double[][] content = new double[(int) Math.pow(2, n)][repetitions];
		int lineCounts = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			for (String line; (line = br.readLine()) != null;) {
				String[] splittedLine = line.split(";");
				for (int i = 0; i < splittedLine.length; ++i) {
					content[lineCounts][i] = new Double(splittedLine[i]);
				}
				++lineCounts;
			}
		}

		for (int r = 1; r <= repetitions; ++r) {
			List<String> dataset = new ArrayList<String>();
			double prob = 0;
			for (int i = 0; i < content.length; i++) {
				prob += content[i][r-1];
				String datapoint = getStringBitsetInCSVFormat(i,n);
				for (int j = 0; j < content[i][r-1] * D; ++j) {
					dataset.add(datapoint);
				}
			}
			
			Collections.shuffle(dataset);
			save(dataset,r);
			
		}

	}

	private static String getStringBitsetInCSVFormat(int i, int n) {
			BitSet bitSet = BitSet.valueOf(new long[] { i });
			String bitsetString = "";
			for(int j = n-1; j>=0;--j){
				bitsetString+=bitSet.get(j)?"1":"0";
				if(j!=0) bitsetString+=",";
			}
			return bitsetString;
	}

	private static void save(List<String> dataset, int r) throws IOException {
		FileWriter fstream;
		BufferedWriter out;

		String outputFile = outputDir + File.separator
				+ generatingModel+".ds.2.r."+r+".seed.1.csv";
		fstream = new FileWriter(outputFile, false);
		out = new BufferedWriter(fstream);
		
		for(String d:dataset){
			out.write(d+"\n");
		}
		
		out.close();
	}

}
