package data.datageneration;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import util.JGraphtUtils;

/**
 * Reads the adjacency matrix of a graph from a file and then generates a number
 * of different Markov networks from its Maximum cliques, generating then a .mn
 * file for each Markov network. Furthermore, the complete joint posterior
 * distribution is generated and sampled with full enumeration. The outputFile
 * argument must be a string with the complete directory and the header name of
 * the files created. For example, -outputFile /home/foo/bar will create ten
 * files /home/foo/bar.r.{i}.mn for ten repetitions, and ten datasets
 * /home/foo/bar.r.{i}.csv
 * 
 * @author fschluter
 * 
 */
public class GenerateRandomMarkovNetworkAndSampleWithFullEnumeration {
	private static String outputFile = "/home/fschluter/experiments/datasets/sampled/realNets/curtis54";
	private static String networkFile = "/home/fschluter/experiments/datasets/sampled/realNets/curtis54.graph";
	private static int repetitions = 10;
	private static int domainSize = 2;
	private static int seed = 1;
	private static double D = 100;

	private static int n;
	
	public static void main(String[] args) throws IOException {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-networkFile")) {
				networkFile = args[i + 1];
			} else if (args[i].equals("-repetitions")) {
				repetitions = new Integer(args[i + 1]);
			} else if (args[i].equals("-seed")) {
				seed = new Integer(args[i + 1]);
			} else if (args[i].equals("-outputFile")) {
				outputFile = args[i + 1];
			} else if (args[i].equals("-domainSize")) {
				domainSize = new Integer(args[i + 1]);
			} else if (args[i].equals("-D")) {
				D = new Integer(args[i + 1]);
			}

		}

		/* read the generating model from a file */
		UndirectedGraph trueGraph = UndirectedGraphParser.readTrueGraph(networkFile);
		n = trueGraph.getNumberOfNodes();

		for (int rep = 1; rep <= repetitions; rep++) {
			Random random = new Random(rep*seed);
			HashMap<java.util.Set<String>, HashMap<String, Double>> factorization = new HashMap<java.util.Set<String>, HashMap<String, Double>>();
			try {
				/* Create file */
				FileWriter fstream = new FileWriter(outputFile + ".r." + rep +".seed."+seed+ ".mn", false);
				BufferedWriter out = new BufferedWriter(fstream);
				String fileContent = "";
				out.write(fileContent);
				for (int i = 0; i < n; ++i) {
					/* In this version, only binary domains are considered */
					fileContent += domainSize + ",";
				}
				/* Removing last comma */
				fileContent = fileContent.substring(0, fileContent.length() - 1);
				fileContent += "\n";
				fileContent += "MN { \n";
				out.write(fileContent);
				fileContent = "";

				SimpleGraph<String, DefaultEdge> g = JGraphtUtils.getJGraphTSimpleGraph(trueGraph);
				Collection<java.util.Set<String>> cliques = JGraphtUtils.getAllMaximalCliques(trueGraph);

				for (java.util.Set<String> clique : cliques) {
					int cliqueSize = clique.size();
					int[] vars = new int[cliqueSize];
					int[] aux = new int[cliqueSize];
					int[] domains = new int[cliqueSize];
					int x = 0;
					for (String var : clique) {
						vars[x] = new Integer(var);
						domains[x] = domainSize - 1;
						x++;
					}

					HashMap<String, Double> permutationsMap = getPermutations(aux, domains, 0, vars, random);
					factorization.put(clique, permutationsMap);
					for (String key : permutationsMap.keySet()) {
						double value = permutationsMap.get(key);
						fileContent += value + " " + key + "\n";
					}
				}

				fileContent += "} \n";
				out.write(fileContent);

				out.close();
			} catch (Exception e) {// Catch exception if any
				System.err.println("Error: " + e.getMessage());
			}

			sampleByfullEnumeration(factorization, rep);
		}
	}


	/**
	 * Construct a map whit a random probability for each permutation of an instantiated subset of the domain 
	 * @param n The domain variables
	 * @param Nr
	 * @param idx
	 * @param vars The values of the variables
	 * @param random Seed for randomization 
	 * @return A map whit a random probability for each permutation of an instantiated subset of the domain 
	 */
	public static HashMap<String, Double> getPermutations(int[] n, int[] Nr, int idx, int[] vars, Random random) {
		// String allPermutations = "";
		HashMap<String, Double> allPermutations = new HashMap<String, Double>();

		/* stop condition for the recursion [base clause] */
		if (idx == n.length) {
			String assignment = "";
			for (int i = 0; i < n.length; ++i) {
				assignment += "+v" + vars[i] + "_" + n[i] + " ";
			}
			HashMap<String, Double> permutations = new HashMap<String, Double>();
			permutations.put(assignment, random.nextDouble());
			return permutations;
		}

		for (int i = 0; i <= Nr[idx]; i++) {
			n[idx] = i;
			allPermutations.putAll(getPermutations(n, Nr, idx + 1, vars, random)); // recursive
			// invokation,
			// for
			// next
			// elements
		}
		return allPermutations;
	}
	
	/**
	 * Computes the joint distribution of the Markov network and the generates a dataset with the "Complete enumeration method"
	 * @param factorization
	 * @param rep
	 * @throws IOException
	 */
	private static void sampleByfullEnumeration(HashMap<Set<String>, HashMap<String, Double>> factorization, int rep)
			throws IOException {
		HashMap<Integer, Double> jointDistribution = getJointDistribution(factorization);

		double totalProb=0;
		int totalNumberOfDatapoints =0;
		List<String> dataset = new ArrayList<String>();
		int i = 0;
		for (int key : jointDistribution.keySet()) {
			BitSet completeAssignment = BitSet.valueOf(new long[] { key });
			String completeAssignmentCsvFormat = "";
			for (int j = n - 1; j >= 0; --j) {
				completeAssignmentCsvFormat += completeAssignment.get(j) ? "1" : "0";
				if (j != 0)
					completeAssignmentCsvFormat += ",";
			}
			
			double probability = jointDistribution.get(key);
			totalProb+=probability;
//			System.out.println("i" + i + " prob " + probability + " datapoints" + probability * D);
			for (int j = 0; j < probability * D; ++j) {
				dataset.add(completeAssignmentCsvFormat);
				++totalNumberOfDatapoints;
			}
			++i;
		}
		Collections.shuffle(dataset);
		FileWriter fstream = new FileWriter(outputFile + ".r." + rep + ".seed."+seed+".csv", false);
		BufferedWriter out = new BufferedWriter(fstream);
		for (String d : dataset) {
			out.write(d + "\n");
		}
		out.close();
	}

	/**
	 * @param factorization
	 * @return
	 */
	private static HashMap<Integer, Double> getJointDistribution(
			HashMap<Set<String>, HashMap<String, Double>> factorization) {
		HashMap<Integer, Double> jointDistribution = new HashMap<Integer, Double>();
		HashMap<Integer, Double> logUnnormalizedDistribution = new HashMap<Integer, Double>();
		double numberOfCompleteAssignments = Math.pow(domainSize, n );
		double normalizationConstant = 0;
		double logNormalizationConstant = 0;
		
		/* for each complete assignment in a binary domain (HARDCODE FOR BINARY DOMAINS) */
		for (int i = 0; i < numberOfCompleteAssignments; i++) {
			BitSet completeAssignment = BitSet.valueOf(new long[] { i });
			/*for each clique in the graph factorization */
			double logUnnnormalizedProbability = 0;
			double unnnormalizedProbability = 1;
			for (Set<String> clique : factorization.keySet()) {
				/* the key for each clique is a String with variable value pairs 
				 * (with the same format that uses the Libra toolkit format)
				 * the variables are in the clique, 
				 * and the values in the current complete assignment*/
				String assignment ="";
				for (String variable : clique) {
					String value = completeAssignment.get(new Integer(variable))?"1":"0";
					assignment += "+v" + variable + "_" + value + " ";
				}
				
				HashMap<String,Double> cliqueFactor = factorization.get(clique);
				/*the corresponding value in the function, 
				 * according with the current values of the clique*/
				double value = cliqueFactor.get(assignment);
				/* the value for the complete assignment 
				 * is the product of the factor function 
				 * for each cliques in the current graph*/
				unnnormalizedProbability *= value;
				logUnnnormalizedProbability += Math.log(value);
				
			}

			jointDistribution.put(i, unnnormalizedProbability);
			logUnnormalizedDistribution.put(i, logUnnnormalizedProbability);
			/**/
			logNormalizationConstant += Math.exp(logUnnnormalizedProbability);
			normalizationConstant += unnnormalizedProbability;

		}

//		HashMap<String, Double> normalizedDistribution = new HashMap<String, Double>();
//		HashMap<String, Double> logNormalizedDistribution = new HashMap<String, Double>();

//		for (int i = 0; i < numberOfCompleteAssignments; i++) {
//			BitSet completeAssignment = BitSet.valueOf(new long[] { i });
//			String completeAssignmentCsvFormat = "";
//			for (int j = n - 1; j >= 0; --j) {
//				completeAssignmentCsvFormat += completeAssignment.get(j) ? "1" : "0";
//				if (j != 0)
//					completeAssignmentCsvFormat += ",";
//			}
//			double normalizedProbability = unnormalizedDistribution.get(i)/normalizationConstant;
//			double logNormalizedProbability = Math.exp(logUnnormalizedDistribution.get(i))/logNormalizationConstant;
//			normalizedDistribution.put(completeAssignmentCsvFormat, normalizedProbability);
//			logNormalizedDistribution.put(completeAssignmentCsvFormat, logNormalizedProbability);
//		}
		
		normalizationConstant = 0;
		for(Double value: jointDistribution.values()){
			normalizationConstant +=value;
		}
		
		for(int key: jointDistribution.keySet()){
			double normalizedValue = jointDistribution.get(key)/normalizationConstant;
			jointDistribution.put(key, normalizedValue);
		}
		
		double totalProb=0;
		double totalDatapoints=0;
		int i = 0;
		for(Double value: jointDistribution.values()){
			++i;
			totalProb+=value;
			totalDatapoints+=value*D;
		}
System.out.println("total prob: "+ totalProb + " totaldatapoints " +totalDatapoints);
		return jointDistribution ;
	}



}
