package data.datageneration;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Random;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import util.JGraphtUtils;


/**
 * Reads the adjacency matrix of a graph from a file and then generates a number
 * of different Markov networks from its Maximum cliques, generating then a .mn
 * file for each Markov network. Then this file can be used with Libra to
 * generate artificial data using Gibbs sampler.
 * 
 * @author fschluter
 * 
 */
public class GenerateRandomMarkovNetworkForLibra {
	private static String outputFile = "/home/fschluter/experiments/datasets/sampled/fullEnum/generatingModel2.n6";
	private static String networkFile = "/home/fschluter/experiments/datasets/sampled/fullEnum/generatingModel2.n6.graph";
	private static int repetitions = 10;
	private static int domainSize = 2;

	public static void main(String[] args) throws IOException {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-networkFile")) {
				networkFile = args[i + 1];
			} else if (args[i].equals("-repetitions")) {
				repetitions = new Integer(args[i + 1]);
			} else if (args[i].equals("-outputFile")) {
				outputFile = args[i + 1];
			} else if (args[i].equals("-domainSize")) {
				domainSize =  new Integer(args[i + 1]);
			}
		}

		/* read the generating model from a file */
		UndirectedGraph trueGraph = UndirectedGraphParser
				.readTrueGraph(networkFile);
		int n = trueGraph.getNumberOfNodes();

		for (int rep = 1; rep <= repetitions; rep++) {
			Random random = new Random(rep);
			try {
				/* Create file */
				FileWriter fstream = new FileWriter(outputFile + ".r." + rep
						+ ".mn");
				BufferedWriter out = new BufferedWriter(fstream);
				String fileContent = "";
				out.write(fileContent);
				for (int i = 0; i < n; ++i) {
					/* In this version, only binary domains are considered */
					fileContent += domainSize + ",";
				}
				/* Removing last comma */
				fileContent = fileContent
						.substring(0, fileContent.length() - 1);
				fileContent += "\n";
				fileContent += "MN { \n";
				out.write(fileContent);
				fileContent = "";
				
				SimpleGraph<String, DefaultEdge> g = JGraphtUtils.getJGraphTSimpleGraph(trueGraph);
				Collection<java.util.Set<String>> cliques = JGraphtUtils.getAllMaximalCliques(trueGraph);
				for (java.util.Set<String> clique : cliques) {
					int cliqueSize = clique.size();
					int[] vars = new int[cliqueSize];
					int[] aux = new int[cliqueSize];
					int[] domains = new int[cliqueSize];
					int x = 0;
					for (String var : clique) {
						vars[x] = new Integer(var);
						domains[x] = domainSize-1;
						x++;
					}

					fileContent = getPermutations(aux, domains, 0, vars, random);
					out.write(fileContent);

				}

				fileContent = "} \n";
				out.write(fileContent);

				out.close();
			} catch (Exception e) {// Catch exception if any
				System.err.println("Error: " + e.getMessage());
			}
		}
	}


	public static String getPermutations(int[] n, int[] Nr, int idx,
			int[] vars, Random random) {
		String allPermutations = "";
		/* stop condition for the recursion [base clause] */
		if (idx == n.length) {
			/* String assignment = "0.000 "; */
			String assignment = random.nextDouble() + " ";
			for (int i = 0; i < n.length; ++i) {
				assignment += "+v" + vars[i] + "_" + n[i] + " ";
			}
			assignment += "\n";
			return assignment;
		}
		for (int i = 0; i <= Nr[idx]; i++) {
			n[idx] = i;
			allPermutations += getPermutations(n, Nr, idx + 1, vars, random); // recursive
			// invokation,
			// for
			// next
			// elements
		}
		return allPermutations;
	}

}
