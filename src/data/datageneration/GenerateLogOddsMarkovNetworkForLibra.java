package data.datageneration;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;

/**
 * Reads the adjacency matrix of a graph from a file and then generates a number
 * of different Markov networks using log-odds and the external parameter
 * EPSILON. EPSILON measures how strong are the dependences of the resulting
 * distribution. It only works for binary domains. Finally, a .mn file for each
 * Markov network is generated. Then this file can be used with Libra to
 * generate artificial data using Gibbs sampler.
 * 
 * @author fschluter
 * 
 */
public class GenerateLogOddsMarkovNetworkForLibra {
	private static String outputFile = "/home/fschluter/generatingModel2.n6";
	private static String networkFile = "/home/fschluter/experiments/datasets/minimalCases/generatingModel2.n6.graph";
	private static int repetitions = 10;
	private static double epsilon = 1;

	public static void main(String[] args) throws IOException {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-networkFile")) {
				networkFile = args[i + 1];
			} else if (args[i].equals("-repetitions")) {
				repetitions = new Integer(args[i + 1]);
			} else if (args[i].equals("-outputFile")) {
				outputFile = args[i + 1];
			} else if (args[i].equals("-epsilon")) {
				epsilon = new Double(args[i + 1]);
			}
		}

		/* read the generating model from a file */
		UndirectedGraph trueGraph = UndirectedGraphParser
				.readTrueGraph(networkFile);
		int n = trueGraph.getNumberOfNodes();

		for (int rep = 1; rep <= repetitions; rep++) {
			Random random = new Random(rep);
			try {
				/* Create file */
				FileWriter fstream = new FileWriter(outputFile + ".r." + rep
						+ ".mn");
				BufferedWriter out = new BufferedWriter(fstream);
				String fileContent = "";
				out.write(fileContent);
				for (int i = 0; i < n; ++i) {
					/* In this version, only binary domains are considered */
					fileContent += 2 + ",";
				}
				/* Removing last comma */
				fileContent = fileContent
						.substring(0, fileContent.length() - 1);
				fileContent += "\n";
				fileContent += "MN { \n";
				out.write(fileContent);
				fileContent = "";

				for (int i = 0; i < n; i++) {
					for (int j = i + 1; j < n; j++) {
						if (trueGraph.existEdge(i, j)) {
							double p00, p01, p11, p10;
							if (epsilon >= 0) {
								double sigma = 0.2, mean = 1.0;

//								p00 = random.nextGaussian() * sigma + epsilon
//										/ 2.0 + mean;
//								p11 = random.nextGaussian() * sigma + epsilon
//										/ 2.0 + mean;
//								p01 = random.nextGaussian() * sigma + mean;
//								p10 = p00 + p11 - p01 - epsilon;
								
								double e = random.nextDouble();
								
								p00 = random.nextDouble();
								p11 = random.nextDouble();
								p01 = random.nextDouble();
								p10 = p00 + p11 - p01 - epsilon;
								
							} else { // understood as totally random
								p00 = random.nextDouble();
								p11 = random.nextDouble();
								p01 = random.nextDouble();
								p10 = random.nextDouble();
							}
							fileContent += p00 + " +v" + i + "_0 +v" + j
									+ "_0 \n";
							fileContent += p01 + " +v" + i + "_0 +v" + j
									+ "_1 \n";
							fileContent += p10 + " +v" + i + "_1 +v" + j
									+ "_0 \n";
							fileContent += p11 + " +v" + i + "_1 +v" + j
									+ "_1 \n";

						}
					}
				}

				fileContent += "} \n";
				out.write(fileContent);

				out.close();
			} catch (Exception e) {// Catch exception if any
				System.err.println("Error: " + e.getMessage());
			}
		}
	}

}
