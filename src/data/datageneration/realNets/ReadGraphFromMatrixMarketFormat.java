package data.datageneration.realNets;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.graphstream.graph.Edge;

import mn.rep.UndirectedGraph;

public class ReadGraphFromMatrixMarketFormat {

	
    public static UndirectedGraph read(String filename, boolean allowLoops) throws java.io.IOException {
    	
        InputStream s = new FileInputStream(filename);
        BufferedReader br = new BufferedReader(new InputStreamReader(s));
        
        // read type code initial line
        String line = br.readLine();
        
        // read comment lines if any
        boolean comment = true;
        while (comment) {
            line = br.readLine();
            comment = line.startsWith("%");
        }
        
        // line now contains the size information which needs to be parsed
        String[] str = line.split("( )+");
        int nRows = (Integer.valueOf(str[0].trim())).intValue();
        int nColumns = (Integer.valueOf(str[1].trim())).intValue();
        int nNonZeros = (Integer.valueOf(str[2].trim())).intValue();
        
        UndirectedGraph uGraph = new UndirectedGraph(nColumns>nRows?nColumns:nRows);

        while (true) {
            line = br.readLine();
            if (line == null)  break;
            str = line.split("( )+");
            int i = (Integer.valueOf(str[0].trim())).intValue();
            int j = (Integer.valueOf(str[1].trim())).intValue();
            if(i!=j)
            uGraph.addEdge(i-1, j-1);
            else
            	 if(allowLoops) uGraph.addEdge(i-1, j-1);
        }
        br.close();
        
        return uGraph;
    }

}
