package data.datageneration.realNets;

import java.io.File;
import java.io.IOException;

import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import util.GraphStreamUtils;

public class CreateRealNetworkGraphFromMatrixMarkeFormat {

	private static String resultsFile = "/home/fschluter";
//	private static String resultsFile = "/home/fschluter/experiments/datasets/sampled/realNets";

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String args[]) throws IOException {
		/**
		 * http://www.cise.ufl.edu/research/sparse/matrices/Newman/dolphins.html
		 */
		// generateNetworkFiles("src/data/datageneration/realNets/dolphins.mtx",
		// resultsFile + File.separator + "dolphins.graph");

		/**
		 * http://www.cise.ufl.edu/research/sparse/matrices/Newman/karate.html
		 */
//		generateNetworkFiles("src/data/datageneration/realNets/karate.mtx",
//				resultsFile + File.separator + "karate.graph");

		
		/**
		 * http://www.cise.ufl.edu/research/sparse/matrices/Newman/adjnoun.html
		 */
//		generateNetworkFiles("src/data/datageneration/realNets/adjnoun.mtx",
//				resultsFile + File.separator + "adjnoun.graph",false);

		/**
		 * http://www.cise.ufl.edu/research/sparse/matrices/HB/bcspwr01.html
		 */
//		generateNetworkFiles("src/data/datageneration/realNets/bcspwr01.mtx",
//				resultsFile + File.separator + "bcspwr01.graph",true);

//		/**
//		 * https://www.cise.ufl.edu/research/sparse/matrices/HB/jagmesh3.html
//		 */
//		generateNetworkFiles("src/data/datageneration/realNets/jagmesh3.mtx",
//				resultsFile + File.separator + "jagmesh3.graph",true);
				
//		/**
//		 * https://www.cise.ufl.edu/research/sparse/matrices/HB/ibm32.html
//		 */
//		generateNetworkFiles("src/data/datageneration/realNets/ibm32.mtx",
//				resultsFile + File.separator + "ibm32.graph",true);
				

		generateNetworkFiles("src/data/datageneration/realNets/ibm32.mtx",
				resultsFile + File.separator + "ibm32.graph",false);

			/**
			 * 		http://math.nist.gov/MatrixMarket/data/Harwell-Boeing/smtape/curtis54.html
			 */
//			generateNetworkFiles("src/data/datageneration/realNets/curtis54.mtx",
//					resultsFile + File.separator + "curtis54.graph",true);


		/**
		 * 		http://math.nist.gov/MatrixMarket/data/Harwell-Boeing/smtape/will57.html
		 */
//		generateNetworkFiles("src/data/datageneration/realNets/will57.mtx",
//				resultsFile + File.separator + "will57.graph",true);

		
			/**
		 * http://www.cise.ufl.edu/research/sparse/matrices/Bai/qh882.html
		 */
//		generateNetworkFiles("src/data/datageneration/realNets/qh882.mtx",
//				resultsFile + File.separator + "qh882.graph", false);
		

		/**
		 * http://www.cise.ufl.edu/research/sparse/matrices/Newman/polbooks
		 */
//		generateNetworkFiles("src/data/datageneration/realNets/polbooks.mtx",
//				resultsFile + File.separator + "polbooks.graph",true);

		/**
		 * http://www.cise.ufl.edu/research/sparse/matrices/HB/arc130.html
		 */
//		generateNetworkFiles("src/data/datageneration/realNets/arc130.mtx",
//				resultsFile + File.separator + "arc130.graph",true);

	}

	private static void generateNetworkFiles(String path, String adjacencyMatrixFileName, boolean showGraph) throws IOException {
		UndirectedGraph ugraph = ReadGraphFromMatrixMarketFormat.read(path,false);
		
//		UndirectedGraphParser.writeGraphInAdjacencyMatrixFormat(ugraph, adjacencyMatrixFileName);

		int irregularity = ugraph.computeIrregularity();
//		double weightedIrregularity = g.computeWeightedIrregularity();
//		double weightedIrregularity = computeIrregularity(g);
		 System.out.println(" n: "+ ugraph.getNumberOfNodes() +
				 " edges: " + ugraph.getNumberOfEdges() + " irr " +   
		 irregularity );
		 
		if(showGraph) GraphStreamUtils.showGraph(ugraph);
	}

}
