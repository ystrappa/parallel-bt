package test.ib.bs;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import mn.learn.bs.BDEu;
import mn.rep.UndirectedGraph;
import data.Dataset;

public class TestBayesianScore {
	private static int alpha = 1; // uniform prior
	private static int domainSize=2;

	public static void main(String[] args) {
		String fullPathToDataset = "data/example12BinaryVariables.csv";
		int n = 12;
		List<Integer> Ds = Arrays.asList(10,50,100,500,1000,5000,10000); // read only
																// first 10
																// examples
		System.out.println("D true empty fully ");
		for (int D : Ds) {
			Dataset dataset = new Dataset(D, Dataset.getUniformDomainSizes(n,domainSize));
			try {
				dataset = dataset.readDataset(new java.io.File(
						fullPathToDataset), D, 1);
				// compute the score of some graph
				UndirectedGraph trueGraph = new UndirectedGraph(n);

				//
				// TRUE GRAPH OF THE EXAMPLE DATASET
				//
				// VAR VAR CHILDREN
				// 0 :
				// 1 : 6
				// 2 :
				// 3 : 7 9
				// 4 :
				// 5 :
				// 6 : 8 1
				// 7 : 8 3 10
				// 8 : 6 7
				// 9 : 3
				// 10 : 7
				// 11 :

				trueGraph.addEdge(1, 6);
				trueGraph.addEdge(3, 7);
				trueGraph.addEdge(3, 9);
				trueGraph.addEdge(6, 8);
				trueGraph.addEdge(6, 1);
				trueGraph.addEdge(7, 8);
				trueGraph.addEdge(7, 3);
				trueGraph.addEdge(7, 10);
				trueGraph.addEdge(8, 6);
				trueGraph.addEdge(8, 7);
				trueGraph.addEdge(9, 3);
				trueGraph.addEdge(10, 7);

				double bayesianScoreOfTrueStructure = new BDEu(alpha)
						.computeScore(trueGraph, dataset).getTotalScore();

				double bayesianScoreOfEmptyStructure = new BDEu(alpha)
						.computeScore(new UndirectedGraph(n), dataset)
						.getTotalScore();

				double bayesianScoreOfFullyStructure = new BDEu(alpha)
						.computeScore(new UndirectedGraph(n, true), dataset)
						.getTotalScore();

				System.out.println(D + " " + bayesianScoreOfTrueStructure + " "
						+ bayesianScoreOfEmptyStructure + " "
						+ bayesianScoreOfFullyStructure);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
