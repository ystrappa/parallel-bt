package test.ib.ibmap.search;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import data.Dataset;
import data.SimulatedDataset;
import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.algs.ibmap.score.CIBScore;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.learn.ib.algs.ibmap.score.MPL;
import mn.learn.ib.algs.ibmap.score.PenalizedIBScore;
import mn.learn.ib.algs.ibmap.score.RIBScore;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.it.SimulatedBayesianTest;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import mn.rep.UndirectedGraphsQualityMeasures;
import util.BitsetUtils;
import util.JGraphtUtils;

/**
 * Experiment for testing the consistency of Markov network structure scoring
 * functions for synthetic data
 * 
 * @author fschluter
 *
 */
public class SimulatedConsistencyExperiment {
	public static String[] scoringFunctions = { "ibscore", "ribscore" };
	private static String generatingModel = "generatingModel2.n5";
	private static String trueModelFile = "/home/fschluter/experiments/datasets/minimalCases/"
			+ generatingModel + ".graph";
	private static String resultsDir = "/home/fschluter/projects/current/IBMAP/exps/simScoringFunctions";

	// private static List<Integer> Ds = Arrays.asList(10,100,1000,10000);
	private static List<Double> uncertainties = Arrays.asList(0.2);
	private static List<Integer> domainSizes = Arrays.asList(2);
	private static List<Integer> Ds = Arrays.asList(250, 500, 1000, 2000, 4000);

	private static double[][] algorithmResults = new double[Ds.size()][8];

	private static SimulatedDataset simulatedDataset;
	private static boolean incremental = true;
	private static boolean onlyChordal = false;
	private static boolean landscapeFileEnabled = true;

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		parseParameters(args);
		for (int domainSize : domainSizes) {
			for (Double uncertainty : uncertainties) {
				for (String scoringFunctionString : scoringFunctions) {
					System.out.println("#### UNCERTAINTY: " + uncertainty
							+ " , DOMAIN SIZE: " + domainSize
							+ " , SCORING FUNCTION: " + scoringFunctionString);
					for (int D = 0; D < Ds.size(); D++) {
						exploreCompleteLandscape(scoringFunctionString, D,
								domainSize, uncertainty);
					}
				}
			}
		}
	}

	/**
	 * @param args
	 */
	private static void parseParameters(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-scoringFunctions")) {
				List<String> aux = new ArrayList<String>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					aux.add(args[dIndex]);
					++dIndex;
				}
				scoringFunctions = new String[aux.size()];
				scoringFunctions = aux.toArray(scoringFunctions);

			} else if (args[i].equals("-Ds")) {
				Ds = new ArrayList<Integer>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					Ds.add(new Integer(args[dIndex]));
					++dIndex;
				}
				algorithmResults = new double[Ds.size()][8];
			} else if (args[i].equals("-domainSize")) {
				domainSizes = new ArrayList<Integer>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					domainSizes.add(new Integer(args[dIndex]));
					++dIndex;
				}
			} else if (args[i].equals("-uncertainties")) {
				uncertainties = new ArrayList<Double>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					uncertainties.add(new Double(args[dIndex]));
					++dIndex;
				}
			} else if (args[i].equals("-trueModelFile")) {
				trueModelFile = args[i + 1];
			} else if (args[i].equals("-generatingModel")) {
				generatingModel = args[i + 1];
			} else if (args[i].equals("-resultsDir")) {
				resultsDir = args[i + 1];
			} else if (args[i].equals("-incremental")) {
				incremental = new Boolean(args[i + 1]);
			} else if (args[i].equals("-onlyChordal")) {
				onlyChordal = new Boolean(args[i + 1]);
			}
		}
	}

	/**
	 * @param scoringFunctionString
	 * @param k
	 * @param domainSize
	 * @param uncertainty
	 * @throws IOException
	 */
	private static void exploreCompleteLandscape(String scoringFunctionString,
			int k, int domainSize, double uncertainty) throws IOException {
		int D = Ds.get(k);
		System.out.println("#generatingModel " + generatingModel
				+ " domainSize=" + domainSize + ", D=" + D + "(time:"
				+ Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + ":"
				+ Calendar.getInstance().get(Calendar.MINUTE) + ")");

		UndirectedGraph trueGraph = UndirectedGraphParser
				.readTrueGraph(trueModelFile);
		int n = trueGraph.getNumberOfNodes();

		simulatedDataset = new SimulatedDataset(D,
				Dataset.getUniformDomainSizes(n, domainSize), trueGraph);

		// dataset.setUseCache();

		GraphScorable scoringFunction = null;
		if (scoringFunctionString.equals("ibscore")) {
			scoringFunction = new IBScore();
			((IBScore) scoringFunction).setTest(new SimulatedBayesianTest(
					uncertainty));
		} else if (scoringFunctionString.equals("mpl")) {
			scoringFunction = new MPL();
		} else if (scoringFunctionString.equals("ribscore")) {
			scoringFunction = new RIBScore();
			((RIBScore) scoringFunction).setTest(new SimulatedBayesianTest(
					uncertainty));
		} else if (scoringFunctionString.equals("iribscore")) {
			scoringFunction = new IBScore(true);
		} else if (scoringFunctionString.equals("giribscore")) {
			scoringFunction = new RIBScore(true);
		} else if (scoringFunctionString.equals("cibscore")) {
			scoringFunction = new CIBScore();
		} else if (scoringFunctionString.equals("pibscore")) {
			scoringFunction = new PenalizedIBScore();
		}

		UndirectedGraphScore trueGraphScore = scoringFunction.computeScore(
				trueGraph, simulatedDataset);

		UndirectedGraph outputGraph = null;
		double scoreOfOutputGraph = 0;
		long runtime = 0;

		int inconsistentScoreCases = 0; // amount of cases where
										// a structure has
										// higher score than the
										// correct structure
		double bestScore = Integer.MIN_VALUE;

		// Create file
		String landscapeFile = resultsDir + File.separator + "landscape"
				+ File.separator + "landscape_generatingModel_"
				+ generatingModel + "_uncertainty_" + uncertainty
				+ "_domainSize_" + domainSize + "_scoringFunction_"
				+ scoringFunctionString + "_D_" + D + ".csv";
		
		FileWriter fstream = null; 
		BufferedWriter out = null;
		
		if(landscapeFileEnabled ){
			fstream = new FileWriter(landscapeFile, true);
			out = new BufferedWriter(fstream);
			out.write("# generativeModel uncertainty domainSize D scoringFunction structureID score HD FP FN"
					+ "\n");
		}
		
		// List<UndirectedGraph> allPossibleGraphs =
		// UndirectedGraph.getAllPossibleGraphs(n);
		UndirectedGraphScore lastScore = null;

		/*
		 * This loop generates all the possible graphs without saving any graph
		 * in memory
		 */
		for (int i = 0; i < Math.pow(2, (n * (n - 1) / 2)); ++i) {
			UndirectedGraph g = new UndirectedGraph(n);
			g.setBitset(BitsetUtils.convert(i)); //

			/*
			 * when onlyChordal is false, it computes the score for all the
			 * graphs, but when onlyChordal is true, it computes the score only
			 * for chordal graphs
			 */
			if (!onlyChordal || JGraphtUtils.isChordal(g)) {
				UndirectedGraphScore gScore;
				if (!incremental) {
					gScore = scoringFunction.computeScore(g, simulatedDataset);
				} else {
					if (lastScore == null) {
						gScore = scoringFunction.computeScore(g,
								simulatedDataset);
					} else {
						gScore = scoringFunction.computeScore(g, lastScore,
								simulatedDataset); // incremental
					}
				}
				lastScore = gScore;

				double score = gScore.getTotalScore();
				int hd = UndirectedGraphsQualityMeasures.hammingDistance(
						trueGraph, g);
				int fp = UndirectedGraphsQualityMeasures.falsePositives(
						trueGraph, g);
				int fn = UndirectedGraphsQualityMeasures.falseNegatives(
						trueGraph, g);

				if(landscapeFileEnabled ){
				out.write(generatingModel + " " + uncertainty + " "
						+ domainSize + " " + D + " " + scoringFunctionString
						+ " " + BitsetUtils.convert(g.getBitset()) + " "
						+ score + " " + hd + " " + fp + " " + fn + "\n");
				}
				
				if (score > bestScore) {
					bestScore = score;
					outputGraph = gScore.getUndirectedGraph();
				}

				// count cases where a structure has higher score
				// than the correct structure
				if (score >= trueGraphScore.getTotalScore()) {
					if (!trueGraph.getBitsetString()
							.equals(g.getBitsetString())) {
						++inconsistentScoreCases;
					}
				}
			}
		}
		if(landscapeFileEnabled ){
			out.close();
		}

		scoreOfOutputGraph = bestScore;

		// System.out.println("learned structure \n" + outputGraph);
		algorithmResults[k][0] = D;
		algorithmResults[k][1] = UndirectedGraphsQualityMeasures
				.hammingDistance(trueGraph, outputGraph);
		algorithmResults[k][2] = UndirectedGraphsQualityMeasures.fMeasure(
				trueGraph, outputGraph);
		algorithmResults[k][3] = runtime;
		algorithmResults[k][4] = UndirectedGraphsQualityMeasures
				.falsePositives(trueGraph, outputGraph);
		algorithmResults[k][5] = UndirectedGraphsQualityMeasures
				.falseNegatives(trueGraph, outputGraph);

		// if hamming distance is zero, optimal result were
		// obtained
		algorithmResults[k][6] = algorithmResults[k][1] == 0 ? 1 : 0;
		algorithmResults[k][7] = inconsistentScoreCases;

		String resultsFile = resultsDir + File.separator
				+ "rawResults_generatingModel_" + generatingModel
				+ "_uncertainty_" + uncertainty + "_domainSize_" + domainSize
				+ "_scoringFunction_" + scoringFunctionString + ".csv";

		saveRawResults(scoringFunctionString, D, resultsFile,
				algorithmResults[k][1], algorithmResults[k][2],
				algorithmResults[k][3], algorithmResults[k][4],
				algorithmResults[k][5], algorithmResults[k][6],
				algorithmResults[k][7], scoreOfOutputGraph);
	}

	/**
	 * @param scoringFunction
	 * @param D
	 * @param outputFile
	 * @param hammingAlgorithm
	 * @param fMeasuresAlgorithm
	 * @param runtimeAlgorithmResults
	 * @param falsePositivesAlgorithmResults
	 * @param falseNegativesAlgorithmResults
	 * @param optimalFound
	 * @param scoreInconsistency
	 * @param scoreOfOutputGraph
	 */
	private static void saveRawResults(String scoringFunction, int D,
			String outputFile, double hammingAlgorithm,
			double fMeasuresAlgorithm, double runtimeAlgorithmResults,
			double falsePositivesAlgorithmResults,
			double falseNegativesAlgorithmResults, double optimalFound,
			double scoreInconsistency, double scoreOfOutputGraph) {

		try {
			// Create file
			FileWriter fstream;
			BufferedWriter out;

			fstream = new FileWriter(outputFile, true);
			out = new BufferedWriter(fstream);
			out.write(scoringFunction + " " + D + " " + " " + hammingAlgorithm
					+ " " + fMeasuresAlgorithm + " " + runtimeAlgorithmResults
					+ " " + falsePositivesAlgorithmResults + " "
					+ falseNegativesAlgorithmResults + " " + optimalFound + " "
					+ scoreInconsistency + " " + scoreOfOutputGraph);

			out.write("\n");
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

}
