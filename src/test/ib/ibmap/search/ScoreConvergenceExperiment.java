package test.ib.ibmap.search;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.algs.ibmap.score.CIBScore;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.learn.ib.algs.ibmap.score.MPL;
import mn.learn.ib.algs.ibmap.score.PenalizedIBScore;
import mn.learn.ib.algs.ibmap.score.RIBScore;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.it.SimulatedBayesianTest;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import mn.rep.UndirectedGraphsQualityMeasures;
import util.JGraphtUtils;
import data.Dataset;
import data.SimulatedDataset;

/**
 * Experiment for testing the consistency of Markov network structure scoring
 * functions for synthetic data
 * 
 * @author fschluter
 *
 */
public class ScoreConvergenceExperiment {
	public static String[] scoringFunctions = { "ibscore", "ribscore"};
	private static String dir = "/home/fschluter/experiments/datasets/sampled/undirected/mpl";
	private static String trueModelFile = "/home/fschluter/experiments/datasets/sampled/undirected/mpl/generatingModel2.n5.graph";
	private static String generatingModel = "generatingModel3.n6";
	private static String resultsDir = "/home/fschluter/projects/current/IBMAP/exps/scoreConvergence";

	private static int rep = 1;
	private static int seed = 1;
	private static int domainSize = 2;
	private static List<Integer> Ds = Arrays.asList(250, 500, 1000, 2000, 4000,
			8000);

	private static Dataset dataset;
	private static String datasetName;
	private static double uncertainty=.1;

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		parseParameters(args);
		for (String scoringFunctionString : scoringFunctions) {
			doConsistencyAnalysis(scoringFunctionString);
		}
	}

	/**
	 * @param args
	 */
	private static void parseParameters(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-scoringFunctions")) {
				List<String> aux = new ArrayList<String>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					aux.add(args[dIndex]);
					++dIndex;
				}
				scoringFunctions = new String[aux.size()];
				scoringFunctions = aux.toArray(scoringFunctions);

			} else if (args[i].equals("-Ds")) {
				Ds = new ArrayList<Integer>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					Ds.add(new Integer(args[dIndex]));
					++dIndex;
				}
			} else if (args[i].equals("-trueModelFile")) {
				trueModelFile = args[i + 1];
			} else if (args[i].equals("-dir")) {
				dir = args[i + 1];
			} else if (args[i].equals("-generatingModel")) {
				generatingModel = args[i + 1];
			} else if (args[i].equals("-resultsDir")) {
				resultsDir = args[i + 1];
			} else if (args[i].equals("-rep")) {
				rep = new Integer(args[i + 1]);
			} else if (args[i].equals("-seed")) {
				seed = new Integer(args[i + 1]);
			} else if (args[i].equals("-domainSize")) {
				domainSize = new Integer(args[i + 1]);

			}
		}
	}

	/**
	 * @param scoringFunctionString
	 * @throws IOException
	 */
	private static void doConsistencyAnalysis(String scoringFunctionString)
			throws IOException {
		System.out.println("#### scoringFunction: " + scoringFunctionString);
		datasetName = generatingModel + ".r." + rep + ".seed." + seed + ".csv";

		FileWriter fstream;
		BufferedWriter out;

		String outputFile = resultsDir + File.separator
				+ "rawResults_generatingModel_" + generatingModel
				+ "_scoringFunction_" + scoringFunctionString + ".csv";
		fstream = new FileWriter(outputFile, false);
		out = new BufferedWriter(fstream);

		GraphScorable scoringFunction = null;
		GraphScorable scoringFunction2 = null;
		if (scoringFunctionString.equals("ibscore")) {
			scoringFunction = new IBScore();
			
			scoringFunction2 = new IBScore();
			((IBScore) scoringFunction2).setTest(new SimulatedBayesianTest(
					uncertainty));
			
		} else if (scoringFunctionString.equals("mpl")) {
			scoringFunction = new MPL();
			
		} else if (scoringFunctionString.equals("ribscore")) {
			scoringFunction = new RIBScore();
			
			scoringFunction2 = new RIBScore();
			((RIBScore) scoringFunction2).setTest(new SimulatedBayesianTest(
					uncertainty));

		} else if (scoringFunctionString.equals("iribscore")) {
			scoringFunction = new IBScore(true);
		} else if (scoringFunctionString.equals("giribscore")) {
			scoringFunction = new RIBScore(true);
		} else if (scoringFunctionString.equals("cibscore")) {
			scoringFunction = new CIBScore();
		} else if (scoringFunctionString.equals("pibscore")) {
			scoringFunction = new PenalizedIBScore();
		}
		
		for (int D: Ds) {
			UndirectedGraph trueGraph = UndirectedGraphParser
					.readTrueGraph(trueModelFile);
			UndirectedGraph complementOfTrueGraph = trueGraph.getComplementaryGraph();
			int n = trueGraph.getNumberOfNodes();

			dataset = new Dataset(D, Dataset.getUniformDomainSizes(n,
					domainSize));
			dataset = dataset.readDataset(new java.io.File(dir + File.separator
					+ datasetName), D, 1);

//			SimulatedDataset simulatedDataset = new SimulatedDataset(D,
//					Dataset.getUniformDomainSizes(n, domainSize), trueGraph);

			UndirectedGraphScore trueGraphScore = scoringFunction.computeScore(
					trueGraph, dataset);
			UndirectedGraphScore complementOfTrueGraphScore = scoringFunction.computeScore(
					complementOfTrueGraph, dataset);
			
//						UndirectedGraphScore trueGraphSimulatedScore = scoringFunction2.computeScore(
//					trueGraph, simulatedDataset);

			String row = scoringFunction.getScoringFunctionName() + " " + D + " " + rep + " " + seed + " "
					+ trueGraphScore.getTotalScore() + " "
					+ Math.exp(trueGraphScore.getTotalScore()) + " " 
					+ complementOfTrueGraphScore.getTotalScore() + " "
					+ Math.exp(complementOfTrueGraphScore.getTotalScore()); 
//					+ " "
//							+ trueGraphSimulatedScore.getTotalScore() + " "
//							+ Math.exp(trueGraphSimulatedScore.getTotalScore());
							
			
			out.write(row);
			
			System.out.println(row);

			out.write("\n");

		}
		out.close();
	}

}
