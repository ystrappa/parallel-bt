package test.ib.ibmap.search;

/**
 * Testing structure learning algorithms for synthetic data
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import data.Dataset;

import math.Mathutil;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.learn.ib.algs.ibmap.score.RIBScore;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.algs.ibmap.search.BruteForce;
import mn.learn.ib.algs.ibmap.search.HillClimbing;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import mn.rep.UndirectedGraphsQualityMeasures;

public class ExperimentBruteForce {
	 private static String dir =
	 "/home/fschluter/experiments/datasets/sampled/undirected/datasetsNoHeader";
//	private static String dir = "/home/fschluter/newData/noHeader";
	private static String resultsDir = "/home/fschluter/foo";
	public static String[] scoringFunctions = { "ibscore" };
	private static List<Integer> ns = new ArrayList<Integer>(Arrays.asList(4));
	private static int[] taus = { 1};
	private static int[] Ds = { 25, 50, 100, 250, 500, 1000, 2500, 5000, 10000 }; // ,
																					// 800,
																					// 1600};
	private static int repetitions = 10;

	private static double[][][][][] algorithmResults;
	private static double[][][][] finalAlgorithmHDs;
	private static double[][][][] finalAlgorithmFMeasures;
	private static double[][][][] finalAlgorithmRuntimes;
	private static double[][][][] finalAlgorithmFalsePositives;
	private static double[][][][] finalAlgorithmFalseNegatives;
	private static double[][][][] finalAlgorithmPrecision;
	private static double[][][][] finalAlgorithmRecall;
	private static double[][][][] finalAlgorithmScoreBestStructure;
	private static double[][][][] finalAlgorithmScoreTrueStructure;

	private static Dataset dataset;
	private static String datasetName;
	private static int domainSize=2;

	public static void main(String[] args) throws IOException {
		algorithmResults = new double[ns.size()][taus.length][repetitions][Ds.length][10];
		finalAlgorithmHDs = new double[ns.size()][taus.length][Ds.length][3];
		finalAlgorithmFMeasures = new double[ns.size()][taus.length][Ds.length][3];
		finalAlgorithmRuntimes = new double[ns.size()][taus.length][Ds.length][3];
		finalAlgorithmFalsePositives = new double[ns.size()][taus.length][Ds.length][3];
		finalAlgorithmFalseNegatives = new double[ns.size()][taus.length][Ds.length][3];
		finalAlgorithmPrecision = new double[ns.size()][taus.length][Ds.length][3];
		finalAlgorithmRecall = new double[ns.size()][taus.length][Ds.length][3];
		for (int i = 0; i < ns.size(); i++) {
			int n = ns.get(i);
			for (int j = 0; j < taus.length; j++) {
				int tau = taus[j];
				for (String scoringFunctionString : scoringFunctions) {
					for (int rep = 1; rep <= repetitions; rep++) {
						datasetName = "sampledData_n" + n + "_t" + tau
								+ "_E100_M500_R" + rep + ".csv";
						for (int k = 0; k < Ds.length; k++) {
							int D = Ds[k];
							System.out.println("#n="
									+ n
									+ ", tau="
									+ tau
									+ " , alg"
									+ scoringFunctionString
									+ ", rep="
									+ rep
									+ ", D="
									+ D
									+ "(time:"
									+ Calendar.getInstance().get(
											Calendar.HOUR_OF_DAY)
									+ ":"
									+ Calendar.getInstance().get(
											Calendar.MINUTE) + ")");

							String fileNameToLoad = dir + File.separator
									+ datasetName;
							dataset = new Dataset(D, Dataset.getUniformDomainSizes(n,domainSize));
							dataset = dataset.readDataset(new java.io.File(
									fileNameToLoad), D, 1);

							GraphScorable scoringFunction = null;
							if (scoringFunctionString.equals("ibscore")) {
								scoringFunction = new IBScore();
							} else if (scoringFunctionString.equals("ribscore")) {
								scoringFunction = new RIBScore();
							}

							BruteForce bf = new BruteForce(n, dataset,
									scoringFunction);
							long runtime = new GregorianCalendar()
									.getTimeInMillis();
							bf.run();
							runtime = new GregorianCalendar().getTimeInMillis()
									- runtime;

							UndirectedGraph trueGraph = UndirectedGraphParser
									.readTrueGraph(fileNameToLoad, n);

							UndirectedGraph outputGraph = bf.getOutputNetwork();
							algorithmResults[i][j][rep - 1][k][0] = D;
							algorithmResults[i][j][rep - 1][k][1] = UndirectedGraphsQualityMeasures
									.hammingDistance(trueGraph, outputGraph);
							algorithmResults[i][j][rep - 1][k][2] = UndirectedGraphsQualityMeasures
									.fMeasure(trueGraph, outputGraph);
							algorithmResults[i][j][rep - 1][k][3] = runtime;
							algorithmResults[i][j][rep - 1][k][4] = UndirectedGraphsQualityMeasures
									.falsePositives(trueGraph, outputGraph);
							algorithmResults[i][j][rep - 1][k][5] = UndirectedGraphsQualityMeasures
									.falseNegatives(trueGraph, outputGraph);
							algorithmResults[i][j][rep - 1][k][6] = UndirectedGraphsQualityMeasures
									.precision(trueGraph, outputGraph);
							algorithmResults[i][j][rep - 1][k][7] = UndirectedGraphsQualityMeasures
									.recall(trueGraph, outputGraph);
							algorithmResults[i][j][rep - 1][k][8] = bf
									.getBestScore();
							algorithmResults[i][j][rep - 1][k][9] = scoringFunction
									.computeScore(trueGraph, dataset)
									.getTotalScore();

							String resultsFile = resultsDir + File.separator
									+ "rawResults_Algorithm_"
									+ scoringFunctionString + "_networkRANDOM_"
									+ "n" + n + "_tau" + tau + "_rep" + rep
									+ ".csv";

							saveRawResults(n, tau, rep, D, resultsFile,
									algorithmResults[i][j][rep - 1][k][1],
									algorithmResults[i][j][rep - 1][k][2],
									algorithmResults[i][j][rep - 1][k][3],
									algorithmResults[i][j][rep - 1][k][4],
									algorithmResults[i][j][rep - 1][k][5],
									algorithmResults[i][j][rep - 1][k][6],
									algorithmResults[i][j][rep - 1][k][7],
									algorithmResults[i][j][rep - 1][k][8],
									algorithmResults[i][j][rep - 1][k][9]);
						}
					}

					double hammings[][] = new double[Ds.length][repetitions];
					double fMeasures[][] = new double[Ds.length][repetitions];
					double runtimes[][] = new double[Ds.length][repetitions];
					double falsePositives[][] = new double[Ds.length][repetitions];
					double falseNegatives[][] = new double[Ds.length][repetitions];
					double precision[][] = new double[Ds.length][repetitions];
					double recall[][] = new double[Ds.length][repetitions];
					double scoreBestStructure[][] = new double[Ds.length][repetitions];
					double scoreTrueStructure[][] = new double[Ds.length][repetitions];

					for (int k = 0; k < Ds.length; k++) {
						for (int rep = 1; rep <= repetitions; rep++) {
							hammings[k][rep - 1] = algorithmResults[i][j][rep - 1][k][1];
							fMeasures[k][rep - 1] = algorithmResults[i][j][rep - 1][k][2];
							runtimes[k][rep - 1] = algorithmResults[i][j][rep - 1][k][3];
							falsePositives[k][rep - 1] = algorithmResults[i][j][rep - 1][k][4];
							falseNegatives[k][rep - 1] = algorithmResults[i][j][rep - 1][k][5];
							precision[k][rep - 1] = algorithmResults[i][j][rep - 1][k][6];
							recall[k][rep - 1] = algorithmResults[i][j][rep - 1][k][7];
							scoreBestStructure[k][rep - 1] = algorithmResults[i][j][rep - 1][k][7];
							scoreTrueStructure[k][rep - 1] = algorithmResults[i][j][rep - 1][k][7];
						}
					}
					for (int k = 0; k < Ds.length; k++) {
						int D = Ds[k];
						finalAlgorithmHDs[i][j][k][0] = D;
						finalAlgorithmHDs[i][j][k][1] = Mathutil
								.findMean(hammings[k]);
						finalAlgorithmHDs[i][j][k][2] = Mathutil
								.findStDev(hammings[k]);

						finalAlgorithmFMeasures[i][j][k][0] = D;
						finalAlgorithmFMeasures[i][j][k][1] = Mathutil
								.findMean(fMeasures[k]);
						finalAlgorithmFMeasures[i][j][k][2] = Mathutil
								.findStDev(fMeasures[k]);

						finalAlgorithmRuntimes[i][j][k][0] = D;
						finalAlgorithmRuntimes[i][j][k][1] = Mathutil
								.findMean(runtimes[k]);
						finalAlgorithmRuntimes[i][j][k][2] = Mathutil
								.findStDev(runtimes[k]);

						finalAlgorithmFalsePositives[i][j][k][0] = D;
						finalAlgorithmFalsePositives[i][j][k][1] = Mathutil
								.findMean(falsePositives[k]);
						finalAlgorithmFalsePositives[i][j][k][2] = Mathutil
								.findStDev(falsePositives[k]);

						finalAlgorithmFalseNegatives[i][j][k][0] = D;
						finalAlgorithmFalseNegatives[i][j][k][1] = Mathutil
								.findMean(falseNegatives[k]);
						finalAlgorithmFalseNegatives[i][j][k][2] = Mathutil
								.findStDev(falseNegatives[k]);

						finalAlgorithmPrecision[i][j][k][0] = D;
						finalAlgorithmPrecision[i][j][k][1] = Mathutil
								.findMean(precision[k]);
						finalAlgorithmPrecision[i][j][k][2] = Mathutil
								.findStDev(precision[k]);

						finalAlgorithmRecall[i][j][k][0] = D;
						finalAlgorithmRecall[i][j][k][1] = Mathutil
								.findMean(recall[k]);
						finalAlgorithmRecall[i][j][k][2] = Mathutil
								.findStDev(recall[k]);
						
						finalAlgorithmScoreBestStructure[i][j][k][0] = D;
						finalAlgorithmScoreBestStructure[i][j][k][1] = Mathutil
								.findMean(scoreBestStructure[k]);
						finalAlgorithmScoreBestStructure[i][j][k][2] = Mathutil
								.findStDev(scoreBestStructure[k]);

						finalAlgorithmScoreTrueStructure[i][j][k][0] = D;
						finalAlgorithmScoreTrueStructure[i][j][k][1] = Mathutil
								.findMean(scoreTrueStructure[k]);
						finalAlgorithmScoreTrueStructure[i][j][k][2] = Mathutil
								.findStDev(scoreTrueStructure[k]);
						
					}

					double[][] hammingResults = finalAlgorithmHDs[i][j];
					double[][] fMeasureResults = finalAlgorithmFMeasures[i][j];
					double[][] runtimeResults = finalAlgorithmRuntimes[i][j];
					double[][] falsePositivesResults = finalAlgorithmFalsePositives[i][j];
					double[][] falseNegativesResults = finalAlgorithmFalseNegatives[i][j];
					double[][] precisionResults = finalAlgorithmPrecision[i][j];
					double[][] recallResults = finalAlgorithmRecall[i][j];
					double[][] bestScoreResults = finalAlgorithmScoreBestStructure[i][j];
					double[][] trueScoreResults = finalAlgorithmScoreTrueStructure[i][j];

					saveResults(scoringFunctionString, n, tau, hammingResults,
							fMeasureResults, runtimeResults,
							falsePositivesResults, falseNegativesResults,
							precisionResults, recallResults, bestScoreResults, trueScoreResults);

				}
			}
		}
	}

	private static void saveResults(String algorithm, int n, int tau,
			double[][] hammingAlgorithm, double[][] fMeasuresalgorithm,
			double[][] runtimeAlgorithmResults,
			double[][] falsePositivesAlgorithmResults,
			double[][] falseNegativesAlgorithmResults,
			double[][] precisionAlgorithmResults,
			double[][] recallalgorithmResults,
			double[][] scoreBestStructureAlgorithmResults,
			double[][] scoreTrueStructureAlgorithmResults
			
			) {

		String outputFile = resultsDir + File.separator + "results_"
				+ algorithm + "_networkRANDOM_n" + n + "_tau" + tau + ".csv";
		try {
			// Create file
			FileWriter fstream = new FileWriter(outputFile, true);
			BufferedWriter out = new BufferedWriter(fstream);

			out.write("#D HD(avg) HD(sd) F-measure(avg) F-measure(sd) runtime(avg) runtime(sd) FalsePositives(avg) FalsePositives(sd) FalseNegatives(avg) FalseNegatives(sd) precision(avg) precision(sd) recall(sd) recall(avg)"
					+ "\n");

			for (int k = 0; k < Ds.length; k++) {
				int D = Ds[k];
				out.write(D + " " + hammingAlgorithm[k][1] + " "
						+ hammingAlgorithm[k][2] + " "
						+ fMeasuresalgorithm[k][1] + " "
						+ fMeasuresalgorithm[k][2] + " "
						+ runtimeAlgorithmResults[k][1] + " "
						+ runtimeAlgorithmResults[k][2] + " "
						+ falsePositivesAlgorithmResults[k][1] + " "
						+ falsePositivesAlgorithmResults[k][2] + " "
						+ falseNegativesAlgorithmResults[k][1] + " "
						+ falseNegativesAlgorithmResults[k][2] + " "
						+ precisionAlgorithmResults[k][1] + " "
						+ precisionAlgorithmResults[k][2] + " "
						+ recallalgorithmResults[k][1] + " "
						+ scoreBestStructureAlgorithmResults[k][2] + " "
						+ scoreTrueStructureAlgorithmResults[k][2] );
				out.write("\n");
			}
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

	private static void saveRawResults(int n, int tau, int rep, int D,
			String outputFile, double hammingAlgorithm,
			double fMeasuresAlgorithm, double runtimeAlgorithmResults,
			double falsePositivesAlgorithmResults,
			double falseNegativesAlgorithmResults,
			double precisionAlgorithmResults, double recallAlgorithmResults,
			double scoreBestStructure, double scoreTrueStructure) {

		try {
			// Create file
			FileWriter fstream;
			BufferedWriter out;

			if (D == Ds[0]) {
				fstream = new FileWriter(outputFile, true);
				out = new BufferedWriter(fstream);
				out.write("#D HD F-measure runtime FalsePositives FalseNegatives precision recall scoreBestStructure scoreTrueStructure"
						+ "\n");
			} else {
				fstream = new FileWriter(outputFile, true);
				out = new BufferedWriter(fstream);
			}

			out.write(D + " " + hammingAlgorithm + " " + fMeasuresAlgorithm
					+ " " + runtimeAlgorithmResults + " "
					+ falsePositivesAlgorithmResults + " "
					+ falseNegativesAlgorithmResults + " "
					+ precisionAlgorithmResults + " " 
					+ recallAlgorithmResults + " "
					+ scoreBestStructure + " "
					+ scoreTrueStructure ) ;

			out.write("\n");
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

}
