package test.ib.ibmap.search;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import mn.learn.ib.algs.ibmap.score.CIBScore;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.learn.ib.algs.ibmap.score.MPL;
import mn.learn.ib.algs.ibmap.score.PenalizedIBScore;
import mn.learn.ib.algs.ibmap.score.RIBScore;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.algs.ibmap.search.HillClimbing;
import mn.learn.ib.algs.mpl.MPLInterIAMBandHillClimbing;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import mn.rep.UndirectedGraphsQualityMeasures;
import data.Dataset;

/**
 * Experiment for testing the performance of Markov network structure scoring
 * functions for synthetic data when optimizing with a hill climbing search
 * method
 * 
 * @author fschluter
 *
 */
public class ExperimentHillClimbing {
	public static String[] scoringFunctions = {"ibscore" ,"ribscore"}; // , "mpl" };
	private static String dir = "/home/fschluter/experiments/datasets/sampled/realNets";
	private static String trueModelFile = "/home/fschluter/experiments/datasets/sampled/realNets/karate.graph";
	private static String generatingModel = "karate.ds.2";
	private static String resultsDir = "/home/fschluter/foo";

	private static int rep = 10;
	private static int seed = 1;
	private static int domainSize = 2;
	private static List<Integer> Ds = Arrays.asList(8000);

	private static double[][] algorithmResults = new double[Ds.size()][8];

	private static Dataset dataset;
	private static String datasetName;
	private static boolean incremental = true;
	private static boolean greedy = true;
	private static boolean debug = true;
	private static boolean synthetic = true;

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		parseParameters(args);
		for (String scoringFunctionString : scoringFunctions) {
			hillClimbingForAllSampleSizes(scoringFunctionString);
		}
	}

	/**
	 * @param args
	 */
	private static void parseParameters(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-scoringFunctions")) {
				List<String> aux = new ArrayList<String>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					aux.add(args[dIndex]);
					++dIndex;
				}
				scoringFunctions = new String[aux.size()];
				scoringFunctions = aux.toArray(scoringFunctions);

			} else if (args[i].equals("-Ds")) {
				Ds = new ArrayList<Integer>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					Ds.add(new Integer(args[dIndex]));
					++dIndex;
				}
				algorithmResults = new double[Ds.size()][8];
			} else if (args[i].equals("-trueModelFile")) {
				trueModelFile = args[i + 1];
			} else if (args[i].equals("-dir")) {
				dir = args[i + 1];
			} else if (args[i].equals("-generatingModel")) {
				generatingModel = args[i + 1];
			} else if (args[i].equals("-resultsDir")) {
				resultsDir = args[i + 1];
			} else if (args[i].equals("-incremental")) {
				incremental = new Boolean(args[i + 1]);
			} else if (args[i].equals("-debug")) {
				debug = new Boolean(args[i + 1]);
			} else if (args[i].equals("-synthetic")) {
				synthetic = new Boolean(args[i + 1]);
			} else if (args[i].equals("-greedy")) {
				greedy = new Boolean(args[i + 1]);
			} else if (args[i].equals("-rep")) {
				rep = new Integer(args[i + 1]);
			} else if (args[i].equals("-seed")) {
				seed = new Integer(args[i + 1]);
			} else if (args[i].equals("-domainSize")) {
				domainSize = new Integer(args[i + 1]);

			}
		}
	}

	/**
	 * @param scoringFunctionString
	 * @throws IOException
	 */
	private static void hillClimbingForAllSampleSizes(String scoringFunctionString) throws IOException {
		System.out.println("#### scoringFunction: " + scoringFunctionString);
		if (synthetic) {
			datasetName = generatingModel + ".r." + rep + ".seed." + seed + ".csv";
		} else {
			datasetName = generatingModel;
		}
		for (int D = 0; D < Ds.size(); D++) {
			hillClimbingForEachSampleSize(scoringFunctionString, rep, seed, D);
		}
	}

	private static void hillClimbingForEachSampleSize(String scoringFunctionString, int rep, int seed, int k)
			throws IOException {
		int D = Ds.get(k);
		System.out.println(
				"#datasetName=" + datasetName + ", D=" + D + "(time:" + Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
						+ ":" + Calendar.getInstance().get(Calendar.MINUTE) + ")");

		UndirectedGraph trueGraph = null;

		if (!trueModelFile.equals("null")) {
			trueGraph = UndirectedGraphParser.readTrueGraph(trueModelFile);
		}

		File dataFile = new java.io.File(dir + File.separator + datasetName);
		int n = Dataset.getNumberOfColumns(dataFile);

		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize), datasetName);
		dataset = dataset.readDataset(dataFile, D, 0);

		GraphScorable scoringFunction = null;
		if (scoringFunctionString.equals("ibscore")) {
			scoringFunction = new IBScore();
		} else if (scoringFunctionString.equals("mpl")) {
			scoringFunction = new MPL();
		} else if (scoringFunctionString.equals("ribscore")) {
			scoringFunction = new RIBScore();
		} else if (scoringFunctionString.equals("iribscore")) {
			scoringFunction = new IBScore(true);
		} else if (scoringFunctionString.equals("giribscore")) {
			scoringFunction = new RIBScore(true);
		} else if (scoringFunctionString.equals("cibscore")) {
			scoringFunction = new CIBScore();
		} else if (scoringFunctionString.equals("pibscore")) {
			scoringFunction = new PenalizedIBScore();
		}

		int inconsistentScoreCases = 0; // amount of cases where
										// a structure has
										// higher score than the
										// correct structure

		String landscapeFile = resultsDir + File.separator + "landscape_generatingModel_" + generatingModel
				+ "_scoringFunction_" + scoringFunctionString + ".csv";
		// Create file
		FileWriter fstream = null;
		BufferedWriter writer = null;

		HillClimbing hc = new HillClimbing(n, dataset, scoringFunction, greedy);
		if (debug) {
			fstream = new FileWriter(landscapeFile, true);
			writer = new BufferedWriter(fstream);
			hc.setDebugFile(writer);
		}

		if (trueGraph != null)
			hc.setTrueGraph(trueGraph);
		hc.setIncremental(incremental);
		
		long runtime = new GregorianCalendar().getTimeInMillis();
		UndirectedGraph outputGraph = null; 
		if (!scoringFunction.equals("mpl")) {
			hc.run();
			outputGraph = hc.getOutputNetwork();
		} else {
			//it is an optimized hill climbing for the MPL scoring function
			MPLInterIAMBandHillClimbing mplMMHC = new MPLInterIAMBandHillClimbing(n, dataset, true);
			mplMMHC.run();
			outputGraph = mplMMHC.getOutputNetwork();
		}
		runtime = new GregorianCalendar().getTimeInMillis() - runtime;

		algorithmResults[k][0] = D;
		algorithmResults[k][1] = trueGraph != null
				? UndirectedGraphsQualityMeasures.hammingDistance(trueGraph, outputGraph) : -1;
		algorithmResults[k][2] = trueGraph != null ? UndirectedGraphsQualityMeasures.fMeasure(trueGraph, outputGraph)
				: -1;
		algorithmResults[k][3] = runtime;
		algorithmResults[k][4] = trueGraph != null
				? UndirectedGraphsQualityMeasures.falsePositives(trueGraph, outputGraph) : -1;
		algorithmResults[k][5] = trueGraph != null
				? UndirectedGraphsQualityMeasures.falseNegatives(trueGraph, outputGraph) : -1;

		// if hamming distance is zero, optimal result were
		// obtained
		algorithmResults[k][6] = algorithmResults[k][1] == 0 ? 1 : 0;
		algorithmResults[k][7] = inconsistentScoreCases;

		String structureFile = resultsDir + File.separator + "structure_" + generatingModel + "_D_" + D
				+ "_scoringFunction_" + scoringFunctionString + ".mn";
		UndirectedGraphParser.writeGraphInLibraFormat(outputGraph, domainSize, structureFile);

		double scoreOfOutputGraph = scoringFunction.computeScore(outputGraph, dataset).getTotalScore();
		String resultsFile = resultsDir + File.separator + "rawResults_generatingModel_" + generatingModel
				+ "_scoringFunction_" + scoringFunctionString + ".csv";
		saveRawResults(scoringFunctionString, rep, seed, D, resultsFile, algorithmResults[k][1], algorithmResults[k][2],
				algorithmResults[k][3], algorithmResults[k][4], algorithmResults[k][5], algorithmResults[k][6],
				algorithmResults[k][7], scoreOfOutputGraph);
	}

	private static void saveRawResults(String scoringFunction, int rep, int seed, int D, String outputFile,
			double hammingAlgorithm, double fMeasuresAlgorithm, double runtimeAlgorithmResults,
			double falsePositivesAlgorithmResults, double falseNegativesAlgorithmResults, double optimalFound,
			double scoreInconsistency, double scoreOfOutputGraph) {

		try {
			// Create file
			FileWriter fstream;
			BufferedWriter out;

			fstream = new FileWriter(outputFile, true);
			out = new BufferedWriter(fstream);
			out.write(scoringFunction + " " + D + " " + rep + " " + seed + " " + hammingAlgorithm + " "
					+ fMeasuresAlgorithm + " " + runtimeAlgorithmResults + " " + falsePositivesAlgorithmResults + " "
					+ falseNegativesAlgorithmResults + " " + optimalFound + " " + scoreInconsistency + " "
					+ scoreOfOutputGraph);

			out.write("\n");
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

}
