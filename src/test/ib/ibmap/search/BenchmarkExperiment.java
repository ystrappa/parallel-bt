package test.ib.ibmap.search;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.independenceBased.BayesianTest;
import machineLearning.independenceBased.IndependenceTestOnData;
import machineLearning.independenceBased.PCAlgorithm;
import machineLearning.independenceBased.gsmn.GSMN;
import machineLearning.independenceBased.hiton.HHC;
import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.learn.ib.algs.ibmap.score.MPL;
import mn.learn.ib.algs.ibmap.score.RIBScore;
import mn.learn.ib.algs.ibmap.search.HillClimbing;
import mn.learn.ib.algs.mpl.MPLInterIAMBandHillClimbing;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import mn.rep.UndirectedGraphsQualityMeasures;
import util.JGraphtUtils;
import data.Dataset;

/**
 * Experiment for testing the consistency of Markov network structure scoring
 * functions for synthetic data
 * 
 * @author fschluter
 *
 */
public class BenchmarkExperiment {
	public static String[] algorithms = { "ibscore" };
	private static String dir = "/home/fschluter/experiments/datasets/sampled/undirected/mpl";
	private static String datasetName = "generatingModel2.n5";
	private static String resultsDir = "/home/fschluter/foo";
	private static int domainSize = 2;
	private static Dataset dataset;
	private static String testSufix = ".test.data";
	private static String trainSufix = ".ts.data";
	private static int seed = 123456;
	private static int D = 1000000;
	private static int numberOfTestsForAccuracy = 2000;
	private static int restarts;

	/**
	 * @param args
	 */
	private static void parseParameters(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-alg")) {
				List<String> aux = new ArrayList<String>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					aux.add(args[dIndex]);
					++dIndex;
				}
				algorithms = new String[aux.size()];
				algorithms = aux.toArray(algorithms);
			} else if (args[i].equals("-dir")) {
				dir = args[i + 1];
			} else if (args[i].equals("-D")) {
				D = new Integer(args[i + 1]);
			} else if (args[i].equals("-restarts")) {
				restarts = new Integer(args[i + 1]);
			} else if (args[i].equals("-dataset")) {
				datasetName = args[i + 1];
			} else if (args[i].equals("-numTests")) {
				numberOfTestsForAccuracy = new Integer(args[i + 1]);
			} else if (args[i].equals("-resultsDir")) {
				resultsDir = args[i + 1];
			} else if (args[i].equals("-testSufix")) {
				testSufix = args[i + 1];
			} else if (args[i].equals("-trainSufix")) {
				trainSufix = args[i + 1];
			}
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		parseParameters(args);
		for (String algorithm : algorithms) {

			FastDataset fastDatasetTrain = new FastDataset(dir + File.separator
					+ dataset + trainSufix);
			fastDatasetTrain.readAllWSchema(dir + File.separator + datasetName + trainSufix,
					1, D, ',');
			fastDatasetTrain.setBinarySchema();

			int n = fastDatasetTrain.numberOfAttributes();

			IndependenceTestOnData trainIT = new BayesianTest(fastDatasetTrain,
					true);
			trainIT.setThreshold(0.5);

			UGraphSimple outputNetwork;
			outputNetwork = null;
			if (algorithm.equals("gsmn")) {
				outputNetwork = runGSMN(trainIT);
			} else if (algorithm.equals("pc")) {
				outputNetwork = runPC(trainIT);
			} else if (algorithm.equals("hhc")) {
				outputNetwork = runHHC(trainIT, n);
			} else if (algorithm.equals("ibmaphhc")) {
				outputNetwork = runIBMAPHHC(trainIT, n, D, "empty");
			} else if (algorithm.equals("mplhc")) {
				outputNetwork = runMPLHHC(trainIT, n, D, "empty");
			} else if (algorithm.equals("mplhc_fully")) {
				outputNetwork = runMPLHHC(trainIT, n, D, "fully");
			} else if (algorithm.equals("mplIAMBHC")) { //optimization suggested by Pensar et al 2014 , section 5.
				outputNetwork = runMPLInterIAMBandHillClimbing(n, D);
			} else if (algorithm.equals("ribhhc")) {
				outputNetwork = runRIBHHC(trainIT, n, D, "empty");
			} else if (algorithm.equals("ibmaphhc_rr")) {
				outputNetwork = runIBMAPHHC_RR(trainIT, n, D, "empty");
			} else if (algorithm.equals("ribhhc_rr")) {
				outputNetwork = runRIBHHC_RR(trainIT, n, D, "empty");
			} else if (algorithm.equals("ibmaphhc_fully")) {
				outputNetwork = runIBMAPHHC(trainIT, n, D, "fully");
			} else if (algorithm.equals("ibmaphhc_gsmn")) {
				outputNetwork = runIBMAPHHC(trainIT, n, D, "gsmn");
			} else if (algorithm.equals("ibmaphhc_hhc")) {
				outputNetwork = runIBMAPHHC(trainIT, n, D, "hhc");
			} else if (algorithm.equals("ibmaphhc_pc")) {
				outputNetwork = runIBMAPHHC(trainIT, n, D, "pc");
			} else if (algorithm.equals("ribhhc_fully")) {
				outputNetwork = runRIBHHC(trainIT, n, D, "fully");
			} else if (algorithm.equals("ribhhc_gsmn")) {
				outputNetwork = runRIBHHC(trainIT, n, D, "gsmn");
			} else if (algorithm.equals("ribhhc_hhc")) {
				outputNetwork = runRIBHHC(trainIT, n, D, "hhc");
			} else if (algorithm.equals("ribhhc_pc")) {
				outputNetwork = runRIBHHC(trainIT, n, D, "pc");
			} else {
				throw new RuntimeException(
						"Unknown algorithm or scoring function! see your -scoringFunction parameter");
			}

			UndirectedGraph outputGraph = new UndirectedGraph(outputNetwork);
			String resultsFile = resultsDir + File.separator
					+ "rawResults_generatingModel_" + datasetName
					+ "_alg_" + algorithm;

			UndirectedGraphParser.writeGraphInLibraFormat(outputGraph, domainSize,
					resultsFile + ".mn");
			UndirectedGraphParser.writeGraphInAdjacencyMatrixFormat(
					outputGraph, resultsFile + ".graph");

			double accuracy = UndirectedGraphsQualityMeasures.accuracy(
					outputGraph, dir + File.separator + datasetName + testSufix, D,
					numberOfTestsForAccuracy, 123456);
			long runtime = outputNetwork.runtime;
			long testCounts =  outputNetwork.testCounts;
			long weightedTestCounts = outputNetwork.weightedTestCounts;
			int ascents = outputNetwork.ascents;

			// Create file
			FileWriter fstream;
			BufferedWriter out;

			fstream = new FileWriter(resultsFile + ".csv", true);
			out = new BufferedWriter(fstream);
			out.write(algorithm + " " + runtime + " " + accuracy + " " + testCounts + " " + weightedTestCounts + " " + ascents);

			out.write("\n");
			out.close();
		}
	}

	
	/**
	 * @param trainIT
	 * @param n
	 * @param D
	 * @param fully
	 *            When fully is false, the empty structure is the initial one.
	 * @return
	 * @throws IOException
	 */
	private static UGraphSimple runIBMAPHHC(IndependenceTestOnData trainIT,
			int n, int D, String initialStructure) throws IOException {
		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName + trainSufix), D, 0);
		
		UndirectedGraph g = null; 
		if (initialStructure.equals("gsmn") || initialStructure.equals("pc") || initialStructure.equals("hhc")) {
//			read the output structure of an algorith to use it as initial structure 
			String file = resultsDir + File.separator
			+ "rawResults_generatingModel_" + datasetName
			+ "_alg_" + initialStructure + ".graph";
			g = UndirectedGraphParser
					.readTrueGraph(file);
		} else if (initialStructure.equals("empty")) {
			g = new UndirectedGraph(n);
		} else if (initialStructure.equals("fully")) {
			g = new UndirectedGraph(n, true);
		}
			
		GraphScorable scoringFunction = new IBScore();
		HillClimbing hc = new HillClimbing(n, dataset, scoringFunction, true);
		long runtime = new GregorianCalendar().getTimeInMillis();
		hc.setIncremental(true);
		hc.run(g);
		UGraphSimple returnGraph = new UGraphSimple(hc.getOutputNetwork());
		returnGraph.runtime = new GregorianCalendar().getTimeInMillis() - runtime;
		returnGraph.testCounts = hc.getTestCounts();
		returnGraph.weightedTestCounts = hc.getWeightedTestCounts();

		return returnGraph;

	}
	
	
	private static UGraphSimple runIBMAPHHC_RR(IndependenceTestOnData trainIT,
			int n, int D, String initialStructure) throws IOException {
		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName + trainSufix), D, 0);
		
		GraphScorable scoringFunction = new IBScore();
		HillClimbing hc = new HillClimbing(n, dataset, scoringFunction, true);
		long runtime = new GregorianCalendar().getTimeInMillis();
		hc.setIncremental(true);
		hc.run(restarts,seed);
		UGraphSimple returnGraph = new UGraphSimple(hc.getOutputNetwork());
		returnGraph.runtime = new GregorianCalendar().getTimeInMillis() - runtime;
		returnGraph.testCounts = hc.getTestCounts();
		returnGraph.weightedTestCounts = hc.getWeightedTestCounts();

		return returnGraph;
	}

	private static UGraphSimple runRIBHHC_RR(IndependenceTestOnData trainIT,
			int n, int D, String initialStructure) throws IOException {
		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName + trainSufix), D, 0);
		
		GraphScorable scoringFunction = new RIBScore();
		HillClimbing hc = new HillClimbing(n, dataset, scoringFunction, true);
		long runtime = new GregorianCalendar().getTimeInMillis();
		hc.setIncremental(true);
		hc.run(restarts,seed);
		UGraphSimple returnGraph = new UGraphSimple(hc.getOutputNetwork());
		returnGraph.ascents = hc.getNumberOfAscents();
		returnGraph.runtime = new GregorianCalendar().getTimeInMillis() - runtime;
		returnGraph.testCounts = hc.getTestCounts();
		returnGraph.weightedTestCounts = hc.getWeightedTestCounts();

		return returnGraph;
	}


	
	private static UGraphSimple runRIBHHC(IndependenceTestOnData trainIT,
			int n, int D, String initialStructure) throws IOException {
		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName + trainSufix), D, 0);
		
		UndirectedGraph g = null; 
		if (initialStructure.equals("gsmn") || initialStructure.equals("pc") || initialStructure.equals("hhc")) {
//			read the output structure of an algorith to use it as initial structure 
			String file = resultsDir + File.separator
			+ "rawResults_generatingModel_" + datasetName
			+ "_alg_" + initialStructure + ".graph";
			g = UndirectedGraphParser
					.readTrueGraph(file);
		} else if (initialStructure.equals("empty")) {
			g = new UndirectedGraph(n);
		} else if (initialStructure.equals("fully")) {
			g = new UndirectedGraph(n, true);
		}

		GraphScorable scoringFunction = new RIBScore();
		HillClimbing hc = new HillClimbing(n, dataset, scoringFunction, true);
		long runtime = new GregorianCalendar().getTimeInMillis();
		hc.setIncremental(true);
		hc.run(g);
		UGraphSimple returnGraph = new UGraphSimple(hc.getOutputNetwork());
		returnGraph.runtime = new GregorianCalendar().getTimeInMillis() - runtime;
		returnGraph.ascents = hc.getNumberOfAscents();
		returnGraph.testCounts = hc.getTestCounts();
		returnGraph.weightedTestCounts = hc.getWeightedTestCounts();

		return returnGraph;

	}

	private static UGraphSimple runGSMN(IndependenceTestOnData trainIT) {
		GSMN gsmn = new GSMN(trainIT);
		gsmn.run();
		UGraphSimple outputNetwork = gsmn.getOutputNetwork();
		outputNetwork.runtime = gsmn.getRuntime();
		outputNetwork.ascents = 0;
		outputNetwork.testCounts = gsmn.testsCost;
		outputNetwork.weightedTestCounts = gsmn.testsWeightedCost;
		return outputNetwork;
	}

	private static UGraphSimple runPC(IndependenceTestOnData trainIT) {
		PCAlgorithm pc = new PCAlgorithm(trainIT, seed);
		pc.run();
		UGraphSimple outputNetwork = pc.getOutputNetwork();
		outputNetwork.runtime = pc.getRuntime();
		outputNetwork.ascents = 0;
		outputNetwork.testCounts = pc.numberOfTests;
		outputNetwork.weightedTestCounts = pc.weightedNumberOfTests;
		return outputNetwork;
	}

	private static UGraphSimple runHHC(IndependenceTestOnData trainIT, int n) {
		HHC hhc = new HHC(n, trainIT);
		hhc.run();
		UGraphSimple outputNetwork = hhc.getOutputNetwork();
		outputNetwork.runtime = hhc.getRuntime();
		outputNetwork.ascents = 0;
		outputNetwork.testCounts = hhc.getNumberOfTests();
		outputNetwork.weightedTestCounts = hhc.getWeightedNumberOfTests();
		return outputNetwork;
	}

	private static UGraphSimple runMPLHHC(IndependenceTestOnData trainIT,
			int n, int D, String initialStructure) throws IOException {
		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName + trainSufix), D, 0);
		
		UndirectedGraph g = null; 
		if (initialStructure.equals("gsmn") || initialStructure.equals("pc") || initialStructure.equals("hhc")) {
//			read the output structure of an algorith to use it as initial structure 
			String file = resultsDir + File.separator
					+ "rawResults_generatingModel_" + datasetName
					+ "_alg_" + initialStructure + ".graph";
			g = UndirectedGraphParser
					.readTrueGraph(file);
		} else if (initialStructure.equals("empty")) {
			g = new UndirectedGraph(n);
		} else if (initialStructure.equals("fully")) {
			g = new UndirectedGraph(n, true);
		}
		
		GraphScorable scoringFunction = new MPL();
		HillClimbing hc = new HillClimbing(n, dataset, scoringFunction, false);
		long runtime = new GregorianCalendar().getTimeInMillis();
		hc.setIncremental(true);
		hc.run(g);
		UGraphSimple returnGraph = new UGraphSimple(hc.getOutputNetwork());
		returnGraph.runtime = new GregorianCalendar().getTimeInMillis() - runtime;
		returnGraph.testCounts = hc.getTestCounts();
		returnGraph.weightedTestCounts = hc.getWeightedTestCounts();
		return returnGraph;
	}

	/**
	 * @param n
	 * @param D
	 * @return
	 * @throws IOException
	 */
	private static UGraphSimple runMPLInterIAMBandHillClimbing(int n, int D) throws IOException {
		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName + trainSufix), D, 0);
		
		long runtime = new GregorianCalendar().getTimeInMillis();
		MPLInterIAMBandHillClimbing mplMMHC = new MPLInterIAMBandHillClimbing(n, dataset, true);
		mplMMHC.run();
		UGraphSimple returnGraph = new UGraphSimple(mplMMHC.getOutputNetwork());
		returnGraph.runtime = new GregorianCalendar().getTimeInMillis() - runtime;
		return returnGraph;
	}
	
}
