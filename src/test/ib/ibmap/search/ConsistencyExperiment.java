package test.ib.ibmap.search;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.independenceBased.BayesianTest;
import machineLearning.independenceBased.IndependenceTestOnData;
import machineLearning.independenceBased.PCAlgorithm;
import machineLearning.independenceBased.gsmn.GSMN;
import machineLearning.independenceBased.hiton.HHC;
import mn.learn.ib.algs.ibmap.UndirectedGraphScore;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.learn.ib.algs.ibmap.score.MPL;
import mn.learn.ib.algs.ibmap.score.RIBScore;
import mn.learn.ib.algs.ibmap.search.HillClimbing;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import mn.rep.UndirectedGraphsQualityMeasures;
import util.JGraphtUtils;
import data.Dataset;

/**
 * Experiment for testing the consistency of Markov network structure scoring
 * functions for synthetic data
 * 
 * @author fschluter
 *
 */
public class ConsistencyExperiment {
	public static String[] scoringFunctions = { "ibscore" , "ribscore"};
	private static String dir = "/home/fschluter/experiments/datasets/sampled/undirected/mpl";
	
//	"synthetic.n4.0.graph",   "synthetic.n4.1.graph",  "synthetic.n4.47.graph" , "synthetic.n4.7.graph",
//	"synthetic.n4.15.graph",  "synthetic.n4.3.graph",  "synthetic.n4.63.graph"

	private static String trueModelFile = "/home/fschluter/experiments/datasets/sampled/undirected/mpl/synthetic.n4.7.graph";
	private static String generatingModel = "synthetic.n4.7.ds.2";
	private static String resultsDir = "/home/fschluter/foo";

	
	
	private static int rep = 1;
	private static int seed = 1;
	private static int domainSize = 2;
//	private static List<Integer> Ds = Arrays.asList(250, 500, 1000, 2000, 4000) ;
	private static List<Integer> Ds = Arrays.asList(8000,16000) ;

	private static Dataset dataset;
	private static String datasetName;
	private static boolean incremental = true;
	private static boolean onlyChordal = true;

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		parseParameters(args);
		for (String scoringFunctionString : scoringFunctions) {
			doConsistencyAnalysis(scoringFunctionString);
		}
	}

	/**
	 * @param args
	 */
	private static void parseParameters(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-scoringFunctions")) {
				List<String> aux = new ArrayList<String>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					aux.add(args[dIndex]);
					++dIndex;
				}
				scoringFunctions = new String[aux.size()];
				scoringFunctions = aux.toArray(scoringFunctions);

			} else if (args[i].equals("-Ds")) {
				Ds = new ArrayList<Integer>();
				int dIndex = i + 1;
				while (!args[dIndex].contains("-") || dIndex == args.length - 1) {
					Ds.add(new Integer(args[dIndex]));
					++dIndex;
				}
			} else if (args[i].equals("-trueModelFile")) {
				trueModelFile = args[i + 1];
			} else if (args[i].equals("-dir")) {
				dir = args[i + 1];
			} else if (args[i].equals("-generatingModel")) {
				generatingModel = args[i + 1];
			} else if (args[i].equals("-resultsDir")) {
				resultsDir = args[i + 1];
			} else if (args[i].equals("-incremental")) {
				incremental = new Boolean(args[i + 1]);
			} else if (args[i].equals("-onlyChordal")) {
				onlyChordal = new Boolean(args[i + 1]);
			} else if (args[i].equals("-rep")) {
				rep = new Integer(args[i + 1]);
			} else if (args[i].equals("-seed")) {
				seed = new Integer(args[i + 1]);
			} else if (args[i].equals("-domainSize")) {
				domainSize = new Integer(args[i + 1]);

			}
		}
	}

	/**
	 * @param scoringFunctionString
	 * @throws IOException
	 */
	private static void doConsistencyAnalysis(String scoringFunctionString)
			throws IOException {
		System.out.println("#### scoringFunction: " + scoringFunctionString);
		datasetName = generatingModel + ".r." + rep + ".seed." + seed + ".csv";
		for (int D = 0; D < Ds.size(); D++) {
			if (scoringFunctionString.equals("gsmn")
					|| scoringFunctionString.equals("pc")
					|| scoringFunctionString.equals("hhc")
					|| scoringFunctionString.equals("ibmaphhc")
					|| scoringFunctionString.equals("ribhhc")
					|| scoringFunctionString.equals("ibmaphhc_rr")
					|| scoringFunctionString.equals("ribhhc_rr")
					|| scoringFunctionString.equals("ibmaphhc_fully")
					|| scoringFunctionString.equals("ribhhc_fully")
					) {
				runAlgorithm(scoringFunctionString, rep, seed, D, datasetName);
			} else {
				exploreCompleteLandscape(scoringFunctionString, rep, seed, D);
			}
		}
	}

	
	private static void runAlgorithm(String scoringFunctionString, int rep2,
			int seed2, int k, String datasetName) throws IOException {
		UndirectedGraph trueGraph = UndirectedGraphParser
				.readTrueGraph(trueModelFile);
		UGraphSimple trueNetwork = new UGraphSimple(trueGraph);
		
		int n = trueGraph.getNumberOfNodes();

		int D = Ds.get(k);
		
		FastDataset fastDataset = new FastDataset(dir + File.separator + dataset);
			fastDataset.readAllWSchema(dir + File.separator + datasetName, 1, D, ',');			
			fastDataset.setBinarySchema();

		
		IndependenceTestOnData trainIT = new BayesianTest(fastDataset, true);
		trainIT.setThreshold(0.5);

		UGraphSimple outputNetwork;
		outputNetwork = null;
			if (scoringFunctionString.equals("gsmn")) {
				outputNetwork = runGSMN(trainIT);
			} else if (scoringFunctionString.equals("pc")) {
				outputNetwork = runPC(trainIT);
			} else if (scoringFunctionString.equals("hhc")) {
				outputNetwork = runHHC(trainIT, n);
			} else if (scoringFunctionString.equals("ibmaphhc")) {
				outputNetwork = runIBMAPHHC(trainIT,n, D,false );
			} else if (scoringFunctionString.equals("ribhhc")) {
				outputNetwork = runRIBHHC(trainIT, n, D, false);
			} else if (scoringFunctionString.equals("ibmaphhc_rr")) {
//				outputNetwork = runHHC(trainIT, n);
			} else if (scoringFunctionString.equals("ribhhc_rr")) {
//				outputNetwork = runHHC(trainIT, n);
			} else if (scoringFunctionString.equals("ibmaphhc_fully")) {
				outputNetwork = runIBMAPHHC(trainIT,n, D, true );
			} else if (scoringFunctionString.equals("ribhhc_fully")) {
				outputNetwork = runRIBHHC(trainIT, n, D, true);
				
		} else {
			throw new RuntimeException("Unknown algorithm or scoring function! see your -scoringFunction parameter");
		}
			UndirectedGraph outputGraph = new UndirectedGraph(outputNetwork);
			int hd = UndirectedGraphsQualityMeasures.hammingDistance(trueGraph,
					outputGraph);
			double fmeasure = UndirectedGraphsQualityMeasures.fMeasure(trueGraph,
					outputGraph);
			int falsePositives = UndirectedGraphsQualityMeasures.falsePositives(
					trueGraph, outputGraph);
			int falseNegatives = UndirectedGraphsQualityMeasures.falseNegatives(
					trueGraph, outputGraph);
			double accuracy = UndirectedGraphsQualityMeasures.accuracy(outputGraph, dir + File.separator + datasetName, D, 2000, 123456);
			long runtime = outputNetwork.runtime;
			
			// if hamming distance is zero, optimal result were
			// obtained
			int isOptimal = hd == 0 ? 1 : 0;

			String resultsFile = resultsDir + File.separator
					+ "rawResults_generatingModel_" + generatingModel
					+ "_scoringFunction_" + scoringFunctionString + ".csv";

			saveRawResults(scoringFunctionString, rep, seed, D, resultsFile, hd,
					fmeasure, runtime, falsePositives, falseNegatives, isOptimal,
					-1, 1, accuracy);

	}


	/**
	 * @param trainIT
	 * @param n
	 * @param D
	 * @param fully When fully is false, the empty structure is the initial one. 
	 * @return
	 * @throws IOException
	 */
	private static UGraphSimple runIBMAPHHC(IndependenceTestOnData trainIT, int n, int D, boolean fully) throws IOException {
		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName), D, 0);

		UndirectedGraph g = new UndirectedGraph(n,fully); 
		GraphScorable scoringFunction = new IBScore();
		HillClimbing hc = new HillClimbing(n, dataset, scoringFunction, true);
		long runtime = new GregorianCalendar().getTimeInMillis();
		hc.setIncremental(incremental);
		hc.run(g);
		UGraphSimple returnGraph = new UGraphSimple(hc.getOutputNetwork());
		returnGraph.runtime = runtime;
		return returnGraph;

	}


	private static UGraphSimple runRIBHHC(IndependenceTestOnData trainIT, int n, int D, boolean fully) throws IOException {
		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName), D, 0);
		UndirectedGraph g = new UndirectedGraph(n,fully);
		GraphScorable scoringFunction = new RIBScore();
		HillClimbing hc = new HillClimbing(n, dataset, scoringFunction, true);
		long runtime = new GregorianCalendar().getTimeInMillis();
		hc.setIncremental(incremental);
		hc.run(g);
		UGraphSimple returnGraph = new UGraphSimple(hc.getOutputNetwork());
		returnGraph.runtime = runtime;
		return returnGraph;

	}

	private static UGraphSimple runGSMN(IndependenceTestOnData trainIT) {
		GSMN gsmn = new GSMN(trainIT);
		gsmn.run();
		UGraphSimple outputNetwork = gsmn.getOutputNetwork();
		outputNetwork.runtime = gsmn.getRuntime();
		outputNetwork.ascents = 0;
		outputNetwork.testCounts = gsmn.testsCost;
		outputNetwork.weightedTestCounts = gsmn.testsWeightedCost;
		return outputNetwork;
	}

	private static UGraphSimple runPC(IndependenceTestOnData trainIT) {
		PCAlgorithm pc = new PCAlgorithm(trainIT, seed);
		pc.run();
		UGraphSimple outputNetwork = pc.getOutputNetwork();
		outputNetwork.runtime = pc.getRuntime();
		outputNetwork.ascents = 0;
		outputNetwork.testCounts = pc.numberOfTests;
		outputNetwork.weightedTestCounts = pc.weightedNumberOfTests;
		return outputNetwork;
	}

	private static UGraphSimple runHHC(IndependenceTestOnData trainIT, int n) {
		HHC hhc = new HHC(n, trainIT);
		hhc.run();
		UGraphSimple outputNetwork = hhc.getOutputNetwork();
		outputNetwork.runtime = hhc.getRuntime();
		outputNetwork.ascents = 0;
		outputNetwork.testCounts = hhc.getNumberOfTests();
		outputNetwork.weightedTestCounts = hhc.getWeightedNumberOfTests();
		return outputNetwork;
	}

	private static void exploreCompleteLandscape(String scoringFunctionString,
			int rep, int seed, int k) throws IOException {
		int D = Ds.get(k);
		System.out.println("#datasetName=" + datasetName + ", D=" + D
				+ "(time:" + Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
				+ ":" + Calendar.getInstance().get(Calendar.MINUTE) + ")");

		UndirectedGraph trueGraph = UndirectedGraphParser
				.readTrueGraph(trueModelFile);
		int n = trueGraph.getNumberOfNodes();

		dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, domainSize));
		dataset = dataset.readDataset(new java.io.File(dir + File.separator
				+ datasetName), D, 1);

		GraphScorable scoringFunction = null;
		if (scoringFunctionString.equals("ibscore")) {
			scoringFunction = new IBScore();
		} else if (scoringFunctionString.equals("mpl")) {
			scoringFunction = new MPL();
		} else if (scoringFunctionString.equals("ribscore")) {
			scoringFunction = new RIBScore();
		}

		UndirectedGraphScore trueGraphScore = null;
		if (scoringFunction != null) {
			trueGraphScore = scoringFunction.computeScore(trueGraph, dataset);
		}

		UndirectedGraph outputGraph = null;
		double scoreOfOutputGraph = 0;
		long runtime = new GregorianCalendar().getTimeInMillis();;

		int inconsistentScoreCases = 0; // amount of cases where
										// a structure has
										// higher score than the
										// correct structure
		double bestScore = Integer.MIN_VALUE;

		// Create file
		// String landscapeFile = resultsDir + File.separator
		// + "landscape_generatingModel_" + generatingModel
		// + "_scoringFunction_" + scoringFunctionString + ".csv";
		// FileWriter fstream = new FileWriter(landscapeFile, true);
		// BufferedWriter out = new BufferedWriter(fstream);
		// out.write("# generativeModel dataset D scoringFunction structureID score HD FP FN"
		// + "\n");

		UndirectedGraph emptyGraph = new UndirectedGraph(n);
		scoringFunction.computeScore(emptyGraph, dataset);
		List<UndirectedGraph> allPossibleGraphs = UndirectedGraph
				.getAllPossibleGraphs(n);
		UndirectedGraphScore lastScore = null;
		
		for (UndirectedGraph g : allPossibleGraphs) {
			/*
			 * when onlyChordal is false, it computes the score for all the
			 * graphs, but when onlyChordal is true, it computes the score only
			 * for chordal graphs
			 */
			if (!onlyChordal || JGraphtUtils.isChordal(g)) {
				UndirectedGraphScore gScore;
				if (!incremental) {
					gScore = scoringFunction.computeScore(g, dataset);
				} else {
					if (lastScore == null) {
						gScore = scoringFunction.computeScore(g, dataset);
					} else {
						gScore = scoringFunction.computeScore(g, lastScore,
								dataset); // incremental
											// computation
					}
				}
				lastScore = gScore;

				double score = gScore.getTotalScore();
				if (score > bestScore) {
					bestScore = score;
					outputGraph = gScore.getUndirectedGraph();
				}

				// count cases where a structure has higher score
				// than the correct structure
				if (trueGraphScore != null
						&& score >= trueGraphScore.getTotalScore()) {
					if (!trueGraph.getBitsetString()
							.equals(g.getBitsetString())) {
						++inconsistentScoreCases;
					}
				}
			}
		}
		// out.close();

		runtime = new GregorianCalendar().getTimeInMillis() - runtime;
		
		scoreOfOutputGraph = bestScore;

		int hd = UndirectedGraphsQualityMeasures.hammingDistance(trueGraph,
				outputGraph);
		double fmeasure = UndirectedGraphsQualityMeasures.fMeasure(trueGraph,
				outputGraph);
		int falsePositives = UndirectedGraphsQualityMeasures.falsePositives(
				trueGraph, outputGraph);
		int falseNegatives = UndirectedGraphsQualityMeasures.falseNegatives(
				trueGraph, outputGraph);
		double accuracy = UndirectedGraphsQualityMeasures.accuracy(outputGraph, dir + File.separator
				+ datasetName, D , 2000, 123456);
		
		// if hamming distance is zero, optimal result were
		// obtained
		int isOptimal = hd == 0 ? 1 : 0;

		String resultsFile = resultsDir + File.separator
				+ "rawResults_generatingModel_" + generatingModel
				+ "_scoringFunction_" + scoringFunctionString + ".csv";

		
		saveRawResults(scoringFunctionString, rep, seed, D, resultsFile, hd,
				fmeasure, runtime, falsePositives, falseNegatives, isOptimal,
				inconsistentScoreCases, scoreOfOutputGraph, accuracy);
	}

	private static void saveRawResults(String scoringFunction, int rep,
			int seed, int D, String outputFile, double hammingAlgorithm,
			double fMeasuresAlgorithm, double runtimeAlgorithmResults,
			double falsePositivesAlgorithmResults,
			double falseNegativesAlgorithmResults, double optimalFound,
			double scoreInconsistency, double scoreOfOutputGraph, double accuracy) {

		try {
			// Create file
			FileWriter fstream;
			BufferedWriter out;

			fstream = new FileWriter(outputFile, true);
			out = new BufferedWriter(fstream);
			out.write(scoringFunction + " " + D + " " + rep + " " + seed + " "
					+ hammingAlgorithm + " " + fMeasuresAlgorithm + " "
					+ runtimeAlgorithmResults + " "
					+ falsePositivesAlgorithmResults + " "
					+ falseNegativesAlgorithmResults + " " + optimalFound + " "
					+ scoreInconsistency + " " + scoreOfOutputGraph + " " + accuracy + " " + runtimeAlgorithmResults);

			out.write("\n");
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

}
