package test.ib.ibmap.search;

/**
 * Testing structure learning algorithms for synthetic data
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import math.Mathutil;
import mn.learn.ib.algs.ibmap.score.IBScore;
import mn.learn.ib.algs.ibmap.score.RIBScore;
import mn.learn.ib.algs.ibmap.score.GraphScorable;
import mn.learn.ib.algs.ibmap.search.BruteForce;
import mn.learn.ib.algs.ibmap.search.HillClimbing;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import mn.rep.UndirectedGraphsQualityMeasures;
import data.Dataset;

public class ExperimentGeneratingCase1 {
	private static String dir = "/home/fschluter/experiments/datasets/sampled/undirected/datasetsNoHeader/mpl";
	private static String resultsDir = "/home/fschluter/projects/current/IBMAP/exps/consistency";
	public static String[] algorithms = { "ibmaphhc" ,"ibmaphc","ibmapbf"
														 }; // =
														// {"ibmapbf","ibmaphc","ibmaphhc"};
	public static String[] scoringFunctions = { "ibscore" };
	private static String trueModelFile = "/home/fschluter/experiments/datasets/sampled/undirected/datasetsNoHeader/mpl/generatingModels/generatingModel3.n6.graph";
	private static String generatingModel = "generatingModel3.n6";
	private static int repetitions = 10;
	private static int seeds = 10;
	private static int[] Ds = { 250, 500, 1000, 2000, 4000, 8000 };

	private static double[][][] algorithmResults;
	private static double[][] finalAlgorithmHDs;
	private static double[][] finalAlgorithmFMeasures;
	private static double[][] finalAlgorithmRuntimes;
	private static double[][] finalAlgorithmFalsePositives;
	private static double[][] finalAlgorithmFalseNegatives;
	private static double[][] finalAlgorithmPrecision;
	private static double[][] finalAlgorithmRecall;

	private static Dataset dataset;
	private static String datasetName;
	private static int domainSize=2;

	public static void main(String[] args) throws IOException {
		algorithmResults = new double[repetitions * seeds][Ds.length][8];
		finalAlgorithmHDs = new double[Ds.length][3];
		finalAlgorithmFMeasures = new double[Ds.length][3];
		finalAlgorithmRuntimes = new double[Ds.length][3];
		finalAlgorithmFalsePositives = new double[Ds.length][3];
		finalAlgorithmFalseNegatives = new double[Ds.length][3];
		finalAlgorithmPrecision = new double[Ds.length][3];
		finalAlgorithmRecall = new double[Ds.length][3];
		;

		for (String algorithm : algorithms) {
			for (String scoringFunctionString : scoringFunctions) {
				System.out.println("#### algorithm: " + algorithm
						+ " , scoringFunction: " + scoringFunctionString);
				int counter = 0;
				for (int rep = 1; rep <= repetitions; rep++) {
					for (int seed = 1; seed <= seeds; seed++) {
						datasetName = generatingModel + ".r." + rep + ".seed."
								+ seed + ".csv";
						for (int k = 0; k < Ds.length; k++) {
							int D = Ds[k];
							System.out.println("#datasetName="
									+ datasetName
									+ ", D="
									+ D
									+ "(time:"
									+ Calendar.getInstance().get(
											Calendar.HOUR_OF_DAY)
									+ ":"
									+ Calendar.getInstance().get(
											Calendar.MINUTE) + ")");

							String fileNameToLoad = dir + File.separator
									+ datasetName;

							UndirectedGraph trueGraph = UndirectedGraphParser
									.readTrueGraph(trueModelFile);
							int n = trueGraph.getNumberOfNodes();
							dataset = new Dataset(D, Dataset.getUniformDomainSizes(n,domainSize));
							dataset = dataset.readDataset(new java.io.File(
									fileNameToLoad), D, 1);

							GraphScorable scoringFunction = null;
							if (scoringFunctionString.equals("ibscore")) {
								scoringFunction = new IBScore();
							} else if (scoringFunctionString.equals("ribscore")) {
								scoringFunction = new RIBScore();
							}

							UndirectedGraph outputGraph = null;
							double scoreOfOutputGraph = 0;
							long runtime = 0;
							if (algorithm.equals("ibmapbf")) {
								BruteForce bf = new BruteForce(n, dataset,
										scoringFunction);
								runtime = new GregorianCalendar()
										.getTimeInMillis();
								bf.run();
								runtime = new GregorianCalendar()
										.getTimeInMillis() - runtime;
								outputGraph = bf.getOutputNetwork();
								scoreOfOutputGraph = bf.getBestScore();
							} else if (algorithm.equals("ibmaphc")) {
								HillClimbing hc = new HillClimbing(n, dataset,
										scoringFunction, false);
								runtime = new GregorianCalendar()
										.getTimeInMillis();
								hc.run();
								runtime = new GregorianCalendar()
										.getTimeInMillis() - runtime;
								outputGraph = hc.getOutputNetwork();
								scoreOfOutputGraph = hc.getBestScore();

							} else if (algorithm.equals("ibmaphhc")) {
								HillClimbing hc = new HillClimbing(n, dataset,
										scoringFunction, true);
								runtime = new GregorianCalendar()
										.getTimeInMillis();
								hc.run();
								runtime = new GregorianCalendar()
										.getTimeInMillis() - runtime;
								outputGraph = hc.getOutputNetwork();
								scoreOfOutputGraph = hc.getBestScore();
							}

							algorithmResults[counter][k][0] = D;
							algorithmResults[counter][k][1] = UndirectedGraphsQualityMeasures
									.hammingDistance(trueGraph, outputGraph);
							algorithmResults[counter][k][2] = UndirectedGraphsQualityMeasures
									.fMeasure(trueGraph, outputGraph);
							algorithmResults[counter][k][3] = runtime;
							algorithmResults[counter][k][4] = UndirectedGraphsQualityMeasures
									.falsePositives(trueGraph, outputGraph);
							algorithmResults[counter][k][5] = UndirectedGraphsQualityMeasures
									.falseNegatives(trueGraph, outputGraph);
							algorithmResults[counter][k][6] = UndirectedGraphsQualityMeasures
									.precision(trueGraph, outputGraph);
							algorithmResults[counter][k][7] = UndirectedGraphsQualityMeasures
									.recall(trueGraph, outputGraph);

							String resultsFile = resultsDir + File.separator
									+ "rawResults_generatingModel_"
									+ generatingModel + "_Algorithm_"
									+ algorithm + "_scoringFunction_"
									+ scoringFunctionString + "_rep_" + rep
									+ "_seed_" + seed + ".csv";

							saveRawResults(algorithm, scoringFunctionString,
									rep, seed, D, resultsFile,
									algorithmResults[counter][k][1],
									algorithmResults[counter][k][2],
									algorithmResults[counter][k][3],
									algorithmResults[counter][k][4],
									algorithmResults[counter][k][5],
									algorithmResults[counter][k][6],
									algorithmResults[counter][k][7],
									scoreOfOutputGraph);

						}
						++counter;
					}

				}

				double hammings[][] = new double[Ds.length][repetitions * seeds];
				double fMeasures[][] = new double[Ds.length][repetitions
						* seeds];
				double runtimes[][] = new double[Ds.length][repetitions * seeds];
				double falsePositives[][] = new double[Ds.length][repetitions
						* seeds];
				double falseNegatives[][] = new double[Ds.length][repetitions
						* seeds];
				double precision[][] = new double[Ds.length][repetitions
						* seeds];
				double recall[][] = new double[Ds.length][repetitions * seeds];

				for (int k = 0; k < Ds.length; k++) {
					for (int l = 0; l < repetitions * seeds; l++) {
						hammings[k][l] = algorithmResults[l][k][1];
						fMeasures[k][l] = algorithmResults[l][k][2];
						runtimes[k][l] = algorithmResults[l][k][3];
						falsePositives[k][l] = algorithmResults[l][k][4];
						falseNegatives[k][l] = algorithmResults[l][k][5];
						precision[k][l] = algorithmResults[l][k][6];
						recall[k][l] = algorithmResults[l][k][7];
					}
				}
				for (int k = 0; k < Ds.length; k++) {
					int D = Ds[k];
					finalAlgorithmHDs[k][0] = D;
					finalAlgorithmHDs[k][1] = Mathutil.findMean(hammings[k]);
					finalAlgorithmHDs[k][2] = Mathutil.findStDev(hammings[k]);

					finalAlgorithmFMeasures[k][0] = D;
					finalAlgorithmFMeasures[k][1] = Mathutil
							.findMean(fMeasures[k]);
					finalAlgorithmFMeasures[k][2] = Mathutil
							.findStDev(fMeasures[k]);

					finalAlgorithmRuntimes[k][0] = D;
					finalAlgorithmRuntimes[k][1] = Mathutil
							.findMean(runtimes[k]);
					finalAlgorithmRuntimes[k][2] = Mathutil
							.findStDev(runtimes[k]);

					finalAlgorithmFalsePositives[k][0] = D;
					finalAlgorithmFalsePositives[k][1] = Mathutil
							.findMean(falsePositives[k]);
					finalAlgorithmFalsePositives[k][2] = Mathutil
							.findStDev(falsePositives[k]);

					finalAlgorithmFalseNegatives[k][0] = D;
					finalAlgorithmFalseNegatives[k][1] = Mathutil
							.findMean(falseNegatives[k]);
					finalAlgorithmFalseNegatives[k][2] = Mathutil
							.findStDev(falseNegatives[k]);

					finalAlgorithmPrecision[k][0] = D;
					finalAlgorithmPrecision[k][1] = Mathutil
							.findMean(precision[k]);
					finalAlgorithmPrecision[k][2] = Mathutil
							.findStDev(precision[k]);

					finalAlgorithmRecall[k][0] = D;
					finalAlgorithmRecall[k][1] = Mathutil.findMean(recall[k]);
					finalAlgorithmRecall[k][2] = Mathutil.findStDev(recall[k]);

				}

				String resultsFile = resultsDir + File.separator
						+ "results_generatingModel_" + generatingModel
						+ "_Algorithm_" + algorithm + "_scoringFunction_"
						+ scoringFunctionString + ".csv";

				saveResults(resultsFile, algorithm, scoringFunctionString,
						finalAlgorithmHDs, finalAlgorithmFMeasures,
						finalAlgorithmRuntimes, finalAlgorithmFalsePositives,
						finalAlgorithmFalseNegatives, finalAlgorithmPrecision,
						finalAlgorithmRecall);

			}
		}
	}

	private static void saveResults(String resultsFile, String algorithm,
			String scoringFunction, double[][] hammingAlgorithm,
			double[][] fMeasuresalgorithm, double[][] runtimeAlgorithmResults,
			double[][] falsePositivesAlgorithmResults,
			double[][] falseNegativesAlgorithmResults,
			double[][] precisionAlgorithmResults,
			double[][] recallalgorithmResults) {

		try {
			// Create file
			FileWriter fstream = new FileWriter(resultsFile, true);
			BufferedWriter out = new BufferedWriter(fstream);

			out.write("#D HD(avg) HD(sd) F-measure(avg) F-measure(sd) runtime(avg) runtime(sd) FalsePositives(avg) FalsePositives(sd) FalseNegatives(avg) FalseNegatives(sd) precision(avg) precision(sd) recall(sd) recall(avg)"
					+ "\n");

			for (int k = 0; k < Ds.length; k++) {
				int D = Ds[k];
				out.write(D + " " + hammingAlgorithm[k][1] + " "
						+ hammingAlgorithm[k][2] + " "
						+ fMeasuresalgorithm[k][1] + " "
						+ fMeasuresalgorithm[k][2] + " "
						+ runtimeAlgorithmResults[k][1] + " "
						+ runtimeAlgorithmResults[k][2] + " "
						+ falsePositivesAlgorithmResults[k][1] + " "
						+ falsePositivesAlgorithmResults[k][2] + " "
						+ falseNegativesAlgorithmResults[k][1] + " "
						+ falseNegativesAlgorithmResults[k][2] + " "
						+ precisionAlgorithmResults[k][1] + " "
						+ precisionAlgorithmResults[k][2] + " "
						+ recallalgorithmResults[k][1] + " "
						+ recallalgorithmResults[k][2] + " ");
				out.write("\n");
			}
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

	private static void saveRawResults(String algorithm,
			String scoringFunction, int rep, int seed, int D,
			String outputFile, double hammingAlgorithm,
			double fMeasuresAlgorithm, double runtimeAlgorithmResults,
			double falsePositivesAlgorithmResults,
			double falseNegativesAlgorithmResults,
			double precisionAlgorithmResults, double recallAlgorithmResults,
			double scoreOfOutputGraph) {

		try {
			// Create file
			FileWriter fstream;
			BufferedWriter out;

			if (D == Ds[0]) {
				fstream = new FileWriter(outputFile, true);
				out = new BufferedWriter(fstream);
				out.write("#algorithm scoringFunction D HD F-measure runtime FalsePositives FalseNegatives precision recall scoreOfOutputGraph"
						+ "\n");
			} else {
				fstream = new FileWriter(outputFile, true);
				out = new BufferedWriter(fstream);
			}

			out.write(algorithm + " " + scoringFunction + " " + D + " "
					+ hammingAlgorithm + " " + fMeasuresAlgorithm + " "
					+ runtimeAlgorithmResults + " "
					+ falsePositivesAlgorithmResults + " "
					+ falseNegativesAlgorithmResults + " "
					+ precisionAlgorithmResults + " " + recallAlgorithmResults
					+ " " + scoreOfOutputGraph);

			out.write("\n");
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

}
