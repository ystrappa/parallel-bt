package test.ib.it;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import mn.learn.ib.it.Gamma;

public class SimulationOfBayesianTest {
	// private static int[] Ds = { 50, 100, 250, 500, 1000, 2500, 5000, 10000 };
	private static int[] Ds = { 10, 25, 50, 100, 250, 500, 1000, 2000, 4000, 8000, 16000 };
	private static int[] domainSizes = {2, 4, 8, 16, 32 }; // , 8 , 16, 32 };
	private static int[] conditioningSetSizes = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; 

	/*
	 * alpha 0 simulates dependence (only counts in diagonal), and alpha 1
	 * simulates independence (uniform counts over cells). alpha 0.1 puts .9 of
	 * the counts in diagonal and the rest are uniformly distributed. alpha .5
	 * puts .5 of the counts in diagonal and the rest are uniformly distributed.
	 * alpha .9 puts an additional 10% of the counts in diagonal and the rest
	 * are uniformly distributed
	 */
	private static double[] alphaValues = { 0, .1, .5, .9, 1 };
	public static double _uniformGamma = 1;
	public static double _uniformAlpha = 1; // marginal over gamma
	public static double _uniformBeta = 1; // marginal over gamma

	static String outputDir = "/home/fschluter/projects/current/IBMAP/exps/btConsistencySim";
	private static double priorOfConditionalIndependenceModel = .5;

	public static void main(String[] args) throws IOException {
		runExperimentCaseI();
		// runExperimentCaseII();
	}

	public static void runExperimentCaseI() throws IOException {
		FileWriter fstreamResults;
		BufferedWriter outResults;
		FileWriter fstreamCounts;
		BufferedWriter outCounts;
		fstreamCounts = new FileWriter(outputDir + File.separator + "counts.dat", false);
		outCounts = new BufferedWriter(fstreamCounts);

		for (int s : conditioningSetSizes) {
			for (double alpha : alphaValues) {
				for (int ds : domainSizes) {
					fstreamResults = new FileWriter(
							outputDir + File.separator + "sim.alpha." + alpha + ".ds." + ds + ".s." + s + ".csv",
							false);
					outResults = new BufferedWriter(fstreamResults);
					for (int D : Ds) {
						outCounts.write("#conditioning set size: " + s + " domain size: " + ds + " D: " + D + "\n");

						double delta = D / (1 + alpha * (ds - 1));
						double delta_ii = delta / Math.pow(ds, s + 1);
						double notDelta = D - delta;
						double notDelta_ij = notDelta / (Math.pow(ds, s + 2) - Math.pow(ds, s + 1));

						double[][] completeContingencyTable = new double[ds][ds];
						double[] contingencyTableForDependence = new double[ds * ds]; 
						
						int k = 0;
						for (int i = 0; i < ds; i++) {
							for (int j = 0; j < ds; j++) {
								double counts = i == j ? delta_ii : notDelta_ij;
								completeContingencyTable[i][j] = counts;
								contingencyTableForDependence[k] = counts;
								++k;
							}
						}

						String contingencyTableString = "";
						for (int i = 0; i < completeContingencyTable.length; i++) {
							for (int j = 0; j < completeContingencyTable.length; j++) {
								contingencyTableString += completeContingencyTable[i][j] + " ";
							}
							contingencyTableString += "\n";
						}

						outCounts.write("ds: " + ds + " alpha: " + alpha + "\n" + contingencyTableString);

						double[] marginalsContingencyTable = new double[ds];
						for (int i = 0; i < ds; i++) {
							for (int j = 0; j < ds; j++) {
								marginalsContingencyTable[i] += completeContingencyTable[i][j];
							}
						}
						// that is the same for any variable
						// ( the only difference here is that the diagonal is
						// the first element of the array).
						// Numerically this is the same.
						// contingencyTableForIndependenceOfAnyVariable =
						// completeContingencyTable[0];

						int numberOfSlices = (int) Math.pow(ds, s);
						/*
						 * Probability of unconditional dependence for each
						 * slice
						 */
						// double[] unconditionalDependenceLoglikelihoods = new
						// double[numberOfSlices];
						/*
						 * probability of unconditional independence for each
						 * slice
						 */
						// double[] unconditionalIndependenceLoglikelihoods =
						// new double[numberOfSlices];

						double unconditionalDependenceLogLikelihood = computeUnconditionalDependenceLogLikelihood(
								contingencyTableForDependence);
						double unconditionalIndependenceLoglikelihood = computeUnconditionalIndependenceLogLikelihood(
								marginalsContingencyTable, ds);

						// in this simulation we have the same counts for all
						// the slices, so we have the same likelihoods
						// for (int i = 0; i < numberOfSlices; ++i) {
						// unconditionalDependenceLoglikelihoods[i] =
						// unconditionalDependenceLogLikelihood;
						// unconditionalIndependenceLoglikelihoods[i] =
						// unconditionalIndependenceLoglikelihood;
						// }

						double loglikelihoodIndependentModel = unconditionalIndependenceLoglikelihood * numberOfSlices;

						double loglikelihoodDependentModel = computeLoglikelihoodOfDependentModel(
								unconditionalIndependenceLoglikelihood, unconditionalDependenceLogLikelihood,
								numberOfSlices);

						// a is a name for log P(D|M_dep) - log P(D|M_ind) + log
						// P(M_dep) - log P(M_ind)
						double a = loglikelihoodDependentModel - loglikelihoodIndependentModel
								+ Math.log(1 - priorOfConditionalIndependenceModel)
								- Math.log(priorOfConditionalIndependenceModel);

						// applying a trick to compute log(exp()) without
						// underflow
						// see
						// https://hips.seas.harvard.edu/blog/2013/01/09/computing-log-sum-exp/
						// -log (1+exp(a)) is now -a -log(1+exp(-a))
						double logPosteriorIndependenceModel = -a - Math.log1p(Math.exp(-a));

						double logPosteriorDependenceModel = -Math
								.log1p(Math.exp(loglikelihoodIndependentModel - loglikelihoodDependentModel));

						outResults.write("D " + D + " indLogProb " + logPosteriorIndependenceModel + " depLogProb "
								+ logPosteriorDependenceModel + "\n");
					}
					outResults.write("\n");
					outResults.close();

				}
			}
		}
		outCounts.close();
	}

	public static void runExperimentCaseII() throws IOException {
	}

	private static double computeUnconditionalDependenceLogLikelihood(double[] contingencyTableForDependence) {
		return dirichletFunction(contingencyTableForDependence, _uniformGamma);
	}

	/**
	 * @param contingencyTableForIndependence
	 * @param dataset
	 * @param x
	 * @param y
	 * @param datasetForSlice
	 * @return
	 */
	private static double computeUnconditionalIndependenceLogLikelihood(double[] contingencyTableForIndependence,
			int ds) {
		double alpha = ds * _uniformAlpha;
		double beta = ds * _uniformBeta;
		return dirichletFunction(contingencyTableForIndependence, alpha)
				+ dirichletFunction(contingencyTableForIndependence, beta);
	}

	/**
	 * Compute the data likelihood of the dependent model. Formula (20) in
	 * (Margaritis and Bromberg, 2009)
	 * 
	 * @param unconditionalDependenceLikelihoods2
	 * @param unconditionalIndependenceLoglikelihood
	 * @return
	 */
	private static double computeLoglikelihoodOfDependentModel(double unconditionalIndependenceLoglikelihood,
			double unconditionalDependenceLoglikelihood, int numberOfSlices) {

		/* probability of the dependent model */
		double not_pMCI = 1 - priorOfConditionalIndependenceModel;

		/* prior of the independence model for each slice k */
		double pk = Math.log(priorOfConditionalIndependenceModel) / numberOfSlices;

		/* prior of the dependence model for each slice k */
		double qk = Math.log1p(-Math.pow(priorOfConditionalIndependenceModel, 1.0 / numberOfSlices));

		if (qk == Double.NaN || qk == Double.NEGATIVE_INFINITY || qk == Double.POSITIVE_INFINITY) {
			throw new RuntimeException("Math error with prior values, qk is  " + qk + ". Prior used: "
					+ priorOfConditionalIndependenceModel);
		}
		if (pk == Double.NaN || pk == Double.NEGATIVE_INFINITY || pk == Double.POSITIVE_INFINITY) {
			throw new RuntimeException("Math error with prior values, pk is  " + pk + ". Prior used: "
					+ priorOfConditionalIndependenceModel);
		}

		double aux1, aux2 = 0, aux3 = 0;

		// for (int k = 0; k < K; ++k) {
		double gk = unconditionalIndependenceLoglikelihood;
		double hk = unconditionalDependenceLoglikelihood;
		if (gk >= hk) { // preventing zero division
			aux1 = Math.log1p(Math.exp((hk + qk) - (gk + pk)));
			aux2 += gk + pk + aux1;
			aux3 += -aux1;
		} else {
			aux1 = Math.log1p(Math.exp((gk + pk) - (hk + qk)));
			aux2 += (hk + qk) + aux1;
			aux3 += (gk + pk) - (hk + qk) - aux1;
		}
		// }

		aux2 = aux2 * numberOfSlices;
		aux3 = aux3 * numberOfSlices;

		double aux4 = Math.log(-Math.expm1(aux3));
		return aux2 + aux4 - Math.log(not_pMCI);
	}

	/**
	 * Computes the Dirichlet function for a given set of counts and the
	 * corresponding hyper-parameters.
	 * 
	 * @param counts
	 *            An array of counts of the variables of interest
	 * @param hyperparameter
	 *            The hyper-parameters of the variables of interest
	 * @return The Dirichlet value (factorial function).
	 */
	final private static double dirichletFunction(double counts[], double hyperparameter) {
		double totalNumberOfCounts = 0;
		double marginalHyperparameter = 0;
		for (int i = 0; i < counts.length; i++) {
			totalNumberOfCounts += counts[i];
			marginalHyperparameter += hyperparameter;
		}

		double dirichletValue = 0;
		for (int i = 0; i < counts.length; i++) {
			dirichletValue += Gamma.logGamma(hyperparameter + counts[i]) - Gamma.logGamma(hyperparameter);
		}
		dirichletValue += Gamma.logGamma(marginalHyperparameter)
				- Gamma.logGamma(marginalHyperparameter + totalNumberOfCounts);

		return dirichletValue;
	}

}
