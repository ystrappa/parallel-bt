package test.ib.it;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.miv.pherd.ntree.NTree;

import mn.learn.ib.it.BayesianTest;
import mn.learn.ib.it.ParallelBayesianTest;
import mn.learn.ib.it.ParallelBayesianTestWithThreads;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import data.Dataset;

public class TestOfParallelBayesianTest {
//	private static int[] Ds = { 50, 100, 250, 500, 1000, 2500, 5000, 10000 };
	private static int[] Ds = { 10000};
	private static int[] domainSizes = {2}; //,3, 4, 5 };
//	private static int nThreads;

	public static void main(String[] args) throws IOException {
		
//		System.out.println("Run as: \n"+
//		"java -jar -Djomp.threads=N bt.jar [conditioning_set_size] parallel [dataset_dir] "+
//		"\njava -jar bt.jar [conditioning_set_size] sequential [dataset_dir]");
		
		int conditioningSetSize = Integer.parseInt(args[0]);
		boolean parallel = args[1].equals("parallel") ? true : false;
		String datasetDir = args[2];
		
//		if(parallel){
//		nThreads = Integer.parseInt(args[3]);
//		}

		List<Integer> x = new ArrayList<Integer>();
		x.add(0);
		List<Integer> y = new ArrayList<Integer>();
		y.add(1);
		List<Integer> z = new ArrayList<Integer>();
		
		for (int i = 2; i < conditioningSetSize+2; i++) {
			z.add(i);
		}
//		System.out.println("Conditioning set size: "+z.size());
		
		runExperiment(x, y, z, parallel, datasetDir);
	}

	public static void runExperiment(List<Integer> x, List<Integer> y,
			List<Integer> z, boolean parallel, String datasetDir) throws IOException {
		
//		String datasetDir = "/home/ystrappa/src/j-ibmap/experiments/data/adjnoun";
//		String datasetDir = "/home/fschluter/experiments/datasets/minimalCases/csv";
		String outputDir = "experiments";
		

//		UndirectedGraph trueGraph = UndirectedGraphParser
//				.readTrueGraph("/home/ystrappa/src/j-ibmap/experiments/data/adjnoun/adjnoun.graph");
////		System.out.println("True graph: " + trueGraph.toString());
//
//		int n = trueGraph.getNumberOfNodes();
		int n = 112;
//		System.out.println(n+ " variables.");
		
		FileWriter fstream;
		BufferedWriter out;

//		int[] reps={1,2,3,4,5,6,7,8,9,10};
		int[] reps={1};
		for (int rep : reps) {
//			System.out.println("######## \n rep: "+rep);
		for (int ds : domainSizes) {
			String version = parallel ? "parallel" : "sequential";
			String datasetName = "adjnoun" + ".ds." +ds
					+ ".r."+rep+".seed.1.csv";
			String fullPathToDataset = datasetDir + File.separator
					+ datasetName;
			String outputFile = outputDir + File.separator + "results_" + x.get(0)
					+ "," + y.get(0) + "," + z.size() + "_" + version + "_" + datasetName + ".txt";

			fstream = new FileWriter(outputFile, true);
			out = new BufferedWriter(fstream);
			long totalTime, startTime, stopTime;

			for (int D : Ds) {
				Dataset dataset = new Dataset(D, Dataset.getUniformDomainSizes(
						n, ds));
				try {
					dataset = dataset.readDataset(new java.io.File(
							fullPathToDataset), D, 1);

					String output = "";
					
//					System.out.println("Parallel? " + parallel);
					
					if(!parallel){
//						System.out.println("Sequential version");
					// Run the original sequential version of the test
					BayesianTest bt = new BayesianTest();
					startTime = System.currentTimeMillis();
					bt.independent(dataset, x, y, z);
					stopTime = System.currentTimeMillis();
					totalTime = stopTime - startTime;
					output += "Serial: size=" + z.size() + ", x " + x + " y " + y + " z " + z + " D: " + D
							+ " loglikelihoods " + bt.getLoglikelihoods()[0]
							+ " " + bt.getLoglikelihoods()[1] + " logprobs  "
							+ bt.getLogProbs()[0] + " " + bt.getLogProbs()[1]+"\n"+
							"total time: " + totalTime;
					}else{
//						System.out.println("Parallel version");
					// Run the same test in parallel
					// Number of threads must be specified as argument when running
					//ParallelBayesianTestWithThreads pbt = new ParallelBayesianTestWithThreads(nThreads);
					ParallelBayesianTest pbt = new ParallelBayesianTest();
					
					startTime = System.currentTimeMillis();
					pbt.independent(dataset, x, y, z);
					stopTime = System.currentTimeMillis();
					totalTime = stopTime - startTime;
					output += "Parallel:  size=" + z.size() + ", " +
							"x " + x + " y " + y + " z " + z + " D: " + D
							+ " loglikelihoods " + pbt.getLoglikelihoods()[0]
							+ " " + pbt.getLoglikelihoods()[1] + " logprobs  "
							+ pbt.getLogProbs()[0] + " " + pbt.getLogProbs()[1]+"\n"+
							"total time: " + totalTime;
					
					}
					
					System.out.println(z.size()+"\t"+totalTime);
					
					
					
					out.write(output);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			out.write("\n");
			out.close();
//			System.out.println(outputFile);
		}
	}
	}

}
