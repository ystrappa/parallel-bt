package test.ib.it;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mn.learn.ib.it.BayesianTest;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import data.Dataset;

public class TestOfBayesianTestPowerLawWithConditioningSetSize {
	 private static int[] Ds = { 125, 250, 500, 1000, 2000, 4000, 8000, 16000, 32000, 64000 };
//	private static int[] Ds = {  };
	private static String datasetDir = "/home/fschluter/experiments/datasets/minimalCases/csv";
	private static String outputDir = "/home/fschluter/projects/current/IBMAP/exps/btPowerLaw";
	private static String trueGraphPath = "/home/fschluter/experiments/datasets/minimalCases/generatingModel5.n16.graph";

	public static void main(String[] args) throws IOException {
		int[] reps = { 1,2,3,4,5,6,7,8,9,10 }; // , 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int rep : reps) {
			for (int D : Ds) {

				List<Integer> x = new ArrayList<Integer>();
				x.add(0);
				List<Integer> y = new ArrayList<Integer>();
				y.add(8);
				List<Integer> z = new ArrayList<Integer>();
				z.add(1);
				runExperiment(x, y, z, D, rep);
				z.add(2);
				runExperiment(x, y, z, D, rep);
				z.add(3);
				runExperiment(x, y, z, D, rep);
				z.add(4);
				runExperiment(x, y, z, D, rep);
				z.add(5);
				runExperiment(x, y, z, D, rep);
				z.add(6);
				runExperiment(x, y, z, D, rep);
				z.add(7);
				runExperiment(x, y, z, D, rep);
				z.add(9);
				runExperiment(x, y, z, D, rep);
			}
		}
	}

	public static void runExperiment(List<Integer> x, List<Integer> y,
			List<Integer> z, int D, int rep) throws IOException {

		// String datasetDir =
		// "/home/fschluter/experiments/datasets/minimalCases/csv";

		UndirectedGraph trueGraph = UndirectedGraphParser
				.readTrueGraph(trueGraphPath);
		System.out.println("True graph: " + trueGraph.toString());

		int n = trueGraph.getNumberOfNodes();

		FileWriter fstream;
		BufferedWriter out;

		System.out.println("######## \n rep: " + rep);
		String datasetName = "generatingModel5" + ".n16.ds." + 2 + ".r." + rep
				+ ".seed.1.csv";
		String fullPathToDataset = datasetDir + File.separator + datasetName;
		String outputFile = outputDir + File.separator + "/results_D" + D + "_"
				+ datasetName + "_r"+rep+ ".txt";

		fstream = new FileWriter(outputFile, true);
		out = new BufferedWriter(fstream);

		Dataset dataset = new Dataset(D, Dataset.getUniformDomainSizes(n, 2));
		try {
			dataset = dataset.readDataset(new java.io.File(fullPathToDataset),
					D, 1);

			BayesianTest bt = new BayesianTest();
			bt.independent(dataset, x, y, z);
			String output = "x " + x + " y " + y + " zsize " + z.size()
					+ " loglikelihoods " + bt.getLoglikelihoods()[0] + " "
					+ bt.getLoglikelihoods()[1] + " logprobs  "
					+ bt.getLogProbs()[0] + " " + bt.getLogProbs()[1];
			System.out.println(output);
			out.write(output);
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.write("\n");
		out.close();
	}

}
