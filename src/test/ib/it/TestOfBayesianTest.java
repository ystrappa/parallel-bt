package test.ib.it;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mn.learn.ib.it.BayesianTest;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import data.Dataset;

public class TestOfBayesianTest {
//	private static int[] Ds = { 50, 100, 250, 500, 1000, 2500, 5000, 10000 };
	private static int[] Ds = { 1000};
	private static int[] domainSizes = {2}; //,3, 4, 5 };

	public static void main(String[] args) throws IOException {

		List<Integer> x = new ArrayList<Integer>();
		x.add(1);
		List<Integer> y = new ArrayList<Integer>();
		y.add(2);
		List<Integer> z = new ArrayList<Integer>();
		 z.add(0);
//		 z.add(3);
//		 z.add(4);
		runExperiment(x, y, z);
	}

	public static void runExperiment(List<Integer> x, List<Integer> y,
			List<Integer> z) throws IOException {
		
		String datasetDir = "/home/fschluter/experiments/datasets/minimalCases/csv";
//		String datasetDir = "/home/fschluter/experiments/datasets/minimalCases/csv";
		String outputDir = "/home/fschluter/projects/current/IBMAP/exps/btConsistency";

		UndirectedGraph trueGraph = UndirectedGraphParser
				.readTrueGraph("/home/fschluter/experiments/datasets/minimalCases/generatingModel2.n5.graph");
		System.out.println("True graph: " + trueGraph.toString());

		int n = trueGraph.getNumberOfNodes();

		FileWriter fstream;
		BufferedWriter out;

		int[] reps={1,2,3,4,5,6,7,8,9,10};
		for (int rep : reps) {
			System.out.println("######## \n rep: "+rep);
		for (int ds : domainSizes) {
			String datasetName = "generatingModel2"+".n5.ds." + ds
					+ ".r."+rep+".seed.1.csv";
			String fullPathToDataset = datasetDir + File.separator
					+ datasetName;
			String outputFile = outputDir + File.separator + "/results_" + x
					+ "," + y + "," + z + "_" + datasetName + ".txt";

			fstream = new FileWriter(outputFile, true);
			out = new BufferedWriter(fstream);

			for (int D : Ds) {
				Dataset dataset = new Dataset(D, Dataset.getUniformDomainSizes(
						n, ds));
				try {
					dataset = dataset.readDataset(new java.io.File(
							fullPathToDataset), D, 1);

					BayesianTest bt = new BayesianTest();
					bt.independent(dataset, x, y, z);
					String output = "x " + x + " y " + y + " z " + z + " D: " + D
							+ " loglikelihoods " + bt.getLoglikelihoods()[0]
							+ " " + bt.getLoglikelihoods()[1] + " logprobs  "
							+ bt.getLogProbs()[0] + " " + bt.getLogProbs()[1]+"\n";
					System.out.println(output);
					out.write(output);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			out.write("\n");
			out.close();
		}
	}
	}

}
