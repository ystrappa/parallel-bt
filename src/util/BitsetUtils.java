package util;

import java.util.BitSet;

public class BitsetUtils {

	public static BitSet convert(long value) {
		BitSet bits = new BitSet();
		int index = 0;
		while (value != 0L) {
			if (value % 2L != 0) {
				bits.set(index);
			}
			++index;
			value = value >>> 1;
		}
		return bits;
	}

	public static long convert(BitSet bits) {
		long value = 0L;
		for (int i = 0; i < bits.length(); ++i) {
			value += bits.get(i) ? (1L << i) : 0L;
		}
		return value;
	}

	public static BitSet convert(long value, int numberOfVariables) {
		BitSet bits = new BitSet(numberOfVariables);
		
		int index = 0;
		while (value != 0L) {
			if (value % 2L != 0) {
				bits.set(index);
			}
			++index;
			value = value >>> 1;
		}
		return bits;
	}

	public static String getStringBitset(BitSet bitset) {
		String bitsetString = "";
		for(int i = bitset.size()-1; i>=0;--i){
			bitsetString+=bitset.get(i)?"1":"0";
		}
		return bitsetString;
	}

}
