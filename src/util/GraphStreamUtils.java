package util;

import java.io.File;
import java.io.IOException;

import org.graphstream.algorithm.generator.BarabasiAlbertGenerator;
import org.graphstream.algorithm.generator.DorogovtsevMendesGenerator;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.algorithm.generator.WattsStrogatzGenerator;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;

import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;

public class GraphStreamUtils {

	private static String resultsFile = "/home/fschluter/experiments/datasets/sampled/scaleFree";

	public static void generateBarabasiAlbertGraph(int n, int regularNodes) throws IOException {
		Graph graph = new SingleGraph("Barabàsi-Albert");

		Generator gen = new BarabasiAlbertGenerator(regularNodes);
		gen.addSink(graph);

		gen.begin();
		for (int i = 0; i < n; i++) {
			gen.nextEvents();
			if (graph.getNodeCount() == n)
				break;
		}
		gen.end();

		UndirectedGraph ugraph = new UndirectedGraph(graph.getNodeSet().size());
		for (Edge e : graph.getEachEdge()) {
			ugraph.addEdge(new Integer(e.getNode0().toString()), new Integer(e.getNode1().toString()));
		}

		UndirectedGraphParser.writeGraphInAdjacencyMatrixFormat(ugraph,
				resultsFile + File.separator + "scaleFree" + regularNodes + ".n" + n + ".graph");

		graph.display(true);

	}

	public static UndirectedGraph generateWattsStrogatzGraph(int n, int regularNodes, double p) throws IOException {
		Graph graph = new SingleGraph("small-world");
		Generator gen = new WattsStrogatzGenerator(n, regularNodes, p);

		gen.addSink(graph);
		gen.begin();
		while (gen.nextEvents()) {
			if (graph.getNodeCount() == n)
				break;
		}

		gen.end();

		UndirectedGraph ugraph = new UndirectedGraph(graph.getNodeSet().size());
		for (Edge e : graph.getEachEdge()) {
			ugraph.addEdge(new Integer(e.getNode0().toString()), new Integer(e.getNode1().toString()));
		}

		UndirectedGraphParser.writeGraphInAdjacencyMatrixFormat(ugraph,
				resultsFile + File.separator + "smallWorld" + regularNodes + ".n" + n + ".p" + p + ".graph");

		graph.display();

		return null;
	}

	public static UndirectedGraph generateDorogstovMendesGraph(int n) throws IOException {
		Graph graph = new SingleGraph("Dorogovtsev mendes");
		Generator gen = new DorogovtsevMendesGenerator();
		gen.addSink(graph);
		gen.begin();

		for (int i = 0; i < n; i++) {
			gen.nextEvents();
			if (graph.getNodeCount() == n)
				break;
		}

		gen.end();

		UndirectedGraph ugraph = new UndirectedGraph(graph.getNodeSet().size());
		for (Edge e : graph.getEachEdge()) {
			ugraph.addEdge(new Integer(e.getNode0().toString()), new Integer(e.getNode1().toString()));
		}

		UndirectedGraphParser.writeGraphInAdjacencyMatrixFormat(ugraph,
				resultsFile + File.separator + "dorogstovMendes.n" + n + ".graph");

		graph.display();

		return null;
	}

	public static void showGraph(UndirectedGraph g) throws IOException {
		Graph graph = new SingleGraph("");
		for (int j = 0; j < g.getNumberOfNodes(); ++j) {
			graph.addNode("" + j);
		}
		for (int j = 0; j < g.getNumberOfNodes(); ++j) {
			for (int k = j + 1; k < g.getNumberOfNodes(); ++k) {
				if (g.existEdge(j, k) || g.existEdge(k, j)) {
					graph.addEdge("" + j + "-" + k, "" + j, "" + k);
				}
			}
		}
		graph.display();
	}

	public static void main(String args[]) throws IOException {

		generateBarabasiAlbertGraph(16, 1);
	}
}
