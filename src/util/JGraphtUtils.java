package util;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mn.rep.CliqueMinimalSeparatorDecomposition;
import mn.rep.UndirectedGraph;

import org.jgrapht.GraphPath;
import org.jgrapht.ListenableGraph;
import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.alg.FloydWarshallShortestPaths;
import org.jgrapht.alg.StoerWagnerMinimumCut;
import org.jgrapht.alg.util.VertexDegreeComparator;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.ListenableDirectedGraph;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.graph.UndirectedSubgraph;

public class JGraphtUtils {

	public static SimpleGraph<String, DefaultEdge> getJGraphTSimpleGraph(
			UndirectedGraph g1) {
		int n = g1.getNumberOfNodes();
		SimpleGraph<String, DefaultEdge> g2 = new SimpleGraph<String, DefaultEdge>(
				DefaultEdge.class);
		for (int i = 0; i < n; ++i) {
			g2.addVertex("" + i);
		}
		for (int i = 0; i < n; ++i) {
			for (int j = i; j < n; ++j) {
				if (g1.existEdge(i, j)) {
					g2.addEdge("" + i, "" + j);
				}
			}
		}
		return g2;
	}
	
	public static SimpleGraph<String, DefaultWeightedEdge> getGraphWeightedByDegree(
			UndirectedGraph g1) {
		int n = g1.getNumberOfNodes();
		SimpleGraph<String, DefaultWeightedEdge> g2 = new SimpleGraph<String, DefaultWeightedEdge>(
				DefaultWeightedEdge.class);
		for (int i = 0; i < n; ++i) {
			g2.addVertex("" + i);
		}
		for (int i = 0; i < n; ++i) {
			for (int j = i; j < n; ++j) {
				if (g1.existEdge(i, j)) {
					g2.addEdge("" + i, "" + j);
				}
			}
		}
		
		for(DefaultWeightedEdge e: g2.edgeSet()){
			int degreeofSource = g2.degreeOf(g2.getEdgeSource(e));
			int degreeofTarget = g2.degreeOf(g2.getEdgeTarget(e));
			g2.setEdgeWeight(e, degreeofSource+degreeofTarget);
		}
		return g2;
	}

	public static boolean isChordal(UndirectedGraph g) {
		CliqueMinimalSeparatorDecomposition<String, DefaultEdge> cliqueMinimalSeparatorDecomposition = new CliqueMinimalSeparatorDecomposition<String, DefaultEdge>(
				getJGraphTSimpleGraph(g));
		return cliqueMinimalSeparatorDecomposition.isChordal();
	}
	

	public static Collection<java.util.Set<String>> getAllMaximalCliques(
			UndirectedGraph g) {
		BronKerboschCliqueFinder<String, DefaultEdge> finder = new BronKerboschCliqueFinder<String, DefaultEdge>(
				getJGraphTSimpleGraph(g));
		return finder.getAllMaximalCliques();
	}

	public static String[] sortNodesByDegreeInAscendingOrder(UndirectedGraph g1) {
		int n= g1.getNumberOfNodes();
		
		org.jgrapht.UndirectedGraph<String, DefaultEdge> sg = new UndirectedSubgraph<String, DefaultEdge>(getJGraphTSimpleGraph(g1), null, null);
		String[] sortedNodes = new String[n];
		sg.vertexSet().toArray(sortedNodes);

		VertexDegreeComparator<String,DefaultEdge> comp = new VertexDegreeComparator<String,DefaultEdge>(sg, true);
		Arrays.sort(sortedNodes, comp);	
		
		return sortedNodes;
	}
	
	public static List<Set<String>> getConnectedSets(UndirectedGraph g) {
		SimpleGraph<String, DefaultEdge> graph = getJGraphTSimpleGraph(g);
		ConnectivityInspector<String, DefaultEdge> ci = new ConnectivityInspector<String, DefaultEdge>(graph);		
		return ci.connectedSets();
	}

	public static List<Integer> getMinimalVertexSeparator(int x, int y, UndirectedGraph g) {
		
		List<Integer> minimalVertexSeparator = new ArrayList<Integer>();
		SimpleGraph<String, DefaultWeightedEdge> graph = getGraphWeightedByDegree(g);
		
//		 StoerWagnerMinimumCut<String, DefaultEdge> tmp = new StoerWagnerMinimumCut<String, DefaultEdge>();
		
		Collection<GraphPath<String, DefaultEdge>> shortestPaths = shortestPaths(g);

		
		
		for(String vertex: bestCut(graph)){
			
		}
		
		 return minimalVertexSeparator;
	}

	
	public static Set<String> bestCut(SimpleGraph<String, DefaultWeightedEdge> graph ){
		StoerWagnerMinimumCut<String, DefaultWeightedEdge> swmc = new StoerWagnerMinimumCut<String, DefaultWeightedEdge>(graph);
		return swmc.minCut();
	}
	
	public static boolean pathExist(int x, int y, UndirectedGraph g){
		SimpleGraph<String, DefaultEdge> graph = getJGraphTSimpleGraph(g);
		 ConnectivityInspector<String, DefaultEdge> ci = new ConnectivityInspector<String, DefaultEdge>(graph);
		 return ci.pathExists(""+x, ""+y);
	}

	public static GraphPath<String, DefaultEdge> shortestPath(int x, int y, UndirectedGraph g){
		SimpleGraph<String, DefaultEdge> graph = getJGraphTSimpleGraph(g);
		FloydWarshallShortestPaths<String, DefaultEdge> fwsp = new FloydWarshallShortestPaths<String,DefaultEdge>(graph);
		GraphPath<String, DefaultEdge> shortestPath = fwsp.getShortestPath(""+x,""+y); 
		return shortestPath;
	}
	
	public static Collection<GraphPath<String, DefaultEdge>> shortestPaths(UndirectedGraph g){
		SimpleGraph<String, DefaultEdge> graph = getJGraphTSimpleGraph(g);
		FloydWarshallShortestPaths<String, DefaultEdge> fwsp = new FloydWarshallShortestPaths<String,DefaultEdge>(graph);
		Collection<GraphPath<String, DefaultEdge>> shortestPaths = fwsp.getShortestPaths(); 
		return shortestPaths;
	}
	
	public static <V,E>E findBridge(SimpleGraph<V,E> graph){
		  @SuppressWarnings("unchecked") SimpleGraph<V,E> gc=(SimpleGraph<V,E>)graph.clone();
		  int size=new ConnectivityInspector<V,E>(graph).connectedSets().size();
		  for (  E e : graph.edgeSet()) {
		    gc.removeEdge(e);
		    
		    int nsize=new ConnectivityInspector<V,E>(gc).connectedSets().size();
		    if (nsize > size)     return e;
		    V source=graph.getEdgeSource(e);
		    V target=graph.getEdgeTarget(e);
		    gc.addEdge(source,target);
		  }
		  return null;
	}
	
	
	public static <V,E>Set<E> findAllBridges(SimpleGraph<V,E> graph){
		  Set<E> bridges=new HashSet<E>();
		  @SuppressWarnings("unchecked") SimpleGraph<V,E> gc=(SimpleGraph<V,E>)graph.clone();
		  int size=new ConnectivityInspector<V,E>(graph).connectedSets().size();
		  for (  E e : graph.edgeSet()) {
		    gc.removeEdge(e);
		    int nsize=new ConnectivityInspector<V,E>(gc).connectedSets().size();
		    if (nsize > size)     bridges.add(e);
		    V source=graph.getEdgeSource(e);
		    V target=graph.getEdgeTarget(e);
		    gc.addEdge(source,target);
		  }
		  return bridges;
		}
	
	

		  
	public static void testWithExampleGraph(String[] args) {
		UndirectedGraph g = new UndirectedGraph(7);
		g.addEdge(0, 1);
		g.addEdge(0, 3);
		g.addEdge(0, 4);
//		g.addEdge(1, 2);
		g.addEdge(1, 3);
		g.addEdge(1, 4);
//		g.addEdge(2, 4);
		g.addEdge(2, 5);
		g.addEdge(2, 6);
		g.addEdge(5,6);
		
		getConnectedSets(g);
		List<Integer> x = getMinimalVertexSeparator(0, 6, g);
		
	}


}
