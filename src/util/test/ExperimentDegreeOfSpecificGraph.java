package util.test;

import java.io.File;
import java.io.IOException;

import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;
import util.BitsetUtils;

public class ExperimentDegreeOfSpecificGraph {
//	static String dir = "/home/fschluter/experiments/datasets/sampled/realNets";
	static String dir = "/home/fschluter/experiments/datasets/sampled/undirected/mpl";
	
//	static String[] graphs = {
//	"synthetic.n4.0.graph",   "synthetic.n4.1.graph",  "synthetic.n4.47.graph" , "synthetic.n4.7.graph",
//	"synthetic.n4.15.graph",  "synthetic.n4.3.graph",  "synthetic.n4.63.graph"
//			
//	};

	static String[] graphs = {
			"generatingModel2.n6.graph"
	};

	public static void main(String[] args) throws IOException {
for(String graph:graphs){
	UndirectedGraph model1Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+graph);
	System.out.println("###graph: " + graph);
	System.out.println("#irr(Model1)" + model1Graph.computeIrregularity());
	System.out.println("#nodes: " + model1Graph.getNumberOfNodes());
	System.out.println("#edges: " + model1Graph.getBitset().cardinality());
	System.out.println("#graphCode: " + BitsetUtils.convert(model1Graph.getBitset()));
	
}
//		UndirectedGraph model2Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel2.n6.graph");
//		UndirectedGraph model3Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel3.n6.graph");
//		UndirectedGraph model5Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel5.n16.graph");
//		UndirectedGraph model7Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel7.n16.graph");
//		UndirectedGraph model8Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel8.n32.graph");
//		UndirectedGraph model9Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel9.n64.graph");
//		UndirectedGraph model11Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel11.n128.graph");
//		System.out.println("irr(Model2)" + model2Graph.computeIrregularity());
//		System.out.println("irr(Model3)" + model3Graph.computeIrregularity());
//		System.out.println("irr(Model5)" + model5Graph.computeIrregularity());
//		System.out.println("irr(Model7)" + model7Graph.computeIrregularity());
//		System.out.println("irr(Model8)" + model8Graph.computeIrregularity());
//		System.out.println("irr(Model9)" + model9Graph.computeIrregularity());
//		System.out.println("irr(Model11)" + model11Graph.computeIrregularity());
//		UndirectedGraph model_1reg= new UndirectedGraph(6);
//		model_1reg.addEdge(0, 1);
//		model_1reg.addEdge(2, 3);
//		model_1reg.addEdge(4, 5);
//		System.out.println("irr(model_1reg)" + model_1reg.computeIrregularity());
	}
}
