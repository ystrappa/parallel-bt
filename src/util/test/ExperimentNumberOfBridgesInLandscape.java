package util.test;

import java.util.List;
import java.util.Set;

import mn.rep.UndirectedGraph;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import util.JGraphtUtils;

public class ExperimentNumberOfBridgesInLandscape {
	
	public static void main(String[] args) {
		int min=6,max=6;
		double[] sizes = new double[max*3];
		for (int n = min; n <= max; n++) {
			List<UndirectedGraph> allPossibleGraphs = UndirectedGraph
					.getAllPossibleGraphs(n);
			for (UndirectedGraph g : allPossibleGraphs) {
				SimpleGraph<String, DefaultEdge> graph = JGraphtUtils.getJGraphTSimpleGraph(g);
				Set<DefaultEdge> e = JGraphtUtils.findAllBridges(graph);
				System.out.print("graph: \n" +g +" \n bridges: " +e + "\n#######\n");
			}
		}
		
	}
}
