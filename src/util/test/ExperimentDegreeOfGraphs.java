package util.test;

import java.io.File;
import java.io.IOException;

import util.JGraphtUtils;
import mn.rep.UndirectedGraph;
import mn.rep.UndirectedGraphParser;

public class ExperimentDegreeOfGraphs {
	static String dir = "/home/fschluter/experiments/datasets/sampled/realNets";
	public static void main(String[] args) throws IOException {

//		int highest = Integer.MIN_VALUE;
//		UndirectedGraph irregularGraph = null;
//		for(UndirectedGraph g: UndirectedGraph.getAllPossibleGraphs(6)){
//			int irregularity = g.computeIrregularity();
//			int numberOfEdges = g.getNumberOfEdges();
//			if(irregularity>highest){
//				irregularGraph=g;
//				highest=irregularity;
//			}
//			if(irregularity==52){
//				System.out.println("#############");
//				System.out.println("" + g.toString());
//				System.out.println("irr: " +irregularity );
//				System.out.println("edges: " +numberOfEdges );
//				System.out.println("is chordal: " +JGraphtUtils.isChordal(g));
//				System.out.println("#############");
//			}
//		}
//		System.out.println("#############");
//		System.out.println("#############");
//		System.out.println("#############");
//		System.out.println("#############");
//		System.out.println("#############");
//		System.out.println("#############");
//		System.out.println("" + irregularGraph.toString());
//		System.out.println("" +highest );
//		System.out.println("#############");

		//		
		UndirectedGraph model1Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel1.n6.graph");
		UndirectedGraph model2Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel2.n6.graph");
		UndirectedGraph model3Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel3.n6.graph");
		UndirectedGraph model5Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel5.n6.graph");
//		UndirectedGraph model7Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel7.n16.graph");
//		UndirectedGraph model8Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel8.n32.graph");
//		UndirectedGraph model9Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel9.n64.graph");
//		UndirectedGraph model11Graph = UndirectedGraphParser.readTrueGraph(dir+File.separator+"generatingModel11.n128.graph");
		System.out.println("irr(Model1)" + model1Graph.computeIrregularity() + " is chordal: " +JGraphtUtils.isChordal(model1Graph));
		System.out.println("irr(Model2)" + model2Graph.computeIrregularity()+" is chordal: " +JGraphtUtils.isChordal(model2Graph));
		System.out.println("irr(Model3)" + model3Graph.computeIrregularity()+" is chordal: " +JGraphtUtils.isChordal(model3Graph));
		System.out.println("irr(Model5)" + model5Graph.computeIrregularity()+" is chordal: " +JGraphtUtils.isChordal(model5Graph));
//		System.out.println("irr(Model7)" + model7Graph.computeIrregularity());
//		System.out.println("irr(Model8)" + model8Graph.computeIrregularity());
//		System.out.println("irr(Model9)" + model9Graph.computeIrregularity());
//		System.out.println("irr(Model11)" + model11Graph.computeIrregularity());
//		UndirectedGraph model_1reg= new UndirectedGraph(6);
//		model_1reg.addEdge(0, 1);
//		model_1reg.addEdge(2, 3);
//		model_1reg.addEdge(4, 5);
//		System.out.println("irr(model_1reg)" + model_1reg.computeIrregularity());
	}
}
