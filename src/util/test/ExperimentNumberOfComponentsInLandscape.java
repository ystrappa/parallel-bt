package util.test;

import java.util.List;
import java.util.Set;

import util.JGraphtUtils;
import mn.rep.UndirectedGraph;

public class ExperimentNumberOfComponentsInLandscape {
	
	public static void main(String[] args) {
		int min=2,max=7;
		double[] sizes = new double[max*3];
		for (int n = min; n <= max; n++) {
			List<UndirectedGraph> allPossibleGraphs = UndirectedGraph
					.getAllPossibleGraphs(n);
			for (UndirectedGraph g : allPossibleGraphs) {
				List<Set<String>> cs = JGraphtUtils.getConnectedSets(g);
				++sizes[cs.size()];
			}
			System.out.print(n +" " );
			for (int i = 0; i < sizes.length; i++) 
				System.out.print(sizes[i]/allPossibleGraphs.size() +" "); 
			System.out.println("\n");
		}
		
	}
}
