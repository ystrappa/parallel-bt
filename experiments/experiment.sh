#!/bin/bash

# Fixed parameters:
# Dataset size D = 100000
# model: adjnoun - 112 binary variables
# Test: I(0,1|Z), Z={2,3,4,...}

echo "conditioning_set_size,total_time" > results_bt.csv
echo "conditioning_set_size,total_time" > results_pbt.csv

for z in $(seq 1 5); do

java -native -Xmx12000M -jar pbt.jar $z parallel experiments/data/adjnoun 6 >> results_pbt.csv
echo "finished parallel, conditioning set size ${z}"

java -native -Xmx12000M -jar pbt.jar $z sequential experiments/data/adjnoun >> results_bt.csv
echo "finished sequential, conditioning set size ${z}"

done

echo "DONE."

